# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'PluginPattern' ]

from .misc import class_fullname

import logging
logger = logging.getLogger('Overlord.plugin_pattern')

class PluginPattern(object):
    def match_distance(self, plugin):
        raise NotImplementedError()

class AnyPluginPattern(PluginPattern):
    def match_distance(self, plugin):
        return 0
Any = AnyPluginPattern()

class Or(PluginPattern):
    def __init__(self, *patterns):
        super(Or, self).__init__()
        self.__patterns = patterns
    def match_distance(self, plugin):
        for pat in self.__patterns:
            d = pat.match_distance(plugin)
            if d is not None:
                return d
        else:
            return None

class And(PluginPattern):
    def __init__(self, *patterns):
        super(And, self).__init__()
        self.__patterns = patterns
    def match_distance(self, plugin):
        distances = [ pat.match_distance(plugin) for pat in self.__patterns ]
        if None in distances:
            return None
        return max(distances)

class ParentOf(PluginPattern):
    """Assumes cls.mro() does not change over the live of the pattern object."""
    def __init__(self, pluginkey, cls):
        assert isinstance(cls, type)
        super(ParentOf, self).__init__()
        self.__pluginkey, self.__cls = pluginkey, cls
        self.__parentnames = [ class_fullname(parent_cls)
                               for parent_cls in cls.mro() ]
    def match_distance(self, plugin):
        candidates = plugin.listfield(self.__pluginkey)
        logger.debug("match_distance(%s) in %s ?", plugin, candidates)
        for candidate in candidates:
            try:
                return self.__parentnames.index(candidate)
            except ValueError:
                continue
        else:
            return None
    def __repr__(self):
        return "ParentOf(%r, %r)" % (self.__pluginkey, self.__cls)
