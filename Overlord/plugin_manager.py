# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Support for loading plugins.
"""

__all__ = [ 'PluginManager' ]

from .exceptions import NoSuchPlugin
from . import plugin_pattern
from .plugin import Plugin, InvalidPlugin
import os, itertools, logging

logger = logging.getLogger('Overlord.PluginManager')

class PluginManager(object):
    def __init__(self, directory):
        self.__directory = directory
        self.__plugin_cache = {}

    def find_plugin(self, plugintype, default=None,
                    pythonplugin=True, pattern=plugin_pattern.Any):
        """
        Find a plugin of given `plugintype`, matching given `pattern`.
        If several plugins match, select the requested `default` if available,
        else pick one.
        """
        # get compatible plugins
        rated_plugins = {}
        for plugin in self.get_plugins_list(plugintype,
                                            pythonplugin=pythonplugin):
            rating = pattern.match_distance(plugin)
            if rating is None:
                continue # drop unmatched plugin
            if rating not in rated_plugins:
                rated_plugins[rating] = []
            rated_plugins[rating].append(plugin)
        if len(rated_plugins) == 0:
            raise NoSuchPlugin(
                "No %s plugin available matching %s." % (plugintype, pattern))

        # see if the default for chosen game is available, else
        # pick one
        if default is not None:
            for plugin in itertools.chain(*rated_plugins.values()):
                if plugin.ID == default:
                    logger.info("Using %s as default %s matching %s",
                                default, plugintype, pattern)
                    return plugin
            else:
                logger.info("Default %s (%s) matching %s not found, picking one",
                            plugintype, default, pattern)
        else:
            logger.info("No default %s specified matching %s, picking one",
                        plugintype, pattern)

        logger.debug("Candidates: " +
                     '; '.join([ "%s %s" % (k, ', '.join([ v.name
                                                           for v in plugins ]))
                                 for k, plugins in rated_plugins.items() ]))

        # pick the 1st one
        bestrating = min(rated_plugins.keys())
        logger.info(" ...picked %s", rated_plugins[bestrating][0].name)
        return rated_plugins[bestrating][0]

    def get_plugins_list(self, plugintype, pythonplugin=True,
                         pattern=plugin_pattern.Any):
        return self.__get_plugins_fromdir(self.__directory, plugintype,
                                          pythonplugin=pythonplugin,
                                          pattern=pattern)

    def get_plugin(self, plugintype, name, pythonplugin=True):
        """Simple lookup of plugin by type and name."""
        for plugin in self.get_plugins_list(plugintype,
                                            pythonplugin=pythonplugin):
            assert isinstance(plugin, Plugin)
            if plugin.ID == name:
                return plugin
        else:
            raise NoSuchPlugin("No %s plugin named %r" % (plugintype, name))

    def __get_plugins_fromdir(self, path, plugintype, pythonplugin=True,
                              pattern=plugin_pattern.Any):
        """
        Iterator returning Plugin instances of given plugintype under path.
        """
        if os.path.isdir(path):
            # recurse into subdirs
            for dirent in os.listdir(path):
                for plugin in self.__get_plugins_fromdir(
                        os.path.join(path, dirent), plugintype,
                        pythonplugin=pythonplugin, pattern=pattern):
                    yield plugin
        else:
            cache_key = (path, plugintype, pythonplugin)
            if cache_key not in self.__plugin_cache:
                try:
                    self.__plugin_cache[cache_key] = \
                        Plugin(path, plugintype, manager=self,
                               pythonplugin=pythonplugin)
                except NoSuchPlugin:
                    return
                except InvalidPlugin as ex:
                    logger.error("ignoring invalid plugin : %s", ex.args[0])
                    return

            if pattern.match_distance(self.__plugin_cache[cache_key]) is None:
                return

            yield self.__plugin_cache[cache_key]
