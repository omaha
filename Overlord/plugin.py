# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'Plugin', 'InvalidPlugin' ]

from .exceptions import NoSuchPlugin
from xdg.DesktopEntry import DesktopEntry, ParsingError
import os
from .misc import which, import_module

class InvalidPlugin(Exception):
    pass

class Plugin(object):
    "An xdg.DesktopEntry-based plugin."

    def __init__(self, path, plugintype, manager, pythonplugin=True):
        if not path.endswith('.desktop'):
            raise NoSuchPlugin("%r does not have .desktop suffix" %
                               (path,))

        self.__entry = DesktopEntry()
        try:
            self.__entry.parse(path)
        except ParsingError as ex:
            raise InvalidPlugin("Error parsing %s: %s, ignoring it" %
                                (path, ex.args))

        if not self.__entry.hasKey('Type'):
            raise InvalidPlugin("%r has no declared type" % (self.__entry,) )
        if self.__entry.get('Type') != plugintype:
            raise NoSuchPlugin("%r has type %r, requested type was %r" %
                               (self.__entry, self.__entry.get('Type'),
                                plugintype))

        self.__usable = True
        if self.__entry.hasKey('TryExec'):
            try:
                which(self.__entry.get('TryExec'))
            except IOError:
                # exe not found
                self.__usable = False

        if pythonplugin:
            if not self.__entry.hasKey('X-Omaha-Package'):
                raise InvalidPlugin("%s %s without X-Omaha-Package" % \
                                    (plugintype, path))
            if not self.__entry.hasKey('X-Omaha-Class'):
                raise InvalidPlugin("%s plugin %s without X-Omaha-Class" % \
                                    (plugintype, path))

        self.__path, self.__manager = path, manager
        self.__class = None

    @property
    def ID(self):
        return self.datafield('X-Omaha-ID')
    @property
    def name(self):
        return self.__entry.getName()
    @property
    def comment(self):
        return self.__entry.getComment()
    @property
    def usable(self):
        return self.__usable
    @property
    def directory(self):
        return os.path.dirname(self.__entry.filename)
    @property
    def icon(self):
        icon = self.__entry.getIcon()
        if icon != '':
            icon = "%s/%s" % (self.directory, icon)
        return icon

    def __repr__(self):
        return "<Plugin(%r) at %r>" % (self.__path, id(self))

    def datafield(self, field):
        return self.__entry.get(field)
    def has_datafield(self, field):
        return self.__entry.hasKey(field)
    def listfield(self, field):
        return self.__entry.get(field, list=True)

    @property
    def Class(self):
        if not self.__usable:
            raise RuntimeError("Plugin %r is not usable" % (self.name,))
        if not self.__class:
            self.__class = _import_class_from(
                self.datafield('X-Omaha-Package'),
                self.datafield('X-Omaha-Class'))
            setattr(self.__class, 'x-plugin-desc', self)

        return self.__class

def _import_class_from(modulename, classname):
    """Import named module and return named class thereof."""
    return getattr(import_module(modulename), classname)
