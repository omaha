# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'Param', 'ParamDecl', 'walk_params', 'dep_ordered_params' ]

from .exceptions import NoSuchPlugin, ParameterError
from .misc import pretty
from . import plugin_pattern
from .plugin import Plugin as CorePlugin

import logging

class AbstractParamDecl(object):
    "Base class for any Param declaration"
    def __has_deps_not_in__(self, ordered):
        raise NotImplementedError()

class ParamDecl(AbstractParamDecl):
    """Abstract class for parameter declarations."""
    def __init__(self, label, default=None, depends_on=None, pinned_value=None):
        self.__label = label
        self.__default = default
        self.__pinnedvalue = pinned_value
        self.__depends_on = depends_on or {}
    def clone(self, **kwargs):
        """Return a new ParamDecl with the same attributes, possibly overridden.

        Any argument will be passed to the constructor of the new
        ParamDecl.
        """
        for argname, attribute in self.clonable_params():
            if argname not in kwargs:
                kwargs[argname] = attribute
        return type(self)(**kwargs)
    def clonable_params(self):
        "A list of (paramname, value) pairs to be used by .clone()."
        return [('label', self.__label),
                ('default', self.__default),
                ('pinned_value', self.__pinnedvalue),
                ('depends_on', self.__depends_on)]
    @property
    def label(self): return self.__label
    @property
    def depends_on(self): return self.__depends_on
    def default(self, plugin_manager):
        """The default value for the parameter.

        When none was declared, the actual value may depend on the
        overall context, which we define to be the plugin_manager in use.
        """
        return self.__default
    def cast(self, value):
        """Cast `value` into something meaningful for this ParamDecl.

        When impossible, raise a `LookupError`.
        """
        raise NotImplementedError()
    @property
    def pinned_value(self):
        return self.__pinnedvalue
    @pinned_value.setter
    def pinned_value(self, v):
        self.__pinnedvalue = self.cast(v)
    def __pretty__(self, indent, curindent):
        return "{0} ParamDecl labelled {1!r}".format(type(self).__name__, self.label)

    # AbstractParamDecl method implementations
    def __has_deps_not_in__(self, ordered):
        for depname in self.depends_on.iterkeys():
            if depname not in [ paramid for paramid, _ in ordered ]:
                return True
        else:
            return False

class Param(object):
    """
    An instance of a parameter declaration, including a value.

    Param instances can be attached a `userdata` object for later use,
    eg. in a dialog callback.
    """
    def __init__(self, decl, plugin_manager=None, userdata=None):
        assert isinstance(decl, ParamDecl), "{0!r} is not a ParamDecl".format(decl)
        if isinstance(decl, Plugin):
            assert plugin_manager, "Plugin parameter requires a plugin_manager"
        self.__decl = decl
        self.__plugin_manager = plugin_manager
        self.__userdata = userdata
        self.__value = None
        self.__changed_listeners = {}
        # FIXME: we should not know about that - paramdecl as
        # metaclass would help ?
        if hasattr(decl, 'subparams'):
            self.__subparams = dict(
                (parentid, fresh_parameters_for_decls(subparams, plugin_manager))
                for parentid, subparams in decl.subparams.items() )
        else:
            self.__subparams = {}

    @property
    def decl(self): return self.__decl
    @property
    def label(self): return self.__decl.label
    @property
    def userdata(self): return self.__userdata

    def register_changed(self, listener, **kwargs):
        assert callable(listener)
        assert listener not in self.__changed_listeners
        self.__changed_listeners[listener] = kwargs
    def unregister_changed(self, listener):
        del self.__changed_listeners[listener]

    @property
    def value(self):
        if self.__decl.pinned_value is not None:
            return self.__decl.pinned_value
        if self.__value is not None:
            return self.__value
        return self.__decl.default(self.__plugin_manager)
    @value.setter
    def value(self, v):
        old = self.__value
        self.__value = self.__decl.cast(v)
        for listener, kwargs in self.__changed_listeners.items():
            listener(param=self, oldvalue=old, **kwargs)

    def __pretty__(self, indent, curindent):
        return "Param(%s): %s%s" % (
            pretty(self.__decl), self.value,
            ("" if (not self.subparams or
                    self.value.name not in self.subparams)
             else " sub=" + pretty(self.subparams[self.value.name],
                                   indent, curindent+indent)))

    # FIXME see above
    @property
    def subparams(self): return self.__subparams
    @property
    def has_subparams(self): return len(self.__subparams) != 0
    def set_subparams(self, subparams):
        # this is only a hack to init Plugin.subparams, don't use it for anything else
        assert isinstance(self.__decl, Plugin), \
               "%r is not a Plugin (%r)" % (self.__decl, type(self.__decl))
        self.__subparams = subparams

# Helpers

# FIXME: should be in parametrizable ?
def getparam(obj, paramname):
    "Return the Param object behind named parameter."
    return getattr(type(obj), paramname).fget_param(obj)

def has_deps_not_in(param, ordered, refobj, paramid):
    if isinstance(param, Param):
        decl = param.decl
    else: # bare value
        if refobj is None:
            raise ParameterError("cannot use bare param values without a reference object")
        decl = getparam(refobj, paramid).decl
    return decl.__has_deps_not_in__(ordered)
def dep_ordered_params(params, refobj=None, ordered=None):
    """Returns a list of (paramid, param) from `params`, ordered by dependencies.

    This uses the `depends_on` attributes from the associated
    ParamDecl's.  When heavyweight parameters (Param instances) are
    used, the ParamDecl is readily accessible from the param itself,
    but when bare values are used in `params`, a reference object must
    be passed as `refobj` to get access to the ParamDecl's.
    """
    if not ordered:
        ordered = []
    if not params:
        return ordered
    newparams = dict(params) # FIXME not efficient at all
    for paramid, param in params.items():
        if not has_deps_not_in(param, ordered, refobj, paramid):
            ordered.append((paramid, param))
            del newparams[paramid]
    assert newparams != params, \
           "params have non-satisfiable depends_on: {0}".format(pretty(params))
    return dep_ordered_params(newparams, refobj=refobj, ordered=ordered)

def walk_params(params, parent_plugin, action_func, refobj=None):
    "Execute `action_func` for all (non-pinned) `params` of `parent_plugin`"
    for paramid, param in reversed(dep_ordered_params(params, refobj)):
        if param.decl.pinned_value:
            continue
        pinnedvalue_key = "X-Omaha-Parameter-" + paramid
        if parent_plugin and parent_plugin.has_datafield(pinnedvalue_key):
            param.value = parent_plugin.datafield(pinnedvalue_key)
            continue
        action_func(param)

def fresh_parameters_for_decls(decls, plugin_manager):
    return dict( (paramid, Param(decl, plugin_manager)) for paramid, decl
                 in decls.items() )

# Concrete parameter classes

class String(ParamDecl):
    def __init__(self, label, default="", **kwargs):
        assert isinstance(default, str)
        super(String, self).__init__(label, default, **kwargs)
    def cast(self, value):
        return str(value)

class Numeric(ParamDecl):
    def __init__(self, label, default, minval=None, maxval=None, **kwargs):
        super(Numeric, self).__init__(label, default, **kwargs)
        self.__min = minval
        self.__max = maxval
    def clonable_params(self):
        return super(Numeric, self).clonable_params() + [('minval', self.__min),
                                                         ('maxval', self.__max)]
    @property
    def min(self): return self.__min
    @min.setter
    def min(self, value):
        assert value <= self.__max
        self.__min = value
    @property
    def max(self): return self.__max
    @max.setter
    def max(self, value):
        assert value >= self.__min
        self.__max = value
    @property
    def has_min(self): return self.__min is not None
    @property
    def has_max(self): return self.__max is not None

class Int(Numeric):
    def __init__(self, label, default, **kwargs):
        assert isinstance(default, int)
        super(Int, self).__init__(label, default, **kwargs)
    def cast(self, value):
        return int(value)

class Float(Numeric):
    def __init__(self, label, default, **kwargs):
        assert isinstance(default, float)
        super(Float, self).__init__(label, default, **kwargs)
    def cast(self, value):
        return float(value)

class AlternativeBased(ParamDecl):
    __logger = logging.getLogger('Overlord.params.AlternativeBased')
    def __init__(self, label, alternatives, default=None, **kwargs):
        super(AlternativeBased, self).__init__(label, default=default, **kwargs)
        self.__alternatives = alternatives
    @property
    def alternatives(self): return self.__alternatives
    def default(self, plugin_manager):
        declared_default = super(AlternativeBased, self).default(plugin_manager)
        if  ((declared_default is not None) and
             (declared_default in self.__alternatives.values())):
            return declared_default
        self.__logger.warning('No sane default (%s) for "%s" (alternatives: %s)',
                              declared_default, self.label, self.__alternatives.values())
        return self.__alternatives.values()[0]

class Choice(AlternativeBased):
    def __init__(self, label, alternatives, default=None, subparams=None,
                 **kwargs):
        super(Choice, self).__init__(label, alternatives, default=default,
                                     **kwargs)
        self.__subparams = subparams or {}
    @property
    def subparams(self): return self.__subparams
    @property
    def has_subparams(self): return len(self.__subparams) != 0
    def cast(self, value):
        if value not in self.alternatives.values():
            raise LookupError('No such choice "%s"' % value)
        return value

class MultiChoice(AlternativeBased):
    def __init__(self, label, alternatives, default=None, **kwargs):
        super(MultiChoice, self).__init__(label, alternatives, default=default or [],
                                          **kwargs)
    def cast(self, values):
        for value in values:
            if value not in self.alternatives.values():
                raise LookupError('No such choice "%s"' % value)
        return values

class Plugin(ParamDecl):
    __logger = logging.getLogger('Overlord.params.Plugin')
    def __init__(self, label, plugintype, context, default=None,
                 pattern=plugin_pattern.Any, **kwargs):
        """A plugin to chose among plugintype, and matching pattern.
        """
        assert default is None or isinstance(default, str) or isinstance(default, unicode), \
               "%s (%s) is not a str/unicode" % (default, type(default))
        super(Plugin, self).__init__(label, default=default, **kwargs)
        self.__plugintype = plugintype
        self.__context = dict(context)
        self.__pattern = pattern
    @property
    def plugintype(self): return self.__plugintype
    @property
    def context(self): return self.__context
    @property
    def pattern(self): return self.__pattern

    def default(self, plugin_manager):
        declared_default = super(Plugin, self).default(plugin_manager)
        if declared_default is not None:
            # FIXME: should also check that this value passes pattern
            # (this is get_plugin's job)
            return plugin_manager.get_plugin(self.__plugintype,
                                             declared_default)

        try:
            return plugin_manager.find_plugin(self.__plugintype,
                                              pattern=self.__pattern)
        except NoSuchPlugin:
            self.__logger.error("found no %s plugin matching pattern %s",
                                self.__plugintype, self.__pattern)
            return None
    def cast(self, value):
        # FIXME: should check pattern
        if not isinstance(value, CorePlugin):
            raise LookupError('No such choice "%s"' % value)
        return value

    def __pretty__(self, indent, curindent):
        return "Plugin(%r, %r)" % (
            self.label, self.plugintype)
