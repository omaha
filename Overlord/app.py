# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .parametrizable import Parametrizable
from .misc import classproperty

__all__ = [ 'App' ]

class App(Parametrizable):
    "Main abstraction for an Overlord-enabled application."
    def __init__(self, prefs, plugin_manager=None, **kwargs):
        self.plugin_manager = plugin_manager
        self.prefs = prefs
        super(App, self).__init__(**kwargs)
        assert "app" not in self.context
        self.context["app"] = self
    @classproperty
    @classmethod
    def context_key(cls):
        return App
