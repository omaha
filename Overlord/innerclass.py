# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = ['OuterClass', 'fixup_innerclass_name']

import sys

class OuterClass(type):
    def __init__(cls, name, bases, attribs):
        inherited_inners = set().union(*[base.__innerclasses__ for base in bases
                                         if isinstance(base, OuterClass)])
        # check we don't violate type restriction
        for inherited_inner in inherited_inners:
            # the candidate that would be selected by python
            innerattr = getattr(cls, inherited_inner)

            if inherited_inner in attribs:
                for base in bases:
                    if hasattr(base, inherited_inner):
                        assert issubclass(innerattr, getattr(base, inherited_inner)), \
                            ("{subcls}.{inner} ({subinner}) is not a subclass of "
                             "{supercls}.{inner} ({superinner})").format(
                                 subcls=name, supercls=base, inner=inherited_inner,
                                 subinner=innerattr,
                                 superinner=getattr(base, inherited_inner))
            else:
                candidates = [getattr(base, inherited_inner) for base in bases
                              if hasattr(base, inherited_inner)]
                # check for clashes
                for c in candidates:
                    if not isinstance(c, type):
                        raise TypeError("None-class member conflicting with inner-class: %s" % (c,))
                # most specific subclasses in candidates
                shortlist = reduce(cls.__extend_shortlist, candidates, [])

                assert len(shortlist) == 1, \
                    ("{0}.{1} cannot be chosen in {2}, "
                     "no single one is most specific, "
                     "shortlist={3}").format(
                         name, inherited_inner, candidates, shortlist)

                # correct the candidate that would be selected by python if needed
                if innerattr is not shortlist[0]:
                    setattr(cls, inherited_inner, shortlist[0])

        super(OuterClass, cls).__init__(name, bases, attribs)

        try:
            new_inners = attribs['__innerclasses__']
        except KeyError:
            cls.__innerclasses__ = inherited_inners
        else:
            assert new_inners is cls.__innerclasses__
            cls.__innerclasses__ = inherited_inners.union(new_inners)
            # (python2) adjust the inner class' __name__
            for inner_name in new_inners:
                fixup_innerclass_name(cls, inner_name)

    @staticmethod
    def __extend_shortlist(classes, newclass):
        # only keep those newclass does not derive from
        surviving = [c for c in classes
                     if not issubclass(newclass, c)]
        # only add newclass if we don't have a derivative yet
        for c in surviving:
            if issubclass(c, newclass):
                break
        else:
            surviving.append(newclass)
        return surviving

def fixup_innerclass_name(cls, innername):
    "Fix name of inner class in python 2"
    if sys.version_info.major < 3:
        inner = getattr(cls, innername)
        # only fixup inner name if it's being defined here
        if inner.__name__ is innername and inner.__module__ is cls.__module__:
            inner.__name__ = "%s.%s" % (cls.__name__, innername)
