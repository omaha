# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'Preferences' ]

from . import params
from .misc import hashable

class Preferences(object):

    def __init__(self):
        self.__prefs = {}

    def register(self, context):
        """Register context."""
        if context not in self.__prefs:
            self.__prefs[context] = {}

    def pref_loaded(self, context, paramid):
        return (context in self.__prefs and
                paramid in self.__prefs[context])

    def record_param(self, context, paramid, param):
        """
        Record given param in specified context.  Any pre-existing
        entry is overwritten.
        """
        assert isinstance(param, params.Param)
        self.__prefs[context][paramid] = param

    def get(self, pzable):
        """Get preferences of given Parametrizable object."""
        return dict(self.__prefs[hashable(pzable.context)])
