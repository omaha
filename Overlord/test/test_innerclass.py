# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Overlord import innerclass
from Overlord.test.lib import known_failure
from nose.tools import ok_, assert_raises
import re

class test_basic(object):
    def setup(self):
        class A(object):
            __metaclass__ = innerclass.OuterClass
            __innerclasses__ = {'I'}
            class I(object):
                pass
        self.A = A
    def test_good(self):
        class B(self.A):
            class I(self.A.I):
                pass
    def test_bad(self):
        with assert_raises(AssertionError) as cm:
            class B(self.A):
                class I(object):
                    pass
        ex = cm.exception
        ok_(re.match(r"B\.I \(.*\) is not a subclass of ", ex.args[0]),
            "Innerclass should be forbidden not to be a specialized version"
            " of its parents")
    def test_empty(self):
        class C(self.A):
            pass

class test_multi(object):
    def setup(self):
        class A(object):
            __metaclass__ = innerclass.OuterClass
            __innerclasses__ = {'I'}
            class I(object):
                pass
        class B1(A):
            class I(A.I):
                pass
        class B2(A):
            class I(A.I):
                pass
        class C2(B2):
            class I(B2.I, B1.I):
                pass
        self.A, self.B1, self.B2, self.C2 = A, B1, B2, C2
    def test_good(self):
        class C(self.B1, self.B2):
            class I(self.B1.I, self.B2.I):
                pass
    def test_bad(self):
        with assert_raises(AssertionError) as cm:
            class C(self.B1, self.B2):
                class I(object):
                    pass
        ex = cm.exception
        ok_(re.match(r"C\.I \(.*\) is not a subclass of ", ex.args[0]),
            "Innerclass should be forbidden not to be a specialized version"
            " of its parents")
    def test_partiallybad(self):
        with assert_raises(AssertionError) as cm:
            class C(self.B1, self.B2):
                class I(self.B1):
                    pass
        ex = cm.exception
        ok_(re.match(r"C\.I \(.*\) is not a subclass of ", ex.args[0]),
            "Innerclass should be forbidden not to be a specialized version"
            " of its parents")
    def test_implicitlybad(self):
        with assert_raises(AssertionError) as cm:
            class C(self.B1, self.B2):
                pass
        ex = cm.exception
        ok_(ex.args[0].startswith("C.I cannot be chosen in "),
            "Inherited innerclass should be forbidden not to be a specialized version"
            " of all its parents")
    def test_implicitlygood(self):
        class D(self.B1, self.C2):
            pass
    def test_othergoodinherited(self):
        # mix with a class which does not have the inner class
        class O(object):
            pass
        class C(self.A, O):
            pass
    def test_othergoodoverload(self):
        # mix with a class which does not have the inner class
        class O(object):
            pass
        class C(self.A, O):
            class I(self.A.I):
                pass
    def test_otherclash(self):
        # mix with a class with a member conflicting with the inner class
        class O(object):
            def I(self):
                pass
        with assert_raises(TypeError) as cm:
            class C(self.A, O):
                pass
        ex = cm.exception
        ok_(ex.args[0].startswith("None-class member conflicting with inner-class"),
            "Name clash between inner-class and non-class should fail")

class test_complexmulti(object):
    def setup(self):
        class A(object):
            __metaclass__ = innerclass.OuterClass
            __innerclasses__ = {'I'}
            class I(object):
                pass
        class B1(A):
            class I(A.I):
                pass
        class B2(A):
            class I(A.I):
                pass
        class C1(B1):
            pass
        class C2(B2):
            pass
        class D(B1, B2):
            class I(B1.I, B2.I):
                pass
        self.A, self.B1, self.B2, self.C1, self.C2, self.D = A, B1, B2, C1, C2, D
    def test_implicitlygood(self):
        class E(self.C1, self.C2, self.D):
            pass

class test_chained(object):
    def setup(self):
        class A(object):
            __metaclass__ = innerclass.OuterClass
            __innerclasses__ = {'I'}
            class I(object):
                pass
        class B(A):
            __innerclasses__ = {'J'}
            class J(object):
                pass
        class C(B):
            pass
        self.A, self.B, self.C = A, B, C
    def test_good(self):
        class D(self.C):
            class I(self.A.I):
                pass
            class J(self.B.J):
                pass
    def test_bad_lower(self):
        with assert_raises(AssertionError) as cm:
            class D(self.C):
                class I(self.A.I):
                    pass
                class J(object):
                    pass
        ex = cm.exception
        ok_(re.match(r"D\.J \(.*\) is not a subclass of ", ex.args[0]),
            "Innerclass should be forbidden not to be a specialized version"
            " of its parents")
    def test_bad_upper(self):
        with assert_raises(AssertionError) as cm:
            class D(self.C):
                class I(object):
                    pass
                class J(self.B.J):
                    pass
        ex = cm.exception
        ok_(re.match(r"D\.I \(.*\) is not a subclass of ", ex.args[0]),
            "Innerclass should be forbidden not to be a specialized version"
            " of its parents")
