# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import Overlord
from Overlord.misc import classproperty
from Overlord.test.lib import known_failure, neq_
from nose.tools import raises, eq_

class rectangle(Overlord.Parametrizable):
    def __init__(self, plugin_manager=None, **kwargs):
        super(rectangle, self).__init__(**kwargs)
        self.plugin_manager = plugin_manager
    @classproperty
    @classmethod
    def context_key(cls):
        return rectangle

    @Overlord.parameter
    def width(cls, context):
        return Overlord.params.Int(label="Width",
                                   minval=0, maxval=100,
                                   default=10)
    @Overlord.parameter
    def height(cls, context):
        return Overlord.params.Int(label="Height",
                                   minval=0, maxval=100,
                                   default=10)

class square(rectangle):
    def set_params(self, params):
        super(square, self).set_params(params)
        if self.width != self.height:
            raise Overlord.ParameterError(
                "square cannot have inequal dimensions, {0} != {1}".format(self.width,
                                                                           self.height))

class test_simple(object):
    def setup(self):
        self.app = Overlord.App(prefs=Overlord.Preferences(),
                                parentcontext={})
        self.r1 = rectangle(parentcontext=self.app.context)
        self.s1 = square(parentcontext=self.app.context,
                         paramvalues=dict(width=20, height=20))
    def test_basics(self):
        eq_(self.r1.width, 10)
        eq_(self.r1.height, 10)
        self.r1.width = 15
        eq_(self.r1.width, 15)
    @known_failure
    @raises(Overlord.ParameterError)
    def test_outofbounds(self):
        self.r1.width = 101

    @raises(ValueError)
    def test_badtype(self):
        self.r1.width = "junk"

    def test_setparams_bare(self):
        self.r1.set_params(dict(width=18))
        eq_(self.r1.width, 18)

    @raises(Overlord.ParameterError)
    def test_create_inconsistent(self):
        ctx = dict(self.app.context)
        ctx['test'] = 'test_create_inconsistent'
        square(parentcontext=ctx,
               paramvalues=dict(width=20, height=25))
    @raises(Overlord.ParameterError)
    def test_violate_constraint(self):
        self.s1.width += 1

    def test_clone(self):
        self.r1.width = 15
        r2 = self.r1.clone()
        eq_(r2.width, self.r1.width)
        eq_(r2.height, self.r1.height)

    # FIXME: shows that params in prefs are shared, not cloned
    @known_failure
    def test_clone_decoupling(self):
        r2 = self.r1.clone()
        self.r1.height = 20
        neq_(r2.height, self.r1.height)
