# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Overlord import params
from Overlord.test.lib import known_failure
from nose.tools import raises

class test_string(object):
    def setup(self):
        self.decl = params.String(label="my string",
                                  default="nothing")
        self.p = params.Param(self.decl, plugin_manager=None)
    def test_default(self):
        assert self.p.value == "nothing"
    def test_set(self):
        self.p.value = "something"
        assert self.p.value == "something"

    # FIXME: should not stay
    def test_string_cast(self):
        self.p.value = 3
        assert self.p.value == "3"

class test_int_bounds(object):
    def setup(self):
        d = params.Int(label="i", default=0, minval=-10, maxval=100)
        self.p = params.Param(d, plugin_manager=None)
    def test_limits(self):
        self.p.value = -10
        self.p.value = 100
    @known_failure
    def test_offlimit_low(self):
        self.p.value = -11
        assert self.p.value != -11
    @known_failure
    def test_offlimit_high(self):
        self.p.value = 101
        assert self.p.value != 101
    def test_limitchange(self):
        self.p.decl.max = 50
        assert self.p.decl.max == 50
        #self.p.decl.min = 0
        #assert self.p.decl.min == 0
    @known_failure
    def test_limitchange_value(self):
        assert self.p.decl.max == 100
        self.p.value = 75
        self.p.decl.max = 50
        assert self.p.decl.max == 50
        assert self.p.value <= self.p.decl.max
    @known_failure
    def test_offlimitchange_max(self):
        self.p.decl.max = -100
        assert self.p.decl.max != -100

class test_choice(object):
    def setup(self):
        d = params.Choice(label="choice",
                          alternatives=dict(first=1, second=2, tenth=10),
                          default=2)
        self.p = params.Param(d, plugin_manager=None)
    def test_set(self):
        assert self.p.value == 2
        self.p.value = 1
        self.p.value = 10
    @raises(LookupError)
    def test_setbad(self):
        self.p.value = 3
