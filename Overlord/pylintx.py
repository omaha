# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# pylint plugin to teach Pylint/Astroid about (some) Overlord
# specificities

import astroid

def transform_function(node):
    # consider @Overlord.parameter as @classmethod
    if node.decorators:
        for d in node.decorators.nodes:
            #print iter(d.infer()).next().qname()
            #print [x.qname() for x in iter(d.infer())]
            if isinstance(d, astroid.Getattr):
                if d.attrname == 'parameter' and d.expr.name == 'Overlord':
                    node.type = 'classmethod'
    return node

def register(linter):
    astroid.MANAGER.register_transform(astroid.nodes.Function,
                                       transform_function)
