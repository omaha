# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Misc utility functions.
"""

import os, collections

def sign(n):
    """Return 0, 1 or -1 depending on the sign of n."""
    if n == 0: return 0
    if n < 0: return -1
    return 1


def class_fullname(cls):
    """Return the full name of the given class, including package name."""
    assert isinstance(cls, type), "{0!r} is not a type".format(cls)
    return "%s.%s" % (cls.__module__, cls.__name__)

def __is_exe_file(path):
    return os.path.exists(path) and os.access(path, os.X_OK)
def which(command):
    "Returns full path of command if found in PATH, or if valid absolute command."
    if os.path.isabs(command):
        if __is_exe_file(command):
            return command
        else:
            raise IOError("No such command %r" % (command,))
    for d in os.environ['PATH'].split(os.pathsep):
        fullname = os.path.join(d, command)
        if __is_exe_file(fullname):
            return fullname
    else:
        raise IOError("No such command %r in PATH" % (command,))

def pretty(obj, indent=2, curindent=0):
    def __prettylst(lst, start, end, indent, curindent):
        if len(lst) == 0:
            return start + " " + end
        if len(lst) == 1:
            return start + " " + lst[0] + " " + end
        return (start + "\n" +
                ",\n".join([ ' ' * (curindent+indent) + e for e in lst ]) +
                '\n' + ' ' * curindent + end)

    if hasattr(obj, "__pretty__"):
        return obj.__pretty__(indent, curindent)
    if isinstance(obj, dict):
        return __prettylst([ "%s: %s" % (pretty(k, indent=indent,
                                                curindent=curindent+indent),
                                         pretty(v, indent=indent,
                                                curindent=curindent+indent))
                             for k, v in obj.iteritems() ],
                           "{", "}", indent, curindent)
    if isinstance(obj, tuple):
        return __prettylst([ pretty(e, indent=indent,
                                    curindent=curindent+indent)
                             for e in obj ],
                           "(", (",)" if len(obj)==1 else ")"),
                           indent, curindent)
    if isinstance(obj, list):
        return __prettylst([ pretty(e, indent=indent,
                                    curindent=curindent+indent)
                             for e in obj ],
                           "[", "]", indent, curindent)
    # fallback to repr()
    return repr(obj)

def hashable(obj):
    if isinstance(obj, collections.Hashable):
        return obj
    if isinstance(obj, collections.Mapping):
        return frozenset([ (k, hashable(v)) for k, v in obj.iteritems() ])
    if isinstance(obj, set):
        return frozenset(obj)
    raise NotImplementedError("does not know how to make %r hashable" % obj)

def method_mro(obj, method_name):
    return [ (class_fullname(k) if method_name in k.__dict__
              else "(%s)" % class_fullname(k))
             for k in type(obj).mro() ]
def classmethod_mro(cls, method_name):
    return [ (class_fullname(k) if method_name in k.__dict__
              else "(%s)" % class_fullname(k))
             for k in cls.mro() ]

# from http://stackoverflow.com/questions/128573/using-property-on-classmethods
# solution by Jason R. Coombs
class classproperty(property):
    def __get__(self, cls, owner):
        return self.fget.__get__(None, owner)()

try:
    from importlib import import_module
except ImportError:
    def import_module(modulename):
        """__import__ a module and return it."""
        try:
            pkg = __import__(modulename)
            for namepart in modulename.split('.')[1:]:
                pkg = getattr(pkg, namepart)
        except ImportError as ex:
            raise RuntimeError("package '%s' failed to load: %s" %
                               (modulename, ex))
        except AttributeError as ex:
            raise RuntimeError("package '%s' failed to load: %s" %
                               (modulename, ex))
        except ValueError as ex:
            raise RuntimeError("package '%s' failed to load: %s" %
                               (modulename, ex))

        return pkg
