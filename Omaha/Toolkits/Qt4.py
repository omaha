# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Core import UnsatisfiableConstraint
import Overlord
from Overlord.misc import pretty
import sys, os, logging, math

import sip
sip.setapi('QString', 2)
sip.setapi('QVariant', 2)
from PyQt4 import QtCore, QtGui, QtSvg

logger = logging.getLogger('Omaha.Toolkit.Qt4')

class BitmapImage(Core.UI.BitmapImage):
    def __init__(self, width, height):
        assert width > 0 and height > 0
        self.__img = QtGui.QImage(width, height, QtGui.QImage.Format_ARGB32)
        assert self.__img.paintEngine()
        self.__img.fill(QtGui.QColor(0, 0, 0, alpha=0).rgba())
    @property
    def _img(self):
        return self.__img
    @property
    def width(self):
        return self.__img.width()
    @property
    def height(self):
        return self.__img.height()

    def draw_sized_text(self, x, y, width, height,
                        text, color=None, bold=False, rotation=None):
        assert width > 0 and height > 0
        painter = QtGui.QPainter(self.__img)

        if color:
            painter.setPen(color)

        f = QtGui.QFont()
        f.setBold(bold)
        painter.setFont(f)

        # get size of default-size rendering of text
        alignment = QtCore.Qt.AlignCenter | QtCore.Qt.AlignHCenter
        markup_rect = painter.boundingRect(x, y, width, height, alignment, text)

        # determine scaling factor to get into requested bounding-box
        scale = min(float(width) / markup_rect.width(),
                    float(height) / markup_rect.height())

        if rotation:
            rx, ry, angle = rotation
            painter.translate(rx, ry)
            painter.rotate(angle * 180 / math.pi)
            painter.translate(-rx, -ry)

        # render using computed scale
        center = QtCore.QPointF(x + width/2, y + height/2)
        painter.translate(center)
        painter.scale(scale, scale)
        painter.translate(-center)

        painter.drawText(markup_rect, alignment, text)

class VectorImage(Core.UI.VectorImage):
    def __init__(self, svgfilename):
        if not os.path.exists(svgfilename):
            raise RuntimeError("File not found '%s'" % svgfilename)
        import rsvg
        self.__svg = QtSvg.QSvgRenderer(svgfilename)
    @property
    def width(self):
        return self.__svg.defaultSize().width()
    @property
    def height(self):
        return self.__svg.defaultSize().height()
    def render_to_bitmap(self, bitmap, x, y, width, height,
                         rotation=None):
        """
        Render vector graphics in bbox, after possible rotation.
        """
        painter = QtGui.QPainter(bitmap._img)
        if rotation:
            rx, ry, angle = rotation
            painter.translate(rx, ry)
            painter.rotate(angle * 180 / math.pi)
            painter.translate(-rx, -ry)
        self.__svg.render(painter, QtCore.QRectF(x, y, width, height))

class Canvas(QtGui.QWidget):
    def __init__(self, app_ui, default_size, *args, **kwargs):
        super(Canvas, self).__init__(*args, **kwargs)
        self.app_ui = app_ui
        assert isinstance(default_size, QtCore.QSize)
        self.__default_size = default_size

        self.__mouse_press_callbacks = []
        self.__mouse_release_callbacks = []
        self.__mouse_move_callbacks = []

    def paintEvent(self, event):
        self.app_ui.game_renderer.draw()

    def mousePressEvent(self, event):
        for cb in self.__mouse_press_callbacks:
            if cb(event.x(), event.y()):
                self.update()
                break
    def mouseReleaseEvent(self, event):
        for cb in self.__mouse_release_callbacks:
            if cb(event.x(), event.y()):
                self.update()
                break
    def mouseMoveEvent(self, event):
        pressed = (event.buttons() & QtCore.Qt.LeftButton)
        for cb in self.__mouse_move_callbacks:
            if cb(event.x(), event.y(), pressed):
                self.update()
                break
    def sizeHint(self):
        return self.__default_size

    def set_mouse_press_callback(self, callback):
        self.__mouse_press_callbacks.append(callback)
    def unset_mouse_press_callback(self, callback):
        self.__mouse_press_callbacks.remove(callback)

    def set_mouse_release_callback(self, callback):
        self.__mouse_release_callbacks.append(callback)
    def unset_mouse_release_callback(self, callback):
        self.__mouse_release_callbacks.remove(callback)

    def set_mouse_move_callback(self, callback):
        self.__mouse_move_callbacks.append(callback)
    def unset_mouse_move_callback(self, callback):
        self.__mouse_move_callbacks.remove(callback)

class Qt4StringWidget(Core.UI.ParamWidget):
    def widget(self):
        return QtGui.QLineEdit(self.param.value)
    def retrieve_value(self, entry=None):
        pass

class Qt4IntWidget(Core.UI.ParamWidget):
    def widget(self):
        self.__spinbox = QtGui.QSpinBox()
        self.__spinbox.valueChanged.connect(self.__set_value)
        self.update()
        return self.__spinbox
    def update(self):
        if self.param.decl.has_min:
            self.__spinbox.setMinimum(self.param.decl.min)
        if self.param.decl.has_max:
            self.__spinbox.setMaximum(self.param.decl.max)
        self.__spinbox.setValue(self.param.value)
    @QtCore.pyqtSlot(int)
    def __set_value(self, value):
        self.param.value = value
    def retrieve_value(self, entry=None):
        # already there
        pass

class Qt4FloatWidget(Core.UI.ParamWidget):
    def widget(self):
        self.__spinbox = QtGui.QDoubleSpinBox()
        self.__spinbox.valueChanged.connect(self.__set_value)
        self.update()
        return self.__spinbox
    def update(self):
        self.__spinbox.setValue(self.param.value)
    @QtCore.pyqtSlot(int)
    def __set_value(self, value):
        self.param.value = value
    def retrieve_value(self, entry=None):
        # already there
        pass

class Qt4ChoiceWidget(Core.UI.ChoiceWidget):
    def widget(self):
        # tell early when we have nothing to choose from
        if len(self.param.decl.alternatives) == 0:
            raise UnsatisfiableConstraint('No choice for "%s".' %
                                          self.param.decl.label)

        self.__combo = QtGui.QComboBox()

        vbox = QtGui.QWidget()
        layout = QtGui.QVBoxLayout(vbox)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__combo)

        # insert items while detecting the index of the current one
        current_value = self.param.value
        for i, (choice, value) in enumerate(self.param.decl.alternatives.items()):
            item = self.__combo.addItem(choice, self.param)
            if value is None:
                item = self.__combo.model().item(self.__combo.count() - 1)
                item.setFlags(item.flags() & ~(QtCore.Qt.ItemIsSelectable |
                                               QtCore.Qt.ItemIsEnabled))
            if current_value == value:
                # found current, make it active
                self.__combo.setCurrentIndex(i)
        self.__combo.currentIndexChanged[str].connect(self.__set_value)

        self._add_subparams(self.__combo, layout)

        return vbox

    def _update_one_subparam_visibility(self, combo, parent_choice, subparam_box, subparam):
        if parent_choice == unicode(combo.currentText()):
            subparam_box.show()
        else:
            subparam_box.hide()

    @QtCore.pyqtSlot(str)
    def __set_value(self, choice):
        self.param.value = self.param.decl.alternatives[choice]
    def retrieve_value(self, entry=None):
        # already there
        pass

    def connect_activated(self, handler):
        self.__combo.activated.connect(handler)

class Qt4PluginChoiceWidget(Core.UI.PluginChoiceWidget):
    def widget(self):
        w = self._choice_widget.widget()
        self._choice_widget.connect_activated(self.retrieve_value)
        return w
    def retrieve_value(self, combo=None):
        self._choice_widget.retrieve_value() # FIXME: now useless ?
        self.param.value = self._choice_widget.param.value


_app = None

class Qt4(Core.UI.Toolkit):
    """
    Omaha abstraction of the Qt4 toolkit.
    """

    BitmapImage = BitmapImage
    VectorImage = VectorImage

    # parameter handling
    RawLabelWidget = QtGui.QLabel
    StringWidget = Qt4StringWidget
    IntWidget = Qt4IntWidget
    FloatWidget = Qt4FloatWidget
    ChoiceWidget = Qt4ChoiceWidget
    PluginChoiceWidget = Qt4PluginChoiceWidget

    def __init__(self, cli_args):
        super(Qt4, self).__init__()
        global _app
        if not _app:
            _app = QtGui.QApplication([sys.argv[0]] + cli_args)
        self.__dialogs = [] # running dialogs
        self.__event_sources = {}
        self.__streams = {}

    def prepend_icon_search_path(self, path):
        pass # FIXME

    # generic GUI elements

    def notify(self, title, text):
        QtGui.QMessageBox.information(None, title, text)

    def select_load_file(self):
        return QtGui.QFileDialog.getOpenFileName(None, "Load game")

    def open_params_dialog(self, title, params, app_ui, accept_cb=None,
                           parent_plugin=None, parent_window=None,
                           destroy=True, synchronous=False):
        dlg = QtGui.QDialog(parent_window)
        layout = QtGui.QVBoxLayout(dlg)

        butbox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |
                                        QtGui.QDialogButtonBox.Cancel)
        butbox.accepted.connect(dlg.accept)
        butbox.rejected.connect(dlg.reject)

        paramwidgets = {}
        def walk_action(param):
            hbox, paramwidgets[param] = self.create_param_widget(param, app_ui, params)
            layout.addWidget(hbox)
        Overlord.walk_params(params, parent_plugin, walk_action)
        requires_interaction = (len(paramwidgets) > 0)

        def __response_handler(accepted):
            if not accepted:
                logger.debug("cancelled")
                return

            # Retrieve values from Widget objects, for params which
            # were allocated a Widget.
            for param in params.values():
                if param in paramwidgets:
                    paramwidgets[param].retrieve_value()

            if accept_cb:
                try:
                    accept_cb(params)
                except Overlord.ParameterError as ex:
                    # FIXME: should notify user
                    logger.error("Bad parameter value: {0}".format(ex))
                    return

        if requires_interaction:
            # Keep a ref to the dialog to avoid it being gc'd if the
            # return value is not kept, and remove it when it gets closed.
            self.__dialogs.append(dlg)
            def __forget_dialog():
                self.__dialogs.remove(dlg)
            dlg.finished.connect(__forget_dialog)

            dlg.finished.connect(__response_handler)
            layout.addWidget(butbox)
            if synchronous:
                response = dlg.exec_()
                return { 1: True,
                         0: False }[response]
            else:
                dlg.show()
        else:
            __response_handler(True)

        return dlg

    def create_param_widget(self, param, app_ui, params):
        """Create widget for editing param in the context of app_ui.

        Both are included in a frame, which is returned so caller can place
        it where it wants.  Returns a tuple of frame and ParamWidget instance.
        """
        assert isinstance(param, Overlord.Param)
        box = QtGui.QGroupBox(param.label)
        w = self._create_widget(param, app_ui, params)
        layout = QtGui.QVBoxLayout(box)
        layout.addWidget(w.widget())

        return box, w

    # FIXME: hackish things
    @staticmethod
    def _vbox_add(vbox, widget):
        vbox.addWidget(widget)
    @staticmethod
    def _combo_setchanged_hook(combo, callback, param):
        def wrapped_callback(index):
            callback(combo, combo.itemData(index, QtCore.Qt.UserRole))
        combo.activated.connect(wrapped_callback)

    ## main event loop

    def run(self):
        sys.exit(QtGui.qApp.exec_())

    def __set_file_event_callback(self, stream, event, callback):
        assert callback not in self.__event_sources
        self.__streams[stream.fileno()] = stream
        def wrapped_callback(fd):
            callback(self.__streams[fd])
        notif = QtCore.QSocketNotifier(stream.fileno(),#gasp
                                       event)
        notif.activated.connect(wrapped_callback)
        self.__event_sources[callback] = (notif, wrapped_callback)
    def __unset_file_event_callback(self, callback):
        notif, wrapped_callback = self.__event_sources[callback]
        notif.activated.disconnect(wrapped_callback)
        del self.__event_sources[callback]

    def set_stream_read_callback(self, stream, callback):
        self.__set_file_event_callback(
            stream, QtCore.QSocketNotifier.Read, callback)
    def unset_stream_read_callback(self, callback):
        self.__unset_file_event_callback(callback)

    def set_stream_error_callback(self, stream, callback):
        self.__set_file_event_callback(
            stream, QtCore.QSocketNotifier.Exception, callback)
    def unset_stream_error_callback(self, callback):
        self.__unset_file_event_callback(callback)

    def set_timer_callback(self, timeout, callback):
        timer = QtCore.QTimer()
        timer.timeout.connect(callback)
        self.__event_sources[callback] = timer
        timer.start(timeout * 1000)
    def unset_timer_callback(self, callback):
        self.__event_sources[callback].stop()
        del self.__event_sources[callback]

    ## canvas interaction

    def set_mouse_press_callback(self, canvas, callback):
        canvas.set_mouse_press_callback(callback)
    def unset_mouse_press_callback(self, canvas, callback):
        canvas.unset_mouse_press_callback(callback)

    def set_mouse_release_callback(self, canvas, callback):
        canvas.set_mouse_release_callback(callback)
    def unset_mouse_release_callback(self, canvas, callback):
        canvas.unset_mouse_release_callback(callback)

    def set_mouse_move_callback(self, canvas, callback):
        canvas.set_mouse_move_callback(callback)
    def unset_mouse_move_callback(self, canvas, callback):
        canvas.unset_mouse_move_callback(callback)

    ## canvas drawing

    def canvas_drawing_context(self, canvas):
        #return QtGui.QPainter(canvas)
        return canvas

    def clear(self, canvas):
        painter = QtGui.QPainter(canvas)
        painter.eraseRect(0, 0, canvas.width(), canvas.height())

    def color(self, canvas, colorname):
        return QtGui.QColor(colorname)

    def draw_line(self, canvas, x0, y0, x1, y1, color, **attrs):
        painter = QtGui.QPainter(canvas)
        painter.drawLine(x0, y0, x1, y1)
    def draw_rectangle(self, canvas, x, y, w, h, color, **attrs):
        painter = QtGui.QPainter(canvas)
        painter.drawRect(x, y, w, h)
    def fill_rectangle(self, canvas, x, y, w, h, color, **attrs):
        painter = QtGui.QPainter(canvas)
        painter.fillRect(x, y, w, h, QtGui.QColor(color))
    def draw_circle(self, canvas, center_x, center_y, radius, color, **attrs):
        painter = QtGui.QPainter(canvas)
        painter.drawEllipse(QtCore.QPointF(center_x, center_y), radius, radius)
    def fill_circle(self, canvas, center_x, center_y, radius, color, **attrs):
        path = QtGui.QPainterPath()
        path.addEllipse(QtCore.QPointF(center_x, center_y), radius, radius)
        painter = QtGui.QPainter(canvas)
        painter.fillPath(path, QtGui.QColor(color))
    def draw_polygon(self, canvas, pointlist, color, **attrs):
        painter = QtGui.QPainter(canvas)
        painter.drawPolygon(QtGui.QPolygonF([QtCore.QPointF(x, y) for x, y in pointlist]))
    def fill_polygon(self, canvas, pointlist, color, **attrs):
        path = QtGui.QPainterPath()
        path.addPolygon(QtGui.QPolygonF([QtCore.QPointF(x, y) for x, y in pointlist]))
        painter = QtGui.QPainter(canvas)
        painter.fillPath(path, QtGui.QColor(color))

    def draw_centered_text(self, drawing_context,
                           x, y, text, color=None, bold=False):
        path = QtGui.QPainterPath()
        path.addText(0, 0, QtGui.qApp.font(), text)
        markup_width, markup_height = path.boundingRect().getRect()[2:4]
        painter = QtGui.QPainter(drawing_context)
        # FIXME: inefficient, we already have a rendering in path
        painter.drawText(x - markup_width // 2, y + markup_height // 2, text)

    def draw_image(self, canvas, image, x, y):
        painter = QtGui.QPainter(canvas)
        painter.drawImage(QtCore.QPointF(x, y), image._img)

    def invalidate_canvas(self, canvas):
        canvas.update()
