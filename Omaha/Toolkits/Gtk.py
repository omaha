# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Core import UnsatisfiableConstraint
import Overlord
from Overlord.misc import pretty

import gtk, gobject, cairo, pangocairo, glib
import os, math, logging
from functools import wraps

logger = logging.getLogger('Omaha.Toolkit.Gtk')

class BitmapImage(Core.UI.BitmapImage):
    def __init__(self, width, height):
        self.__img = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
    @property
    def _img(self):
        return self.__img
    @property
    def width(self):
        return self.__img.get_width()
    @property
    def height(self):
        return self.__img.get_height()

    def draw_sized_text(self, x, y, width, height,
                        text, color=None, bold=False, rotation=None):
        cairo_context = pangocairo.CairoContext(cairo.Context(self._img))
        pango_layout = cairo_context.create_layout()
        _pango_layout_settext(pango_layout, text, color, bold)

        # get size of default-size rendering of text
        markup_rect = pango_layout.get_pixel_extents()[1]
        markup_width, markup_height = markup_rect[2:4]

        # determine scaling factor to get into requested bounding-box
        scale = min(float(width) / markup_width, float(height) / markup_height)

        # center text inside bounding box
        x += (width - scale * markup_width) / 2
        y += (height - scale * markup_height) / 2

        if rotation is not None:
            Gtk.context_rotate(cairo_context, rotation)

        # render using computed position and scale
        cairo_context.move_to(x, y)
        cairo_context.scale(scale, scale)
        cairo_context.show_layout(pango_layout)

class VectorImage(Core.UI.VectorImage):
    def __init__(self, svgfilename):
        if not os.path.exists(svgfilename):
            raise RuntimeError("File not found '%s'" % svgfilename)
        import rsvg             # FIXME: performance impact ?
        self.__svg = rsvg.Handle(svgfilename)
    @property
    def width(self):
        return self.__svg.props.width
    @property
    def height(self):
        return self.__svg.props.height
    def render_to_bitmap(self, bitmap, x, y, width, height,
                         rotation=None):
        """
        Render vector graphics in bbox, after possible rotation.
        """
        scale = min(float(width) / self.width, float(height) / self.height)
        ctx = cairo.Context(bitmap._img)
        if rotation is not None:
            Gtk.context_rotate(ctx, rotation)
        ctx.scale(scale, scale)
        ctx.translate(x, y)
        self.__svg.render_cairo(ctx)

def _pango_layout_settext(pango_layout, text, color=None, bold=False):
    """Set text in pango layout with given attributes."""
    if color is not None:
        text = '<span foreground="%s">%s</span>' % (color, text)
    if bold:
        text = "<b>%s</b>" % text

    # compute text rendering
    pango_layout.set_markup(text)

class GtkStringWidget(Core.UI.ParamWidget):
    def widget(self):
        self.__entry = gtk.Entry()
        self.__entry.set_text(self.param.value)
        self.__entry.set_property('activates-default', True)
        self.__entry.connect("changed", self.retrieve_value)
        self.__entry.show_all()
        return self.__entry
    def retrieve_value(self, entry=None):
        self.param.value = self.__entry.get_text()

class GtkIntWidget(Core.UI.ParamWidget):
    def widget(self):
        adj = gtk.Adjustment(step_incr=1, page_incr=2)
        self.__spinbtn = gtk.SpinButton(adjustment=adj)
        self.__spinbtn.set_numeric(True)
        self.__spinbtn.set_property('activates-default', True)
        self.__spinbtn.connect("value-changed", self.retrieve_value)
        self.update()
        self.__spinbtn.show_all()
        return self.__spinbtn
    def update(self):
        adj = self.__spinbtn.get_adjustment()
        if self.param.decl.has_min:
            adj.set_lower(self.param.decl.min)
        if self.param.decl.has_max:
            adj.set_upper(self.param.decl.max)
        adj.set_value(self.param.value)
    def retrieve_value(self, spinbtn=None):
        self.param.value = self.__spinbtn.get_value_as_int()

class GtkIntWidget2(Core.UI.ParamWidget):
    def widget(self):
        self.__entry = gtk.Entry()
        self.__entry.set_text(
            "%d" % self.param.value)
        self.__entry.set_property('activates-default', True)
        self.__entry.connect("changed", self.retrieve_value)
        self.__entry.show_all()
        return self.__entry
    def retrieve_value(self, entry=None):
        try:
            self.param.value = int(self.__entry.get_text())
        except ValueError as ex:
            raise Overlord.ParameterError(str(ex))

class GtkFloatWidget(Core.UI.ParamWidget):
    def widget(self):
        self.__entry = gtk.Entry()
        self.__entry.set_text(
            "%g" % self.param.value)
        self.__entry.set_property('activates-default', True)
        self.__entry.connect("changed", self.retrieve_value)
        self.__entry.show_all()
        return self.__entry
    def retrieve_value(self, entry=None):
        try:
            self.param.value = float(self.__entry.get_text())
        except ValueError as ex:
            raise Overlord.ParameterError(str(ex))

class GtkChoiceWidget(Core.UI.ChoiceWidget):
    def widget(self):
        # tell early when we have nothing to choose from
        if len(self.param.decl.alternatives) == 0:
            raise UnsatisfiableConstraint('No choice for "%s".' %
                                          self.param.decl.label)

        self.__liststore = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_BOOLEAN)
        self.__combo = gtk.ComboBox(self.__liststore)
        cell = gtk.CellRendererText()
        self.__combo.pack_start(cell, True)
        self.__combo.add_attribute(cell, 'text', 0)
        self.__combo.add_attribute(cell, 'sensitive', 1)

        vbox = gtk.VBox()
        vbox.pack_start(self.__combo, expand=False)

        # insert items while detecting the index of the current one
        current_value = self.param.value
        for i, (choice, value) in enumerate(self.param.decl.alternatives.items()):
            item = self.__liststore.append([choice, True])
            if value is None:
                self.__liststore.set(item, 1, False)
            if current_value == value:
                # found current, make it active
                self.__combo.set_active(i)
        self.__combo.connect("changed", self.retrieve_value)
        self.__combo.show_all()

        self._add_subparams(self.__combo, vbox)

        return vbox

    def _update_one_subparam_visibility(self, combo, parent_choice, subparam_box, subparam):
        if parent_choice == unicode(combo.get_active_text()):
            subparam_box.show()
        else:
            subparam_box.hide()

    def retrieve_value(self, combo=None):
        # workaround pygtk 2.16.0 bug
        # see: https://bugzilla.gnome.org/show_bug.cgi?id=600617
        active_text = self.__combo.get_active_text()
        if type(active_text) is str:
            active_text = unicode(active_text)
        self.param.value = self.param.decl.alternatives[active_text]
        self._retrieve_subparam_values(active_text)

    def connect_changed(self, handler):
        self.__combo.connect("changed", handler)

class GtkPluginChoiceWidget(Core.UI.PluginChoiceWidget):
    def widget(self):
        w = self._choice_widget.widget()
        self._choice_widget.connect_changed(self.retrieve_value)
        return w
    def retrieve_value(self, combo=None):
        self._choice_widget.retrieve_value() # FIXME: now useless ?
        self.param.value = self._choice_widget.param.value

class GtkMultiChoiceWidget(Core.UI.ParamWidget):
    def widget(self):
        ## create the data model
        lstore = gtk.ListStore(gobject.TYPE_BOOLEAN,
                               gobject.TYPE_STRING)
        for choice in self.param.decl.alternatives.keys():
            itor = lstore.append()
            lstore.set(itor, 0, False, 1, choice)
        ## create the view
        # FIXME: put a ScrolledWindow around ?
        self.__treeview = gtk.TreeView(lstore)
        ## create the columns
        # selected
        def __toggled(cell, path, lstore):
            """Toggle the boolean in the liststore."""
            itor = lstore.get_iter((int(path),))
            lstore.set(itor, 0, not lstore.get_value(itor, 0))
        renderer = gtk.CellRendererToggle()
        renderer.connect('toggled', __toggled, lstore)
        column = gtk.TreeViewColumn('Use', renderer, active=0)
        self.__treeview.append_column(column)
        # name
        column = gtk.TreeViewColumn('Player driver', # FIXME!
                                    gtk.CellRendererText(),
                                    text=1)
        self.__treeview.append_column(column)
        self.__treeview.show_all()
        return self.__treeview
    def retrieve_value(self, treeview=None):
        self.param.value = []
        def __handle_node(lstore, path, itor):
            """If node was selected, append its label to param.value."""
            if lstore.get_value(itor, 0):
                self.param.value.append(self.param.decl.alternatives[
                    unicode(lstore.get_value(itor, 1))])
        self.__treeview.get_model().foreach(__handle_node)

class Gtk(Core.UI.Toolkit):
    """
    Omaha abstraction of the Gtk toolkit.
    """

    BitmapImage = BitmapImage
    VectorImage = VectorImage

    # parameter handling
    RawLabelWidget = gtk.Label
    StringWidget = GtkStringWidget
    IntWidget = GtkIntWidget
    FloatWidget = GtkFloatWidget
    ChoiceWidget = GtkChoiceWidget
    MultiChoiceWidget = GtkMultiChoiceWidget
    PluginChoiceWidget = GtkPluginChoiceWidget

    def __init__(self):
        super(Gtk, self).__init__()
        self.__event_sources = {}

        self.__mouse_release_callbacks = {}
        self.__mouse_press_callbacks = {}
        self.__mouse_move_callbacks = {}
        self.__colors = {}

    def prepend_icon_search_path(self, path):
        gtk.icon_theme_get_default().prepend_search_path(path)

    # generic GUI elements

    def notify(self, title, text):
        dlg = gtk.Dialog("omaha - " + title,
                         buttons=(gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        dlg.vbox.pack_start(gtk.Label(text))
        dlg.vbox.show_all()
        dlg.run()
        dlg.destroy()

    def select_load_file(self):
        try:
            dlg = gtk.FileChooserDialog(
                title="omaha - load game",
                action=gtk.FILE_CHOOSER_ACTION_OPEN,
                buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                         gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
            answer = dlg.run()  # FIXME: this is modal
            if answer == gtk.RESPONSE_ACCEPT:
                return dlg.get_filename()
        finally:
            dlg.destroy()

    def open_params_dialog(self, title, params, app_ui, accept_cb=None,
                           parent_plugin=None, parent_window=None,
                           destroy=True, synchronous=False):
        dlg = gtk.Dialog(title,
                         buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT))
        btn_ok = dlg.add_button(gtk.STOCK_OK, gtk.RESPONSE_ACCEPT)
        dlg.set_default_response(gtk.RESPONSE_ACCEPT)

        paramwidgets = {}
        def walk_action(param):
            hbox, paramwidgets[param] = self.create_param_widget(param, app_ui, params)
            dlg.vbox.pack_start(hbox, expand=False)
        Overlord.walk_params(params, parent_plugin, walk_action)
        requires_interaction = (len(paramwidgets) > 0)

        def __response_handler(dlg, response_id,
                               params, paramwidgets, accept_cb,
                               parent_window, destroy):
            def __cleanup(dlg, parent_window):
                if destroy:
                    dlg.destroy()
                    if parent_window:
                        parent_window.set_sensitive(True)

            if response_id in (gtk.RESPONSE_REJECT, gtk.RESPONSE_DELETE_EVENT):
                logger.debug("cancelled")
                __cleanup(dlg, parent_window)
                return
            if response_id != gtk.RESPONSE_ACCEPT:
                __cleanup(dlg, parent_window)
                raise RuntimeError("gtk.Dialog responded unexpected %r" %
                                   gtk.ResponseType(response_id))

            # Retrieve values from Widget objects, for params which
            # were allocated a Widget.
            for param in params.values():
                if param in paramwidgets:
                    paramwidgets[param].retrieve_value()

            if accept_cb:
                try:
                    accept_cb(params)
                except Overlord.ParameterError as ex:
                    # FIXME: should notify user
                    logger.error("Bad parameter value: {0}".format(ex))
                    return

            __cleanup(dlg, parent_window)

        if requires_interaction:
            if parent_window:
                dlg.set_transient_for(parent_window)
            if synchronous:
                response_id = dlg.run()
                __response_handler(dlg, response_id,
                                   params, paramwidgets, accept_cb,
                                   parent_window, destroy)
                return {
                    gtk.RESPONSE_ACCEPT: True,
                    gtk.RESPONSE_REJECT: False,
                    gtk.RESPONSE_DELETE_EVENT: False,
                }[response_id]
            else:
                dlg.connect("response", __response_handler,
                            params, paramwidgets, accept_cb,
                            parent_window, destroy)
                dlg.show()
        else:
            __response_handler(dlg, gtk.RESPONSE_ACCEPT,
                               params, paramwidgets, accept_cb,
                               parent_window, destroy)

        return dlg

    def create_param_widget(self, param, app_ui, params):
        """Create widget for editing param in the context of app_ui.

        Both are included in a frame, which is returned so caller can place
        it where it wants.  Returns a tuple of frame and ParamWidget instance.
        """
        assert isinstance(param, Overlord.Param)
        box = gtk.Frame(param.label)
        w = self._create_widget(param, app_ui, params)
        wdg = w.widget()
        wdg.show()
        box.add(wdg)
        box.show()
        return box, w

    # FIXME: hackish things
    @staticmethod
    def _vbox_add(vbox, widget):
        vbox.pack_start(widget, expand=False)
    @staticmethod
    def _combo_setchanged_hook(combo, callback, param):
        combo.connect("changed", callback, param)

    ## main event loop

    def run(self):
        gtk.main()

    def set_stream_read_callback(self, stream, callback):
        assert callback not in self.__event_sources
        def wrapped_callback(stream, condition):
            callback(stream)
            return True
        self.__event_sources[callback] = \
            glib.io_add_watch(stream, glib.IO_IN, wrapped_callback)
    def unset_stream_read_callback(self, callback):
        glib.source_remove(self.__event_sources[callback])
        del self.__event_sources[callback]

    def set_stream_error_callback(self, stream, callback):
        assert callback not in self.__event_sources
        def wrapped_callback(stream, condition):
            callback(stream)
            return True
        self.__event_sources[callback] = \
            glib.io_add_watch(stream, glib.IO_NVAL|glib.IO_ERR|glib.IO_HUP,
                              wrapped_callback)
    def unset_stream_error_callback(self, callback):
        glib.source_remove(self.__event_sources[callback])
        del self.__event_sources[callback]

    def set_timer_callback(self, timeout, callback):
        assert callback not in self.__event_sources
        def wrapped_callback():
            callback()
            # do not autoremove
            return True
        self.__event_sources[callback] = \
            glib.timeout_add_seconds(timeout, wrapped_callback)
    def unset_timer_callback(self, callback):
        glib.source_remove(self.__event_sources[callback])
        del self.__event_sources[callback]

    ## canvas interaction

    def set_mouse_release_callback(self, canvas, callback):
        def wrapped_callback(w, evt):
            if callback(int(evt.x), int(evt.y)):
                canvas.stop_emission("button_release_event")
                self.invalidate_canvas(canvas)
        self.__mouse_release_callbacks[callback] = canvas.connect(
            "button_release_event", wrapped_callback)
    def unset_mouse_release_callback(self, canvas, callback):
        if callback not in self.__mouse_release_callbacks:
            logger.error("unset_mouse_release_callback: %s not in %s",
                         callback, self.__mouse_release_callbacks)
            return
        canvas.disconnect(self.__mouse_release_callbacks[callback])
        del self.__mouse_release_callbacks[callback]

    def set_mouse_press_callback(self, canvas, callback):
        def wrapped_callback(w, evt):
            if callback(int(evt.x), int(evt.y)):
                canvas.stop_emission("button_press_event")
                self.invalidate_canvas(canvas)
        self.__mouse_press_callbacks[callback] = canvas.connect(
            "button_press_event", wrapped_callback)
    def unset_mouse_press_callback(self, canvas, callback):
        if callback not in self.__mouse_press_callbacks:
            logger.error("unset_mouse_press_callback: %s not in %s",
                         callback, self.__mouse_press_callbacks)
            return
        canvas.disconnect(self.__mouse_press_callbacks[callback])
        del self.__mouse_press_callbacks[callback]

    def set_mouse_move_callback(self, canvas, callback):
        def wrapped_callback(w, evt):
            if evt.is_hint:
                x, y, state = evt.window.get_pointer()
            else:
                x, y, state = evt.x, evt.y, evt.state
            if callback(int(x), int(y),
                        pressed=(evt.state & gtk.gdk.BUTTON1_MASK)):
                canvas.stop_emission("motion_notify_event")
                self.invalidate_canvas(canvas)
        self.__mouse_move_callbacks[callback] = canvas.connect(
            "motion_notify_event", wrapped_callback)
    def unset_mouse_move_callback(self, canvas, callback):
        if callback not in self.__mouse_move_callbacks:
            logger.error("unset_mouse_move_callback: %s not in %s",
                         callback, self.__mouse_move_callbacks)
            return
        canvas.disconnect(self.__mouse_move_callbacks[callback])
        del self.__mouse_move_callbacks[callback]

    ## canvas drawing

    def canvas_drawing_context(self, canvas):
        return canvas.window.cairo_create()
    def canvas_begin_subcontext(self, parent):
        parent.save()
        return parent
    def canvas_end_subcontext(self, context):
        context.restore()

    @staticmethod
    def context_rotate(drawing_context, rotation):
        """
        Apply rotation around a point to drawing_context's CTM.

        Rotation parameter must be a tuple of (center_x, center_y, radians).
        """
        cx, cy, angle = rotation
        drawing_context.translate(cx, cy)
        drawing_context.rotate(angle)
        drawing_context.translate(-cx, -cy)
    @staticmethod
    def context_scale(drawing_context, factor, center):
        """
        Apply scaling around a point to drawing_context's CTM.

        Center parameter must be a tuple of (center_x, center_y).
        """
        cx, cy = center
        drawing_context.translate(cx, cy)
        drawing_context.scale(factor, factor)
        drawing_context.translate(-cx, -cy)

    def __context_set_attributes(self, cairo_context, attrs):
        # FIXME: barf on unsupported attributes
        if 'color' in attrs:
            c = attrs['color']
            cairo_context.set_source_rgb(c.red_float, c.green_float, c.blue_float)
        if 'width' in attrs:
            cairo_context.set_line_width(attrs['width'])

    def clear(self, canvas):
        canvas.window.clear()

    def color(self, canvas, colorname):
        if colorname not in self.__colors:
            self.__colors[colorname] = gtk.gdk.color_parse(colorname)
        return self.__colors[colorname]

    # This is not a method, but belongs to this namespace for access
    # to __context_set_attributes,
    def cairo_operation(func): # pylint: disable=no-self-argument
        "Decorator to spare cairo boilerplate code"
        assert callable(func)
        import inspect
        argspec = inspect.getargspec(func)
        assert argspec.args[0] == 'self'
        assert argspec.args[1] == 'drawing_context'
        assert argspec.keywords == 'attrs'
        @wraps(func)
        def wrapper(self, drawing_context, *args, **attrs):
            self.canvas_begin_subcontext(drawing_context)
            self.__context_set_attributes(drawing_context, attrs)
            func(self, drawing_context, *args, **attrs) # pylint: disable=not-callable
            self.canvas_end_subcontext(drawing_context)
        return wrapper

    @cairo_operation
    def draw_line(self, drawing_context, x0, y0, x1, y1, **attrs):
        drawing_context.move_to(x0, y0)
        drawing_context.line_to(x1, y1)
        drawing_context.stroke()
    @cairo_operation
    def draw_rectangle(self, drawing_context, x, y, w, h, **attrs):
        drawing_context.rectangle(x, y, w, h)
        drawing_context.stroke()
    @cairo_operation
    def fill_rectangle(self, drawing_context, x, y, w, h, **attrs):
        drawing_context.rectangle(x, y, w, h)
        drawing_context.fill()
    @cairo_operation
    def draw_circle(self, drawing_context, center_x, center_y, radius, **attrs):
        drawing_context.arc(center_x, center_y, radius,
                            0, math.radians(360))
        drawing_context.stroke()
    @cairo_operation
    def fill_circle(self, drawing_context, center_x, center_y, radius, **attrs):
        drawing_context.arc(center_x, center_y, radius,
                            0, math.radians(360))
        drawing_context.fill()
    @cairo_operation
    def draw_polygon(self, drawing_context, pointlist, **attrs):
        x0, y0 = pointlist[0]
        drawing_context.move_to(x0, y0)
        for x, y in pointlist[1:]:
            drawing_context.line_to(x, y)
        drawing_context.close_path()
        drawing_context.stroke()
    @cairo_operation
    def fill_polygon(self, drawing_context, pointlist, **attrs):
        x0, y0 = pointlist[0]
        drawing_context.move_to(x0, y0)
        for x, y in pointlist[1:]:
            drawing_context.line_to(x, y)
        drawing_context.close_path()
        drawing_context.fill()

    def draw_centered_text(self, drawing_context,
                           x, y, text, color=None, bold=False):
        pango_layout = drawing_context.create_layout()
        _pango_layout_settext(pango_layout, text, color, bold)
        markup_rect = pango_layout.get_pixel_extents()[1]
        markup_width, markup_height = markup_rect[2:4]

        # render at given position
        drawing_context.move_to(x - markup_width // 2, y - markup_height // 2)
        drawing_context.show_layout(pango_layout)

    def draw_image(self, drawing_context, image, x, y):
        drawing_context.set_source_surface(image._img, x, y)
        drawing_context.paint()
        drawing_context.stroke()

    def invalidate_canvas(self, canvas):
        w, h = canvas.window.get_size()
        canvas.window.invalidate_rect(gtk.gdk.Rectangle(0, 0, w, h), True)
