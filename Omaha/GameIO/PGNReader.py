# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Games.abstract import ChessGame
from Omaha.Games.abstract import StandardShogi
import Overlord

import logging
logger = logging.getLogger('Omaha.GameIO.PGNReader')

class PGNGameReader(Core.GameReader):
    suffix = ".pgn"

    def __init__(self, stream, plugin_manager, parentcontext):
        self.data = ''.join(stream.readlines())
        self.plugin_manager = plugin_manager
        self.context = dict(parentcontext)

        # FIXME: global stuff stinks - but pyparsing seems to have
        # problem with methods, and we must(?) use functions for
        # hooks, so...
        global pgn_contents
        pgn_contents = dict(tags={}, moves=[])

    def game(self):
        parse_pgn(self.data)
        tags = pgn_contents['tags'] # shortcut
        variant = tags['Variant'] if 'Variant' in tags else None
        logger.debug("variant %r", variant)
        try:
            gamekind, notationclass, init_game, inject_move = {
                None: ("Chess", ChessGame.SANNotation,
                       self.chess_init_game, self.chess_inject_move),
                "shogi": ("Shogi", StandardShogi.SANNotation,
                          self.chess_init_game, self.chess_inject_move),
                }[variant]
        except KeyError:
            raise Core.UserNotification(
                "error reading game file",
                "Game variant %r currently not supported by PGN reader" %
                (tags['Variant'],))

        game = self.plugin_manager.get_plugin('X-Omaha-Game',
                                              gamekind).Class(parentcontext=self.context,
                                                              import_mode=True)
        self.notation = notationclass(game)

        init_game(game, pgn_contents, self.context)
        game.setup()
        for move in pgn_contents['moves']:
            try:
                inject_move(game, move)
            except Core.ParseError as ex:
                # FIXME: notify asynchronously
                self.context["app"].tk.notify("error reading game file",
                                              "could not interpret next move: %s" % (ex,))
                break

        game.done_importing()
        return game

    def chess_init_game(self, game, pgn_contents, context):
        # setup game instance
        param_decls = game.parameters_dcl(context)
        params = {}
        for paramid, decl in param_decls.items():
            if paramid == 'handicap':
                # today, only for shogi games
                param = Overlord.Param(decl=decl,
                                       plugin_manager=None)
                params[paramid] = param
            else:
                raise AssertionError("Unknown parameter %r" % (paramid,))
        game.set_params(params)

    def chess_inject_move(self, game, move):
        logger.debug("chess_inject_move(%r)", move)
        game.make_move(
            game.whose_turn,
            self.notation.move_deserialization(move, player=game.whose_turn))

## parser

def record_tag(tokens):
    pgn_contents['tags'][tokens[1]] = tokens[2]
def record_move(tokens):
    # FIXME handle initial n... for black
    # FIXME check consistency of move numbers
    pgn_contents['moves'].append(tokens[0])

from pyparsing import Word, ZeroOrMore, OneOrMore, QuotedString, Optional
from pyparsing import alphanums, nums, printables, oneOf
from pyparsing import Regex

TagName = Word( alphanums + "_" )
TagValue = QuotedString(quoteChar='"', escChar='\\')
TagPair = ("[" + TagName + TagValue + "]").setParseAction(record_tag)
BraceComment = Regex(r"{[^}]*}")

Termination = oneOf('0-1 1-0 1/2-1/2 *')

MoveNum = Word( nums ) + ZeroOrMore(".")
Move = ( ~Termination + Optional(MoveNum) + ZeroOrMore(BraceComment) +
         Word( printables ).setParseAction(record_move) +
         ZeroOrMore(BraceComment) )

pgn_grammar = (ZeroOrMore(TagPair) + ZeroOrMore(BraceComment) +
               OneOrMore(Move) + Termination + ZeroOrMore(BraceComment))

def parse_pgn(data):
    pgn_grammar.parseString(data)
