# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Overlord.misc import class_fullname

from string import ascii_lowercase

class SGFGameWriter(Core.GameWriter):
    def __init__(self, game):
        if class_fullname(type(game)) not in sgfgames:
            raise RuntimeError("Unsupported game class %r" %
                               (class_fullname(type(game)),))
        self.__game = game

    def write_to(self, stream):
        gamecode, write_root, move_write = \
            sgfgames[class_fullname(type(self.__game))]
        stream.write("(;GM[%s]FF[4]\n" % (gamecode,))
        write_root(stream, self.__game)
        stream.write("\n")
        for move in self.__game.moves.played:
            stream.write(";")
            move_write(stream, move, self.__game)
        stream.write(")\n")

def go_write_root(stream, game):
    assert game.boardwidth == game.boardheight
    stream.write("SZ[%d]" % game.boardwidth)
    stream.write("KM[%g]" % game.komi)
    stream.write("HA[%d]" % game.handicap)

def go_move_write(stream, move, game):
    if move.player.color is game.COLOR_BLACK:
        movetag = 'B'
    else:
        movetag = 'W'
    stream.write("%s[%s]" % (movetag, sgf_serialize(move)))

# FIXME: share a notation class with Reader ?
def sgf_serialize(move):
    "Serialize a (go ?) sgf location"
    return "%s%s" % (ascii_lowercase[move.target.col],
                     ascii_lowercase[move.target.row])

sgfgames = {
    "Omaha.Games.Go.Go": ('1', go_write_root, go_move_write),
    # '2': "Othello"
    # '3': "Chess"
    # '6': "Backgammon"
    # '8': "Shogi"
    # '11': "Hex"
    # '18': "Amazons"
    # '39': "Gipf"
    }
