# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord

import re
from string import ascii_lowercase

class SGFGameReader(Core.GameReader):
    suffix = ".sgf"

    def __init__(self, stream, plugin_manager, parentcontext):
        self.data = ''.join(stream.readlines())
        self.plugin_manager = plugin_manager
        self.context = dict(parentcontext)

    def game(self):
        moves = main_variation(self.data)
        game_node, moves = moves[0], moves[1:]
        assert 'GM' in game_node

        if game_node['GM'] not in sgfgames:
            raise Core.UserNotification(
                "error reading game file",
                "Game type %r currently not supported by SGF reader" % (game_node['GM'],))
        gamekind, init_game, inject_move = sgfgames[game_node['GM']]
        game = self.plugin_manager.get_plugin('X-Omaha-Game',
                                              gamekind).Class(parentcontext=self.context,
                                                              import_mode=True)

        init_game(game, game_node, self.context)
        game.setup()
        for move in moves:
            inject_move(game, move)

        game.done_importing()
        return game

def go_init_game(game, game_node, context):
    # setup game instance
    param_decls = game.parameters_dcl(context)
    params = {}
    for paramid, decl in param_decls.items():
        if paramid == 'boardsize':
            param = Overlord.Param(decl=decl,
                                   plugin_manager=None)
            param.value = int(game_node['SZ'])
            params[paramid] = param
        elif paramid == 'handicap':
            param = Overlord.Param(decl=decl,
                                   plugin_manager=None)
            if 'HA' in game_node:
                param.value = int(game_node['HA'])
                # FIXME: should load stone positions
            else:
                param.value = 0
            params[paramid] = param
        elif paramid == 'komi':
            param = Overlord.Param(decl=decl,
                                   plugin_manager=None)
            param.value = float(game_node['KM'])
            params[paramid] = param
        else:
            raise AssertionError("Unknown parameter %r" % (paramid,))
    game.set_params(params)

def go_inject_move(game, move):
    if game.whose_turn.color is game.COLOR_BLACK:
        movetag = 'B'
    else:
        movetag = 'W'
    game.make_move(
        game.whose_turn,
        game.Move(target=sgf_deserialize(move[movetag],
                                         game)))

# FIXME: share a notation class with Writer ?
def sgf_deserialize(string, game):
    "Deserialize a (go ?) sgf location"
    m = re.match(r"([a-z])([a-z])$", string)
    if m:
        return game.board.Location(
            game.board,
            ascii_lowercase.index(m.group(1)),
            ascii_lowercase.index(m.group(2)))
    else:
        raise Core.ParseError("Could not parse '%s' as a serialized go move",
                              string)

def main_variation(data):
    """Raw naive parsing, ignoring '(' and whitespace, stopping at first ')'.

    That makes us ignore any secondary variation, but indeed, we stop
    at first thing we don't grok :}
    """
    nodes = []
    assert data.startswith('(;')
    data = data[2:]
    for node_data in data.split(';'):
        n = Node()
        while True:
            match = re.match(r'[\[\s]*([A-Z]+)\[([^\]]*)\]', node_data)
            if not match:
                break
            n[match.group(1)] = match.group(2)
            node_data = node_data[match.end():]
        nodes.append(n)
    return nodes

class Node(dict):
    pass

sgfgames = {
    '1': ("Go", go_init_game, go_inject_move),
    # '2': "Othello"
    # '3': "Chess"
    # '6': "Backgammon"
    # '8': "Shogi"
    # '11': "Hex"
    # '18': "Amazons"
    # '39': "Gipf"
    }
