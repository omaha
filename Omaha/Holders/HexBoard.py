# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.Euclidian2D import Euclidian2D
import Overlord

# constant denoting absence of location
# (not all squares of the square matrix are in the hexagon)
_NoLocation = "no loc"

class HexBoard(Euclidian2D):
    """
    Hexagonal board made of hexagonal tiles.

    The Hex board is stored into a square matrix, as rows shifted by
    progressive multiples of half squares.
    """

    def __init__(self, **kwargs):
        self.__hexes = None
        super(HexBoard, self).__init__(**kwargs)
        self.__piece_locations = {}

    def clone(self):
        c = super(HexBoard, self).clone()
        c.clear()
        for piece, location in self.__piece_locations.items():
            c.__insert_piece(c.Location(c, location.col, location.row), piece)
        return c

    @Overlord.parameter
    def width0(cls, context):
        return Overlord.params.Int(label = 'First width',
                                   default = 5,
                                   minval = 2, maxval = 99)
    @Overlord.parameter
    def width1(cls, context):
        return Overlord.params.Int(label = 'Second width',
                                   default = 5,
                                   minval = 2, maxval = 99)
    @Overlord.parameter
    def width2(cls, context):
        return Overlord.params.Int(label = 'Third width',
                                   default = 5,
                                   minval = 2, maxval = 99)

    @property
    def matrix_width(self):
        """Width of the storage matrix."""
        return self.width0 + self.width2 - 1

    @property
    def matrix_height(self):
        """Width of the storage matrix."""
        return self.width1 + self.width2 - 1

    def clear(self):
        """Set all board hexes as empty."""
        self.__hexes = []
        for vcol in range(self.matrix_width):
            self.__hexes.append([])
            for vrow in range(self.matrix_height):
                if vrow < self.width1 + vcol and vrow > vcol - self.width0:
                    self.__hexes[vcol].append(None)
                else:
                    self.__hexes[vcol].append(_NoLocation)

    @property
    def all_locations(self):
        for column in range(self.matrix_width):
            for row in range(self.matrix_height):
                if self.__hexes[column][row] is not _NoLocation:
                    yield self.Location(self, column, row)

    @property
    def leftmost_row(self):
        """Find the 1st row of hex that will touch the left side."""
        # iterate on the tiles in 1st column, last first
        for i in reversed(range(self.matrix_height)):
            if self.__hexes[0][i] is not _NoLocation:
                return i
    @property
    def topmost_col(self):
        """Find the 1st column of hex that will touch the top side."""
        # iterate on the tiles in 1st row, last first
        for i in reversed(range(self.matrix_width)):
            if self.__hexes[i][0] is not _NoLocation:
                return i

    def __ascii_dump(self):
        """For debugging or understanding the internal representation of
        the board."""
        for vcol in range(self.matrix_width):
            for vrow in range(self.matrix_height):
                if self.__hexes[vcol][vrow] is None:
                    # empty hex
                    print "O"
                else:
                    # not an hex
                    print "."

    def __insert_piece(self, location, piece):
        "Just update holder's attributes to hold the piece, without touching piece's attributes"
        self.__hexes[location.col][location.row] = piece
        self.__piece_locations[piece] = location
    def put_piece(self, location, piece):
        super(HexBoard, self).put_piece(location, piece)
        assert self.__hexes[location.col][location.row] is None
        self.__insert_piece(location, piece)
    def take_piece(self, location):
        piece = super(HexBoard, self).take_piece(location)
        self.__hexes[location.col][location.row] = None
        del self.__piece_locations[piece]
        return piece
    def replace_piece(self, location, piece):
        assert piece not in self.pieces
        oldpiece = self.piece_at(location)
        self.__insert_piece(location, piece)
        del self.__piece_locations[oldpiece]
        assert piece in self.pieces
        assert oldpiece not in self.pieces
        return oldpiece

    def piece_location(self, piece):
        """Returns location if piece is in board, else None."""
        if piece not in self.__piece_locations:
            return None
        return self.__piece_locations[piece]

    def valid_location(self, location):
        if location.holder is not self:
            return False
        if  (location.col >= self.matrix_width or location.col < 0 or
             location.row >= self.matrix_height or location.row < 0 or
             self.__hexes[location.col][location.row] is _NoLocation):
            return False
        return True

    def piece_at(self, location):
        if not self.valid_location(location):
            raise IndexError("No such position %s." % location)
        return self.__hexes[location.col][location.row]

    @property
    def pieces(self):
        return self.__piece_locations.keys()
