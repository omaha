# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Overlord.misc import sign

class _LocationDelta(object):
    def __init__(self, dcol, drow):
        self.dcol, self.drow = dcol, drow

    def __mul__(self, scalar):
        if not isinstance(scalar, int):
            return NotImplemented
        return _LocationDelta(scalar * self.dcol, scalar * self.drow)

    @property
    def normalized(self):
        """Normalize self to "one step"."""
        # Only implemented for orthogonal moves
        if self.dcol != 0 and self.drow != 0:
            raise NotImplementedError("Not orthogonal Delta.")
        return type(self)(sign(self.dcol), sign(self.drow))

    # FIXME: no support for non-vertical orientations
    def normalized_orientation(self, orientation):
        """
        Normalize self according to player orientation.
        """
        assert orientation[0] == 0 # temporary
        return type(self)(self.dcol * -orientation[1],
                          self.drow * orientation[1])
    def player_oriented(self, orientation):
        """
        Turn a normalized (player-oriented) delta (eg. taken from move
        definitions) into one applicable on the board on a piece of a
        player with given orientation.
        """
        # transformation is reversible as long as we only support
        # vertical orientations
        return self.normalized_orientation(orientation)

class Euclidian2DLocation(Core.PieceHolder.Location):
    Delta = _LocationDelta
    def __init__(self, holder, col, row):
        # FIXME: assert holder is instance of Euclidian2DLocation
        super(Euclidian2DLocation, self).__init__(holder)
        self.col = col
        self.row = row
    def __str__(self):
        return "(%d,%d)" % (self.col, self.row)
    def __hash__(self):
        "Return unique value for identical locations."
        return (self.col, self.row).__hash__()
    def __eq__(self, other):
        if type(other) is not type(self):
            return NotImplemented
        return self.col == other.col and self.row == other.row
    def __ne__(self, other):
        if type(other) is not type(self):
            return NotImplemented
        return self.col != other.col or self.row != other.row

    def __sub__(self, other):
        if not isinstance(other, Euclidian2DLocation):
            return NotImplemented
        return self.Delta(self.col - other.col, self.row - other.row)
    def __add__(self, other):
        if not isinstance(other, self.Delta):
            return NotImplemented
        return Euclidian2DLocation(self.holder,
                                   self.col + other.dcol,
                                   self.row + other.drow)

class Euclidian2D(Core.PieceHolder):
    """
    Base class for 2D matrix-based boards.
    """
    @property
    def matrix_width(self):
        raise NotImplementedError()
    @property
    def matrix_height(self):
        raise NotImplementedError()

    Location = Euclidian2DLocation

    # FIXME: does not deal correctly with "no move"
    def move_follows_direction(self, move, direction, orientation):
        """
        Checks whether the given move follows the given direction, for
        the specified player orientation.
        """
        assert move.source.holder is self
        assert move.target.holder is self

        #print("checking %s against direction %s" % (move, direction))
        delta = (move.target - move.source).normalized_orientation(orientation)
        # shortcuts for readability
        dx, dy = delta.dcol, delta.drow

        def _samesign(x, y):
            return not (x > 0) ^ (y > 0)

        # vertical
        if direction[0] == 0:
            if dx != 0:
                return False
            return _samesign(dy, direction[1])
        elif dx == 0:
            return False

        # here we already know there is a non-zero horiz component,
        # filter by sign early
        if not _samesign(dx, direction[0]):
            return False

        # horizontal
        if direction[1] == 0:
            return dy == 0 # dx sign already checked
        elif dy == 0:
            return False

        return ((dx % direction[0] == 0) and
                (dy % direction[1] == 0) and
                (dx // direction[0] == dy // direction[1]))

    def nunits_in_direction(self, move, direction, orientation):
        """
        Returns the number of units made by move in given direction.

        Assumes self.has_direction(move, direction).  Returns None if
        not an integer multiple of direction vector.
        """
        assert move.source.holder is self
        assert move.target.holder is self

        delta = (move.target - move.source).normalized_orientation(orientation)

        if direction[0] == 0:
            q, r = divmod(delta.drow, direction[1])
        else:
            q, r = divmod(delta.dcol, direction[0])

        if r == 0:
            return q
        else:
            return None

    def set_params(self, params):
        super(Euclidian2D, self).set_params(params)
        self.clear()
