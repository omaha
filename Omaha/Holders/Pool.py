# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

class PoolLocation(Core.PieceHolder.Location):
    def __init__(self, holder, index):
        assert isinstance(holder, PiecePool)
        if (index is not None) and (index >= len(holder.pieces)):
            raise IndexError("Index %d requested in pool with %d pieces" %
                             (index, len(holder.pieces)))
        super(PoolLocation, self).__init__(holder)
        self.index = index
    def __str__(self):
        return "pool@%d" % (self.index)

class PiecePool(Core.PieceHolder):
    """A pool in which a player can keep pieces.

    Pieces in a pool are retrieved by their linear index."""

    Location = PoolLocation

    def __init__(self, **kwargs):
        super(PiecePool, self).__init__(**kwargs)
        self.__pieces = []

    @property
    def pieces(self):
        return self.__pieces

    def clone(self):
        c = super(PiecePool, self).clone()
        c.__pieces = list(self.__pieces)
        return c

    def put_piece(self, location, piece):
        """
        A bit specially, we do not allow do put a piece at a
        specific place inside the pool.
        """
        super(PiecePool, self).put_piece(location, piece)
        assert location.index is None
        self.__pieces.append(piece)
    def take_piece(self, location):
        piece = super(PiecePool, self).take_piece(location)
        self.__pieces[location.index:location.index+1] = ()
        return piece
    def replace_piece(self, location, piece):
        assert piece not in self.pieces
        oldpiece = self.piece_at(location)
        self.__pieces[location.index] = piece
        piece.holder = self
        assert piece in self.__pieces
        assert oldpiece not in self.__pieces
        return oldpiece

    def piece_at(self, location):
        assert location.holder is self
        if location.index < 0 or location.index >= len(self.__pieces):
            raise IndexError("No such position %s." % location.index)
        return self.__pieces[location.index]
    def piece_location(self, piece):
        """
        The location of the given piece.

        We need that because we do not store the index in the
        piece's location (see put_piece).
        """
        if piece not in self.__pieces:
            return None
        return self.Location(self, self.__pieces.index(piece))

    def piece_location_by_type(self, piece_type):
        """Return location for 1st piece of given type, or None."""
        for idx, piece in enumerate(self.__pieces):
            if piece.type is piece_type:
                return self.Location(self, idx)
        else:
            return None
