# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.Euclidian2D import Euclidian2D
import Overlord

class RectBoard(Euclidian2D):
    """Rectangle board made of squares."""

    def __init__(self, **kwargs):
        self.__squares = None # array holding pieces hashed by location
        super(RectBoard, self).__init__(**kwargs)
        self.__piece_locations = {} # reverse mapping

    def clone(self):
        c = super(RectBoard, self).clone()
        c.clear()
        for piece, location in self.__piece_locations.items():
            c.__insert_piece(c.Location(c, location.col, location.row), piece)
        return c

    @Overlord.parameter
    def ncolumns(cls, context):
        return Overlord.params.Int(label = 'Number of columns',
                                   default = 8,
                                   minval = 2, maxval = 99)
    @Overlord.parameter
    def nrows(cls, context):
        return Overlord.params.Int(label = 'Number of rows',
                                   default = 8,
                                   minval = 2, maxval = 99)

    # Euclidian2D properties
    @property
    def matrix_width(self):
        return self.ncolumns
    @property
    def matrix_height(self):
        return self.nrows

    def clear(self):
        """Set all board squares as empty."""
        # FIXME: how to drop this 'i' without having nrows *clones* ?
        self.__squares = [ [None] * self.ncolumns
                           for i in range(self.nrows) ]

    def __insert_piece(self, location, piece):
        "Just update holder's attributes to hold the piece, without touching piece's attributes"
        self.__squares[location.col][location.row] = piece
        self.__piece_locations[piece] = location
    def put_piece(self, location, piece):
        super(RectBoard, self).put_piece(location, piece)
        assert self.__squares[location.col][location.row] is None, \
            "({0.col},{0.row}) already contains {1}".format(
                location, self.__squares[location.col][location.row])
        self.__insert_piece(location, piece)
    def take_piece(self, location):
        piece = super(RectBoard, self).take_piece(location)
        self.__squares[location.col][location.row] = None
        del self.__piece_locations[piece]
        return piece
    def replace_piece(self, location, piece):
        assert piece not in self.pieces
        oldpiece = self.piece_at(location)
        self.__insert_piece(location, piece)
        del self.__piece_locations[oldpiece]
        assert piece in self.pieces
        assert oldpiece not in self.pieces
        return oldpiece

    def valid_location(self, location):
        if location.holder is not self:
            return False
        if  (location.col >= self.ncolumns or location.col < 0 or
             location.row >= self.nrows or location.row < 0):
            return False
        return True

    def piece_at(self, location):
        if not self.valid_location(location):
            raise IndexError("No such position %s." % location)
        return self.__squares[location.col][location.row]
    def piece_location(self, piece):
        """Returns location if piece is in board, else None."""
        if piece not in self.__piece_locations:
            return None
        return self.__piece_locations[piece]

    @property
    def all_locations(self):
        for column in range(self.ncolumns):
            for row in range(self.nrows):
                yield self.Location(self, column, row)

    @property
    def pieces(self):
        return self.__piece_locations.keys()
