# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'GameRenderer', 'HolderDelegatingGameRenderer',
            'PieceDelegatingGameRenderer', 'SinglePieceSetGameRenderer',
            'MoveHighlights' ]

from .game import Game
from .piece_holder import PieceHolder
import Overlord
from Overlord.misc import class_fullname, classproperty
from Overlord import plugin_pattern

# imports for GameRenderer subclasses
from .holder_renderer import HolderRenderer
from .piece_renderer import PieceRenderer

import logging

class GameRenderer(Overlord.Parametrizable):
    """
    Base class for game renderers.

    Used as hierarchy root for proper call chaining of __init__ and
    other methods, and for checking by introspection.
    """

    __logger = logging.getLogger('Omaha.Core.game_renderer.GameRenderer')

    def __init__(self, game, gui, **kwargs):
        super(GameRenderer, self).__init__(**kwargs)
        self.game = game
        self.gui = gui

    @classproperty
    @classmethod
    def context_key(cls):
        return GameRenderer

    def free_resources(self):
        """
        Free any resources that may prevent destruction.
        """

    def clear_highlights(self, mode):
        """
        Deregister all moves/locations for highlighting with specified mode.

        Subclasses are not required to support this mechanism, but
        can do so by overloading this method.
        """
    def add_highlights(self, mode, items):
        """
        Register moves/locations for highlighting with specified mode.

        Subclasses are not required to support this mechanism, but
        can do so by overloading this method.
        """
    def highlights(self, mode):
        """
        Return a tuple of the moves/locations for specified highlighting mode.
        """

    def draw(self):
        drawing_context = self.gui.tk.canvas_drawing_context(self.gui.canvas)
        self.gui.tk.clear(self.gui.canvas) # no incremental updates yet
        self.draw_holders(drawing_context)
        self.draw_pieces(drawing_context)

    def draw_holders(self, drawing_context):
        raise NotImplementedError()

    def draw_pieces(self, drawing_context):
        for piece in self.game.pieces:
            if piece.location is not None:
                self._draw_piece(piece, drawing_context)

    def _draw_piece(self, piece, drawing_context):
        raise NotImplementedError()

    def point_to_location(self, x, y):
        raise NotImplementedError()
    @staticmethod
    def check_point_to_location(x, y):
        assert isinstance(x, int), "{0} is not an int".format(x)
        assert isinstance(y, int), "{0} is not an int".format(y)

class HolderDelegatingGameRenderer(GameRenderer):
    """
    Game renderer delegating rendering of holders to HolderRenderers.
    """

    __default_renderers = {}
    __logger = logging.getLogger('Omaha.Core.game_renderer.HolderDelegatingGameRenderer')

    @classmethod
    def __init_default_renderer(cls, game_class, holder_class):
        # Look for a default HolderRenderer declaration matching this holder
        # 1. in the Game decl
        for ancestor in holder_class.mro():
            default = getattr(game_class, 'x-plugin-desc'). \
                      datafield("X-Omaha-DefaultRenderer-%s" % ancestor.__name__)
            if default is not '':
                cls.__logger.info("Using %s as requested by %s",
                                  default, class_fullname(game_class))
                break
        else:
            cls.__logger.info("No matching X-Omaha-DefaultRenderer-* for %s",
                              class_fullname(cls))

            # 2. in the GameRenderer decl as fallback
            for ancestor in holder_class.mro():
                default = getattr(cls, 'x-plugin-desc'). \
                          datafield("X-Omaha-DefaultRenderer-%s" % ancestor.__name__)
                if default is not '':
                    cls.__logger.info("Using %s as requested by %s",
                                      default, class_fullname(cls))
                    break
            else:
                cls.__logger.info("No matching X-Omaha-DefaultRenderer-* for %s",
                                  class_fullname(cls))
                default = None
        cls.__default_renderers[(game_class, holder_class)] = default

    @classmethod
    def parameters_dcl(cls, context):
        p = dict(super(HolderDelegatingGameRenderer,
                       cls).parameters_dcl(context))
        game_class = context[Game]
        for holder_class in context['holder_types']:
            paramid = "HolderRenderer-" + class_fullname(holder_class)
            assert paramid not in p
            if (game_class, holder_class) not in cls.__default_renderers:
                cls.__init_default_renderer(game_class, holder_class)
            p[paramid] = Overlord.params.Plugin(
                label = 'Renderer for %s %s' % (game_class.__name__,
                                                holder_class.__name__),
                plugintype = 'X-Omaha-HolderRenderer',
                context=context,
                pattern = plugin_pattern.ParentOf('X-Omaha-Holder-Categories',
                                                  holder_class),
                default = cls.__default_renderers[(game_class, holder_class)]
                )
        return p

    def set_params(self, params):
        parentparams = dict(params)

        self.holder_renderers = {}
        for paramid, param in params.items():
            assert isinstance(param, Overlord.Param), \
                   "%s %s is not a Param" % (type(param), param)
            if paramid.startswith("HolderRenderer-"):
                if self.game.is_set_up:
                    for holder in self.game.holders:
                        if paramid != "HolderRenderer-" + class_fullname(type(holder)):
                            continue

                        # setup context for this param
                        context = dict(self.context)
                        assert PieceHolder not in context
                        context[PieceHolder] = type(holder)

                        # instantiate a renderer for this holder
                        brclass = param.value.Class
                        assert issubclass(brclass, HolderRenderer), \
                               "%r is not a %r" % (brclass, HolderRenderer)
                        self.holder_renderers[holder] = brclass(holder=holder, gui=self.gui,
                                                                parentcontext=context)
                        if  (param.value.name in param.subparams and
                             param.subparams[param.value.name]):
                            self.holder_renderers[holder].set_params(
                                param.subparams[param.value.name])
                else:
                    for holder_class in self.game.holder_classes:
                        if paramid != "HolderRenderer-" + class_fullname(holder_class):
                            continue
                del parentparams[paramid]

        # postcondition
        if self.game.is_set_up:
            for holder in self.game.holders:
                assert holder in self.holder_renderers, \
                       "holder %r not in %r" % (holder, self.holder_renderers.keys())

        super(HolderDelegatingGameRenderer, self).set_params(parentparams)

class PieceDelegatingGameRenderer(GameRenderer):
    """
    Game renderer delegating rendering of pieces to a PieceRenderer.
    """

    # FIXME: make it a param
    MARGIN = 10 # percent

    def __init__(self, **kwargs):
        self.__piece_renderer = None

        super(PieceDelegatingGameRenderer, self).__init__(**kwargs)

    @Overlord.parameter
    def piecerenderer_class(cls, context):
        game_class = context[Game]
        cls_plugin = getattr(cls, 'x-plugin-desc')

        # look for a default renderer declaration matching this piece
        for ancestor in game_class.mro():
            default = cls_plugin.datafield("X-Omaha-DefaultPieceRenderer-%s" %
                                           ancestor.__name__)
            if default is not '':
                break
        else:
            default = None

        return Overlord.params.Plugin(
            label = 'Piece renderer for %s' % (game_class.__name__),
            plugintype = 'X-Omaha-PieceRenderer',
            context=context,
            pattern = plugin_pattern.ParentOf('X-Omaha-Game-Categories',
                                              game_class),
            default = default
            )

    def set_params(self, params):
        super(PieceDelegatingGameRenderer, self).set_params(params)

        # propagate piecerenderer_class change
        param = type(self).piecerenderer_class.fget_param(self)
        if param.value is None:
            raise Overlord.NoSuchPlugin("No PieceRenderer for %s" %
                                        type(self.game))

        # instantiate a piece renderer
        self.__prclass = param.value.Class
        assert issubclass(self.__prclass, PieceRenderer)
        self.__piece_renderer = self.__prclass(gui=self.gui,
                                               parentcontext=self.context)

        if param.subparams and param.value.name in param.subparams:
            self.__piece_renderer.set_params(
                param.subparams[param.value.name])

    def draw_pieces(self, drawing_context):
        # notify the piece renderer of the piece size for each set
        for set_id in self._piece_sets:
            self.__piece_renderer.set_piece_size(
                set_id,
                self._piece_set_size(set_id),
                self._piece_set_rotation(set_id))

        # do render
        super(PieceDelegatingGameRenderer, self).draw_pieces(drawing_context)

    def _draw_piece(self, piece, drawing_context):
        # FIXME: this depends on HolderDelegatingGameRenderer
        center = self.holder_renderers[piece.holder].tile_center(
            piece.location)
        self.__piece_renderer.draw_image(drawing_context, center, self._piece_set(piece),
                                         piece)

    @property
    def _piece_sets(self):
        raise NotImplementedError()
    def _piece_set(self, piece):
        raise NotImplementedError()
    def _piece_set_rotation(self, set_id):
        raise NotImplementedError()
    def _piece_set_size(self, set_id):
        raise NotImplementedError()

class SinglePieceSetGameRenderer(PieceDelegatingGameRenderer):
    """
    PieceDelegatingGameRenderer using a single set of pieces per holder.
    """

    @property
    def _piece_sets(self):
        return tuple(self.game.holders)

    def _piece_set(self, piece):
        return piece.holder

    def _piece_set_rotation(self, set_id):
        assert set_id in self._piece_sets, \
               "%r not in %r" % (set_id, self._piece_sets)
        return 0
    def _piece_set_size(self, set_id):
        assert set_id in self._piece_sets, \
               "%r not in %r" % (set_id, self._piece_sets)
        # size of images
        holder_renderer = self.holder_renderers[set_id]
        size = min(holder_renderer.delta_x, holder_renderer.delta_y)
        size = max(1, (100 - 2 * self.MARGIN) * size / 100)
        return size


class MoveHighlights(object):
    "Namespacing-only class for type of move highlights"

    # recently-made moves
    LASTMOVES = "lastmoves"
