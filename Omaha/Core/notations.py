# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .exceptions import ParseError
import re
from string import ascii_lowercase
import Overlord

__all__ = [ 'PieceNotation', 'LocationNotation',
            'MoveNotation', 'PositionNotation',

            'ChessLikeCoordinateNotation', 'SingleBoardCoordinateNotation',
            'LetterColumnNotation', 'NumberRowNotation',
            'NumberColumnNotation', 'LetterRowNotation' ]

# core classes

class Notation(object):
    __metaclass__ = Overlord.OuterClass
    def __init__(self, game):
        self.game = game
class PieceNotation(Notation):
    def piece_name(self, piece):
        raise NotImplementedError()
    def piece_type(self, name):
        raise NotImplementedError()
class LocationNotation(Notation):
    def location_name(self, location):
        raise NotImplementedError()
    def location(self, string):
        raise NotImplementedError()
# FIXME: this is specific to holders featuring cols and rows,
#  ie. Euclidian2D boards, and that could turn into a limitation
#  some day
    def colname(self, location):
        raise NotImplementedError()
    def rowname(self, location):
        raise NotImplementedError()
    def col(self, string):
        raise NotImplementedError()
    def row(self, string):
        raise NotImplementedError()
class MoveNotation(Notation):
    __innerclasses__ = {'LocationNotation', 'PieceNotation'}
    LocationNotation = LocationNotation
    # PieceNotation is not used by all MoveNotation's, but by enough
    # of them to keep it here
    PieceNotation = PieceNotation

    def __init__(self, game):
        super(MoveNotation, self).__init__(game)
        self.location_notation = self.LocationNotation(game)
        self.piece_notation = self.PieceNotation(game)
    def move_serialization(self, move):
        raise NotImplementedError()
    def move_deserialization(self, string, player):
        raise NotImplementedError()
class PositionNotation(Notation):
    "Notation for the full state of a game"
    __innerclasses__ = {'LocationNotation', 'PieceNotation'}
    LocationNotation = LocationNotation
    PieceNotation = PieceNotation
    def __init__(self, game):
        super(PositionNotation, self).__init__(game)
        self.location_notation = self.LocationNotation(game)
        self.piece_notation = self.PieceNotation(game)
    def position_serialization(self):
        raise NotImplementedError()
#    def position_deserialization(self, string):
#        raise NotImplementedError()

# common classes
# FIXME: numbering/lettering direction is hardcoded in there

class LetterColumnNotation(LocationNotation):
    def colname(self, location):
        return ascii_lowercase[location.col]
    def col(self, string):
        return ascii_lowercase.index(string)
class NumberRowNotation(LocationNotation):
    def rowname(self, location):
        return "%s" % (self.game.boardheight - location.row)
    def row(self, string):
        return self.game.boardheight - int(string)

class NumberColumnNotation(LocationNotation):
    def colname(self, location):
        return "%s" % (self.game.boardwidth - location.col)
    def col(self, string):
        return self.game.boardwidth - int(string)
class LetterRowNotation(LocationNotation):
    def rowname(self, location):
        return ascii_lowercase[location.row]
    def row(self, string):
        try:
            return ascii_lowercase.index(string)
        except ValueError:
            print "{0!r} not a lowercase char".format(string)
            raise

# FIXME: this is specific to SingleBoardGame, should live
# somewhere else, or the latter should be in Core

class SingleBoardCoordinateNotation(LocationNotation):
    locname = "generic chesslike coordinates location"
    def location_name(self, location):
        assert location.holder is self.game.board, \
               "location for %s is %r, not %r" % (location,
                                                  location.holder,
                                                  self.game.board)
        return "%s%s" % (self.colname(location),
                         self.rowname(location))

    location_regexp = r"([a-w]+)(\d+)"
    def location_from_match(self, match):
        return self.game.board.Location(self.game.board,
                                        self.col(match.group(1)),
                                        self.row(match.group(2)))

    def location(self, string):
        # only match exact string, don't leave any trailing chars behind
        m = re.match(self.location_regexp + "$", string)
        if m:
            return self.location_from_match(m)
        raise ParseError("could not parse '%s' as a %s" %
                         (string, self.locname))

class ChessLikeCoordinateNotation(MoveNotation):
    movename = "generic chesslike coordinates move"
    class LocationNotation(SingleBoardCoordinateNotation,
                           LetterColumnNotation, NumberRowNotation):
        "LocationNotation for ChessLikeCoordinateNotation"

    # default implementation for "a1" style serialization
    def __init__(self, game):
        #assert isinstance(game, SingleBoardGame)
        super(ChessLikeCoordinateNotation, self).__init__(game)

    def move_serialization(self, move):
        return "%s%s" % (self.location_notation.location_name(move.source),
                         self.location_notation.location_name(move.target))

    # FIXME: use named groups
    regexp = r"([a-w]+\d+)([a-w]+\d+)"
    def move_from_match(self, match, player):
        return self.game.Move(source=self.location_notation.location(match.group(1)),
                              target=self.location_notation.location(match.group(2)))

    def move_deserialization(self, string, player):
        # only match exact string, don't leave any trailing chars behind
        m = re.match(self.regexp + "$", string)
        if m:
            return self.move_from_match(m, player)
        raise ParseError("could not parse '%s' as a %s" %
                         (string, self.movename))
