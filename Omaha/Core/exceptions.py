# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

class OmahaException(Exception):
    """Base class for all Omaha exceptions."""
    pass

class InvalidMove(OmahaException):
    pass

class NotMyTurn(OmahaException):
    pass

class IncompleteMove(OmahaException):
    """Signals a move that requires more information to be performed."""
    def __init__(self, paramlist):
        """Records a dict of Param instances for unspecified aspects of the move."""
        super(IncompleteMove, self).__init__()
        self.paramlist = paramlist

class ParseError(OmahaException):
    pass

class UserCancel(OmahaException):
    pass

class UnsatisfiableConstraint(Exception):
    """A parameter's constraints left no available choices."""

class UserNotification(OmahaException):
    """Exception that should be shown to user."""
    def __init__(self, title, text):
        self.title, self.text = "omaha - " + title, text
class UserErrorNotification(UserNotification):
    "Convenience subclass of `UserNotification` for errors."
    def __init__(self, text):
        UserNotification.__init__(self, "error", text)
