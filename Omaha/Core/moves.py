# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

class Move(object):
    """The root class of all Move objects."""
    special_moves = {}
    def __init__(self):
        # This ctor *must not* have a **kwargs, so any stray kwargs
        # passed by error are correctly reported as errors.
        pass
    def clone(self, game, game_clone):
        assert type(self) is game.Move, "%r is not a %r" % (self, game.Move)
        return game_clone.Move()
    def assert_locations(self, holders):
        """Assert any locations referenced by this move belongs to specified holders.

        This allows to easily and early pinpoint problems with the
        game cloning mechanism.  Move classes adding new types of
        references to holders must add the necessary assertions here."""

class PutMove(Move):
    """A move where a piece is put onto a location."""
    def __init__(self, target=None, **kwargs):
        super(PutMove, self).__init__(**kwargs)
        self.set_target(target)
    def set_target(self, location):
        self.__target = location
    @property
    def target(self):
        return self.__target
    def clone(self, game, game_clone):
        newmove = super(PutMove, self).clone(game, game_clone)
        newmove.set_target(game.clone_location(self.target, game_clone))
        return newmove
    def assert_locations(self, holders):
        super(PutMove, self).assert_locations(holders)
        assert self.__target
        assert self.__target.holder in holders, \
            "%s not in %s" % (self.__target.holder, holders)

class PutOnlyMove(PutMove):
    """A PutMove where the source of piece needs not be specified."""
    def __str__(self):
        return str(self.target)

class TakeMove(Move):
    """A move where a piece is removed from a location."""
    def __init__(self, source=None, **kwargs):
        super(TakeMove, self).__init__(**kwargs)
        self.set_source(source)
    def set_source(self, location):
        self.__source = location
    @property
    def source(self):
        return self.__source
    def clone(self, game, game_clone):
        newmove = super(TakeMove, self).clone(game, game_clone)
        newmove.set_source(game.clone_location(self.source, game_clone))
        return newmove
    def assert_locations(self, holders):
        super(TakeMove, self).assert_locations(holders)
        assert self.__source
        assert self.__source.holder in holders, \
            "%s not in %s" % (self.__source.holder, holders)

class GenericDisplaceMove(TakeMove):
    @property
    def target(self):
        raise NotImplementedError()

class SimpleDisplaceMove(PutMove, GenericDisplaceMove):
    """A move where a piece is moved from one location to another."""
    def __init__(self, **kwargs):
        super(SimpleDisplaceMove, self).__init__(**kwargs)
    def set_source(self, location):
        TakeMove.set_source(self, location)
        PutMove.set_target(self, None)
    def __str__(self):
        return "%s -> %s" % (self.source, self.target)

class WaypointDisplaceMove(GenericDisplaceMove):
    def __init__(self, **kwargs):
        super(WaypointDisplaceMove, self).__init__(**kwargs)
        self.waypoints = []
        self.finished = False
    def clone(self, game, game_clone):
        newmove = super(WaypointDisplaceMove, self).clone(game, game_clone)
        newmove.waypoints = [game.clone_location(loc, game_clone) for loc in self.waypoints]
        newmove.finished = self.finished
        return newmove
    @property
    def target(self):
        if self.finished:
            assert len(self.waypoints) > 0
            return self.waypoints[-1]
        else:
            return None
    @property
    def steps(self):
        """Iterator returning 2-tuples for each step in the move."""
        if len(self.waypoints) > 0:
            yield (self.source, self.waypoints[0])
        for src_idx in range(0, len(self.waypoints) - 1):
            yield (self.waypoints[src_idx], self.waypoints[src_idx + 1])
    def assert_locations(self, holders):
        super(WaypointDisplaceMove, self).assert_locations(holders)
        for waypoint in self.waypoints:
            assert waypoint.holder in holders, \
                "%s not in %s" % (waypoint.holder, holders)

    def add_waypoint(self, location):
        self.waypoints.append(location)

    def finish(self):
        self.finished = True

    def __str__(self):
        result = str(self.source)
        for step in self.waypoints:
            result += " -> %s" % step
        if not self.finished:
            result += " -> ?"
        return result
