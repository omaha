# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'Toolkit', 'App',
            'ParamWidget', 'PluginChoiceWidget',
            'BitmapImage', 'VectorImage' ]

import Overlord
from .exceptions import UserCancel, UnsatisfiableConstraint
import logging

class ParamWidget(object):
    """
    Base class for a widget that edits a Param.
    """
    def __init__(self, param, gui):
        assert isinstance(param, Overlord.Param)
        self.param, self.gui = param, gui

class LabelWidget(ParamWidget):
    def widget(self):
        return self.gui.tk.RawLabelWidget(self.param.label)
    def retrieve_value(self, entry=None):
        pass

class ChoiceWidget(ParamWidget):
    def _add_subparams(self, combo, vbox):
        # handle subparams
        if self.param.has_subparams:
            # create a box holding the combo and it's subparams hboxes
            self.__subparam_hboxes = {}
            self.__subparam_widgets = {}
            for parent_id, parent_param in self.param.subparams.items():
                # FIXME: walk_params ?
                for subparam_key, subparam in reversed(Overlord.dep_ordered_params(parent_param)):
                    hbox = self.__make_subparam_widget(subparam, parent_id, parent_param)
                    if hbox is None:
                        continue
                    self.gui.tk._vbox_add(vbox, hbox)
                    # FIXME this does not support subparam nesting
                    self.__subparam_hboxes[(parent_id, subparam_key)] = hbox

            def __update_subparams_visibility(combo, param):
                # FIXME: should also shrink dialog when needed
                for parent_choice in param.subparams.keys():
                    for subparam_key, subparam in param.subparams[parent_choice].items():
                        if not subparam.decl.pinned_value:
                            self._update_one_subparam_visibility(
                                combo, parent_choice,
                                self.__subparam_hboxes[(parent_choice, subparam_key)],
                                subparam)

            self.gui.tk._combo_setchanged_hook(
                combo, __update_subparams_visibility, self.param)

    def __make_subparam_widget(self, subparam, parent_id, parent_param):
        assert isinstance(subparam, Overlord.Param)
        if subparam.decl.pinned_value:
            return None
        try:
            hbox, self.__subparam_widgets[subparam] = \
                  self.gui.tk.create_param_widget(subparam,
                                                  self.gui,
                                                  parent_param)
        except UnsatisfiableConstraint as ex:
            hbox = self.gui.tk.RawLabelWidget(ex)
        # FIXME: something causes all subparams to be shown
        # by default, so we need to hide those for
        # non-default choices
        if parent_id != self.param.value.name:
            hbox.hide()
        return hbox

    def _retrieve_subparam_values(self, active_text):
        if  (self.param.has_subparams and
             active_text in self.param.subparams):
            for subparam in self.param.subparams[active_text].values():
                if subparam.decl.pinned_value:
                    continue
                self.__subparam_widgets[subparam].retrieve_value()

class PluginChoiceWidget(ParamWidget):
    """
    A ChoiceWidget wrapper for selecting in an Omaha plugin list.
    """

    __logger = logging.getLogger('Omaha.Core.UI.PluginChoiceWidget')

    def __init__(self, param, gui):
        assert isinstance(param, Overlord.Param)
        assert isinstance(param.decl, Overlord.params.Plugin)
        super(PluginChoiceWidget, self).__init__(param, gui)

        alternatives, subparams = {}, {}
        for plugin in gui.plugin_manager.get_plugins_list(
                param.decl.plugintype, pattern=param.decl.pattern):
            if not plugin.usable:
                # we will gray it out
                alternatives[plugin.name] = None
                continue
            alternatives[plugin.name] = plugin
            plugin_cls = plugin.Class
            if not issubclass(plugin_cls, Overlord.Parametrizable):
                continue # no parameter to take care of

            # emulate context building
            # FIXME: cannot hope to be fully correct as-is

            # local imports to avoid circular deps
            from .game_renderer import GameRenderer, HolderDelegatingGameRenderer
            from .holder_renderer import HolderRenderer
            from .game_app import GameApp
            # contribution from Overlord.Parametrizable.__init__
            context = dict(param.decl.context)
            assert plugin_cls.context_key not in context
            context[plugin_cls.context_key] = plugin_cls
            # contribution from Game.setup
            if isinstance(context["app"], GameApp):
                game = context["app"].game
                context["holder_types"] = set(type(holder)
                                              for holder in game.holders)
            # contribution from HolderDelegatingGameRenderer to Holder
            if plugin_cls.context_key == HolderRenderer:
                assert GameRenderer in context
                # FIXME: unnecessary condition ?  what else can happen ?
                if isinstance(context[GameRenderer],
                              HolderDelegatingGameRenderer):
                    # FIXME
                    self.__logger.warning("NOT IMPLEMENTED: addition to 'Holder' to context")

            subparamdecls = plugin_cls.parameters_dcl(context)
            # pin requested values
            self.__pin_decls(plugin, subparamdecls)

            if len(subparamdecls) > 0:
                # FIXME: must also inject values from prefs into default
                subparams[plugin.name] = subparamdecls
        choiceparam = Overlord.Param(
            Overlord.params.Choice(
                label=param.label,
                alternatives = alternatives,
                subparams = subparams,
                default = param.value),
            gui.plugin_manager)
        param.set_subparams(choiceparam.subparams) # FIXME: that shall not be :)

        self._choice_widget = gui.tk.ChoiceWidget(choiceparam, gui)

    @classmethod
    def __pin_decls(cls, plugin, decls):
        for subparamid, subparamdecl in decls.items():
            assert isinstance(subparamdecl, Overlord.ParamDecl), \
                   "%r is not a ParamDecl" % (subparamdecl,)
            pinnedvalue_key = "X-Omaha-Parameter-{0}".format(subparamid)
            if plugin.has_datafield(pinnedvalue_key):
                subparamdecl.pinned_value = plugin.datafield(pinnedvalue_key)


class App(Overlord.App):
    def __init__(self, plugin_manager, prefs, toolkit, **kwargs):
        self.tk = toolkit
        super(App, self).__init__(plugin_manager=plugin_manager, prefs=prefs, **kwargs)
    def free_resources(self):
        pass
    def _create_gui(self):
        raise NotImplementedError()

    # serialized-move input

    def set_manual_move_entry_callback(self, callback):
        raise NotImplementedError()
    def unset_manual_move_entry_callback(self, callback):
        raise NotImplementedError()
    def clear_manual_move_entry(self):
        raise NotImplementedError()

class BitmapImage(object):
    """An interface for bitmap image manipulation by the UI toolkit."""
    def __init__(self, width, height):
        raise NotImplementedError()
    @property
    def _img(self):
        raise NotImplementedError()
    @property
    def width(self):
        raise NotImplementedError()
    @property
    def height(self):
        raise NotImplementedError()
    def draw_sized_text(self, x, y, width, height,
                        text, color=None, bold=False, rotation=None):
        raise NotImplementedError()

class VectorImage(object):
    """An interface for vector image manipulation by the UI toolkit."""
    def __init__(self, svgfilename):
        raise NotImplementedError()
    @property
    def width(self):
        raise NotImplementedError()
    @property
    def height(self):
        raise NotImplementedError()
    def render_to_bitmap(self, bitmap, x, y, width, height,
                         rotation=None):
        """
        Render vector graphics in bbox, after possible rotation.
        """
        raise NotImplementedError()

class Toolkit(object):
    __metaclass__ = Overlord.OuterClass
    __innerclasses__ = {'BitmapImage', 'VectorImage',
                        'RawLabelWidget', 'LabelWidget',
                        'StringWidget', 'IntWidget', 'FloatWidget',
                        'ChoiceWidget', 'MultiChoiceWidget',
                        'PluginChoiceWidget'}

    def __init__(self):
        self._paramwidget_classes = {
            Overlord.params.String:      self.StringWidget,
            Overlord.params.Int:         self.IntWidget,
            Overlord.params.Float:       self.FloatWidget,
            Overlord.params.Choice:      self.ChoiceWidget,
            Overlord.params.MultiChoice: self.MultiChoiceWidget,
            Overlord.params.Plugin:      self.PluginChoiceWidget,
            }

    # to be defined in subclasses
    BitmapImage = BitmapImage
    VectorImage = VectorImage

    # those classes, referenced below, that have to be further
    # specialized for concrete toolkits
    _UnimplementedWidgetClasses = (ParamWidget,
                                   PluginChoiceWidget)

    RawLabelWidget = object
    LabelWidget = LabelWidget
    StringWidget = ParamWidget
    IntWidget = ParamWidget
    FloatWidget = ParamWidget
    ChoiceWidget = ChoiceWidget
    MultiChoiceWidget = ParamWidget
    PluginChoiceWidget = PluginChoiceWidget

    def prepend_icon_search_path(self, path):
        raise NotImplementedError()

    # generic GUI elements

    def notify(self, title, text):
        raise NotImplementedError()

    def select_load_file(self):
        raise NotImplementedError()

    def open_params_dialog(self, title, params, app_ui, accept_cb=None,
                           parent_plugin=None, parent_window=None,
                           destroy=True, synchronous=False):
        raise NotImplementedError()

    def askuser_params(self, title, params, app_ui, parent_plugin=None):
        """
        Present the user with a dialog allowing him to set parameters.
        """
        answer = self.open_params_dialog(title, params, app_ui, accept_cb=None,
                                         parent_plugin=parent_plugin,
                                         synchronous=True)
        if answer is False:
            raise UserCancel()

    def _create_widget(self, param, app_ui, params):
        try:
            cls = self._paramwidget_classes[type(param.decl)]
        except KeyError:
            # FIXME: can also be a wrong parameter decl (missing
            # mandatory attribute)
            raise Overlord.ParameterError("Parameter type '%s' not supported for %s" %
                                          (type(param.decl), pretty(param)))
        if cls in self._UnimplementedWidgetClasses:
            w = self.LabelWidget(
                # pseudo-param to forge a label
                Overlord.Param(Overlord.ParamDecl(
                    "(parameter type '%s' not supported)" %
                    (type(param.decl),)), None), app_ui)
        else:
            w = cls(param, app_ui)
            # update this widget when the param's dependencies change
            for dep_name, callback in param.decl.depends_on.items():
                assert dep_name in params, \
                       "%r not in %r" % (dep_name, params)
                def __hook(param, oldvalue, dependant_param):
                    callback(param, oldvalue, dependant_param)
                    w.update()
                params[dep_name].register_changed(__hook,
                                                  dependant_param=param)
        return w

    ## main event loop

    def run(self):
        raise NotImplementedError()

    ## canvas user interaction

    # A GUI needs to implement at least one of those groups for user
    # interaction:

    # 1. file/stream input, timers

    def set_stream_read_callback(self, stream, callback):
        raise NotImplementedError()
    def unset_stream_read_callback(self, callback):
        raise NotImplementedError()

    def set_stream_error_callback(self, stream, callback):
        raise NotImplementedError()
    def unset_stream_error_callback(self, callback):
        raise NotImplementedError()
    def set_timer_callback(self, timeout, callback):
        raise NotImplementedError()
    def unset_timer_callback(self, callback):
        raise NotImplementedError()

    # 2. pointing-device input

    def set_mouse_release_callback(self, canvas, callback):
        raise NotImplementedError()
    def unset_mouse_release_callback(self, canvas, callback):
        raise NotImplementedError()

    def set_mouse_press_callback(self, canvas, callback):
        raise NotImplementedError()
    def unset_mouse_press_callback(self, canvas, callback):
        raise NotImplementedError()

    def set_mouse_move_callback(self, canvas, callback):
        raise NotImplementedError()
    def unset_mouse_move_callback(self, canvas, callback):
        raise NotImplementedError()

    ## canvas drawing methods

    def canvas_drawing_context(self, canvas):
        raise NotImplementedError()
    # poor man's (ie. gtk's...) nesting
    def canvas_begin_subcontext(self, parent):
        raise NotImplementedError()
    def canvas_end_subcontext(self, context):
        raise NotImplementedError()

    @staticmethod
    def context_rotate(drawing_context, rotation):
        raise NotImplementedError()
    @staticmethod
    def context_scale(drawing_context, factor, center):
        raise NotImplementedError()

    def clear(self, canvas):
        raise NotImplementedError()

    def color(self, canvas, color):
        raise NotImplementedError()

    def draw_line(self, drawing_context, x0, y0, x1, y1, **attrs):
        raise NotImplementedError()
    def draw_rectangle(self, drawing_context, x, y, w, h, **attrs):
        raise NotImplementedError()
    def fill_rectangle(self, drawing_context, x, y, w, h, **attrs):
        raise NotImplementedError()
    def draw_circle(self, drawing_context, center_x, center_y, radius, **attrs):
        raise NotImplementedError()
    def fill_circle(self, drawing_context, center_x, center_y, radius, **attrs):
        raise NotImplementedError()
    def draw_polygon(self, drawing_context, pointlist, **attrs):
        raise NotImplementedError()
    def fill_polygon(self, drawing_context, pointlist, **attrs):
        raise NotImplementedError()

    # FIXME: completely hackish, color must be str here
    def draw_centered_text(self, drawing_context,
                           x, y, text, color=None, bold=False):
        raise NotImplementedError()

    def draw_image(self, drawing_context, image, x, y):
        raise NotImplementedError()

    #def image_from_svg(self, canvas, filename, width, height):
    #    image = self.BitmapImage(int(width), int(height))
    #    vimg = self.VectorImage(filename)
    #    vimg.render_to_bitmap(image, 0, 0, width, height)
    #
    #    return image
