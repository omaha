# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'PieceRenderer', 'NotationBasedPieceRenderer' ]

from .game import Game
import Overlord
from Overlord.misc import classproperty

class PieceRenderer(Overlord.Parametrizable):
    def __init__(self, gui, **kwargs):
        super(PieceRenderer, self).__init__(**kwargs)
        self.__gui = gui
    @classproperty
    @classmethod
    def context_key(cls):
        return PieceRenderer
    @property
    def gui(self):
        return self.__gui
    def set_piece_size(self, set_id, newsize, angle=0):
        raise NotImplementedError()
    def draw_image(self, drawing_context, center, set_id, key):
        raise NotImplementedError()

class NotationBasedPieceRenderer(PieceRenderer):

    def __init__(self, **kwargs):
        super(NotationBasedPieceRenderer, self).__init__(**kwargs)
        self.__notation = None

    @Overlord.parameter
    def notation_class(cls, context):
        game_class = context[Game]
        game_plugin = getattr(game_class, 'x-plugin-desc')
        if game_plugin.has_datafield("X-Omaha-DefaultPieceNotation"):
            default_notation = game_plugin.datafield("X-Omaha-DefaultPieceNotation")
#            logger.info("Using %s as requested by %s",
#                        default_renderer, game_plugin.name)
        else:
            default_notation = None
        return Overlord.params.Plugin(
            label = 'Character set',
            plugintype = 'X-Omaha-PieceNotation',
            context = context,
            pattern = Overlord.plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_class),
            default = default_notation,
            )

    @property
    def notation(self):
        notationcls = self.notation_class.Class
        # (re)instanciate notation if required
        if not isinstance(self.__notation, notationcls):
            self.__notation = notationcls(self.gui.game)
        return self.__notation
