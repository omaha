# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'PieceHolder', 'Location' ]

import Overlord
from Overlord.misc import classproperty

class Location(object):
    """Base class for description of piece location within a holder.

    Description of the precise location within holder is left to
    the specific subclasses.

    Used as hierarchy root, but does not provide proper __init__
    chaining for multiple inheritance.
    """
    def __init__(self, holder):
        self.holder = holder

class PieceHolder(Overlord.Parametrizable):
    """
    Base class for piece-holding objects: boards, bags, ...

    Used as hierarchy root, but does not provide proper __init__
    chaining for multiple inheritance.
    """

    __metaclass__ = Overlord.ParametrizableOuterclass
    __innerclasses__ = {'Location'}

    # the default, to be subclassed by most PieceHolder subclasses
    Location = Location

    @classproperty
    @classmethod
    def context_key(cls):
        return PieceHolder

    def piece_at(self, location):
        """Return piece at location (may be None).
        Throws IndexError if location coordinates are out of bounds."""
        raise NotImplementedError()
    def take_piece(self, location):
        """Remove piece from location in holder and return it."""
        assert location.holder is self
        piece = self.piece_at(location)
        if piece is None:
            return None
        piece.holder = None
        return piece

    def put_piece(self, location, piece):
        """Put piece at given location in the holder."""
        assert location.holder is self
        assert piece.holder is None, "%s has holder!" % piece
        piece.holder = self

    def replace_piece(self, location, piece):
        raise NotImplementedError()

    def piece_location(self, piece):
        "Return Location in `self` where `piece` can be found, or None."
        raise NotImplementedError()

    def clear(self):
        raise NotImplementedError()

    @property
    def pieces(self):
        "The list of pieces in this PieceHolder."
        raise NotImplementedError()
