# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'GameApp' ]

from .UI import App
from .game import Game, UndoableGame
from .game_renderer import GameRenderer
from .player_driver import PlayerDriver, IsInteractive
from .exceptions import UserCancel

import Overlord
from Overlord import plugin_pattern

import logging
logger = logging.getLogger('Omaha.Core.game_app')

# FIXME: isn't there some Play-specific stuff here ...

class GameApp(App):
    def __init__(self, gamedesc=None, **kwargs):
        super(GameApp, self).__init__(**kwargs)
        self.canvas = None
        self.game_renderer = None
        self._player_drivers = {}

        self.game = None # to be filled by the __init_* calls
        if gamedesc:
            self.__init_newgame(gamedesc)
        else:
            self.__init_loadgame()

        assert self.game
        assert Game not in self.context
        self.context[Game] = type(self.game)

        if gamedesc:
            self.__setup_newgame(gamedesc)

    def __init_newgame(self, gamedesc):
        self.game = gamedesc.Class(parentcontext=self.context)
    def __setup_newgame(self, gamedesc):
        # ask user for any parameters
        game_params = self.game.fresh_parameters_for(self.plugin_manager,
                                                     self.context)
        should_run = True
        if len(game_params) > 0:
            should_run = self.tk.open_params_dialog(
                "Game parameters", game_params,
                app_ui=self, accept_cb=self.game.set_params,
                parent_plugin=gamedesc, synchronous=True)
        if not should_run:
            raise UserCancel()
        self.game.setup()

    def __init_loadgame(self):
        filename = self.tk.select_load_file()
        # FIXME: hardcoded, should be pluggable
        if filename.endswith(".sgf"):
            from Omaha.GameIO.SGFReader import SGFGameReader
            with open(filename) as f:
                reader = SGFGameReader(f, self.plugin_manager,
                                       parentcontext=self.context)
        elif filename.endswith(".pgn"):
            from Omaha.GameIO.PGNReader import PGNGameReader
            with open(filename) as f:
                reader = PGNGameReader(f, self.plugin_manager,
                                       parentcontext=self.context)
        else:
            raise AssertionError("Unknown file extension for %r" % (filename,))

        self.game = reader.game()
        assert isinstance(self.game, Game)

    def run(self):
        self._create_gui()

        ## connect GUI and game engine

        self.connect_players()
        self.game.register_listener(self.__game_listener)

        # launch game
        self.game.ui_resume()

    def free_resources(self):
        super(GameApp, self).free_resources()
        try:
            # can be called before registration (from _init_default_params)
            self.game.unregister_listener(self.__game_listener)
        except ValueError:
            # FIXME: should use a more specific exception
            pass

    @Overlord.parameter
    def game_renderer_class(cls, context):
        game_class = context[Game] # set early in __init__ and won't ever change
        game_plugin = getattr(game_class, 'x-plugin-desc')
        if game_plugin.has_datafield("X-Omaha-DefaultRenderer"):
            default_renderer = game_plugin.datafield("X-Omaha-DefaultRenderer")
            logger.info("Using %s as requested by %s",
                        default_renderer, game_plugin.name)
        else:
            default_renderer = None
        return Overlord.params.Plugin(
            label = 'Renderer for %s' % game_class.__name__,
            plugintype = 'X-Omaha-GameRenderer',
            context = context,
            # FIXME: should allow X-Omaha-RendererCompat too
            pattern = plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_class),
            default = default_renderer
            )


    @classmethod
    def parameters_dcl(cls, context):
        p = dict(super(GameApp, cls).parameters_dcl(context))
        game_class = context[Game]
        for player_name in game_class.player_names:
            paramkey = "PlayerDriver-" + player_name
            assert paramkey not in p
            #if (game_class, holder_class) not in cls.__default_renderers:
            #    cls.__init_default_renderer(game_class, holder_class)
            p[paramkey] = Overlord.params.Plugin(
                label = player_name,
                plugintype = 'X-Omaha-PlayerDriver',
                context = context,
                # filter by moves
                pattern = plugin_pattern.Or(
                    plugin_pattern.ParentOf(
                        'X-Omaha-Move-Categories', game_class.Move),
                    plugin_pattern.ParentOf(
                        'X-Omaha-Game-Categories', game_class)),
                default = cls.__default_playerdriver(context),
                #default = str(getattr(type(self._player_drivers[player]),
                #                      'x-plugin-desc').ID),
                #default = cls.__default_renderers[(game_class, holder_class)]
            )
        return p

    def __set_pluginparam(self, paramname, **kwargs):
        param = getattr(type(self), paramname).fget_param(self)
        if param.value is None:
            raise Overlord.NoSuchPlugin("No %s" % param.label)

        try:
            context = dict(self.context)
            context.update(self.game.context)
            ret = param.value.Class(parentcontext=context, **kwargs)
        except:
            self.free_resources()
            raise

        if param.subparams and param.value.name in param.subparams:
            ret.set_params(param.subparams[param.value.name])

        return ret

    def set_params(self, params):
        parentparams = dict(params)
        replacing_playerdrivers = False

        for paramid, param in params.items():
            if paramid.startswith("PlayerDriver-"):
                logger.debug("%s (%s): %s", paramid, param.label, param.value)
                for player in self.game.players:
                    if paramid == "PlayerDriver-" + player.name:
                        break
                else:
                    continue

                Driver = param.value.Class
                assert issubclass(Driver, PlayerDriver), \
                    "%s is not a PlayerDriver" % (Driver,)

                # replace the existing player driver, but wait till the
                # newone started before removing the old one
                newdrv = Driver(gui=self, player=player, parentcontext=self.context)
                if player in self._player_drivers:
                    replacing_playerdrivers = True
                    self._player_drivers[player].disconnect()
                self._player_drivers[player] = newdrv

                # apply any PlayerDriver parameters
                choice_text = param.value.name
                if choice_text in param.subparams:
                    self._player_drivers[player].set_params(
                        param.subparams[choice_text])
                else:
                    logger.debug("no subparams for %r in %r",
                                 choice_text, param.subparams)

                del parentparams[paramid]

        super(GameApp, self).set_params(parentparams)

        if replacing_playerdrivers:
            # FIXME: on first setting PlayerDriver, we must wait for
            # the GUI to be there before we can connect.
            self.connect_players()

        # propagate game_renderer_class change

        if self.game_renderer:
            self.game_renderer.free_resources()

        self.game_renderer = self.__set_pluginparam("game_renderer_class",
                                                    game=self.game,
                                                    gui=self)
        assert isinstance(self.game_renderer, GameRenderer)

    def connect_players(self):
        for player_drv in self._player_drivers.values():
            player_drv.connect()
        self.check_for_undo()
    def disconnect_players(self):
        for key, player_drv in self._player_drivers.items():
            player_drv.disconnect()
            del self._player_drivers[key]

    @classmethod
    def __default_playerdriver(cls, context):
        # try to locate a default playerdriver decl
        # FIXME: setting that for the game is nonsense
        default_drv = None
        game_class = context[Game]
        for ancestor in game_class.mro():
            if hasattr(ancestor, 'x-plugin-desc'):
                ancestor_plugin = getattr(ancestor, 'x-plugin-desc')
                if ancestor_plugin.has_datafield("X-Omaha-DefaultPlayerDriver"):
                    default_drv = ancestor_plugin.datafield(
                        "X-Omaha-DefaultPlayerDriver")
                    break
        else:
            logger.info("no default PlayerDriver for %s", game_class)
            app = context["app"]
            # select a player driver for the most specific move type
            default_drv = app.plugin_manager.find_plugin(
                'X-Omaha-PlayerDriver',
                pattern = plugin_pattern.And(
                    plugin_pattern.Or(
                        plugin_pattern.ParentOf('X-Omaha-Move-Categories',
                                                game_class.Move),
                        plugin_pattern.ParentOf('X-Omaha-Game-Categories',
                                                game_class)),
                    IsInteractive()) ).ID
        return default_drv

    ## the UI itself

    def __game_listener(self, event):
        if isinstance(event, Game.PhaseChangeEvent):
            if event.newphase is Game.Phases.Over:
                winners = [ player for player, rank
                            in self.game.outcome.items()
                            if rank is self.game.Outcomes.Winner ]
                resigns = [ player for player, rank
                            in self.game.outcome.items()
                            if rank is self.game.Outcomes.Resign ]
                draws = [ player for player, rank
                          in self.game.outcome.items()
                          if rank is self.game.Outcomes.IsDraw ]
                if len(winners) == 1:
                    phrase = "%s won!" % (winners[0].name,)
                    if resigns:
                        phrase = "{0} resigned, {1}".format(
                            ', '.join(p.name for p in resigns), phrase)
                elif len(draws) == len(self.game.players):
                    phrase = 'Draw game'
                else:
                    phrase = "cannot interpret outcome {0}".format(
                        self.game.outcome)
                self.tk.notify("Game over",
                               "Game over, %s" % (phrase,) )

    @property
    def main_window(self):
        raise NotImplementedError()

    def _create_gui(self):
        raise NotImplementedError()

    @property
    def canvas_size(self):
        raise NotImplementedError()

    def add_game_action(self, label, callback):
        raise NotImplementedError()
    def remove_game_action(self, game_action):
        raise NotImplementedError()

    def check_for_undo(self):
        supports_undo = reduce(
            lambda x, y: x and y,
            (drv.supports_undo() for drv in self._player_drivers.values()))
        if supports_undo and isinstance(self.game, UndoableGame):
            self._enable_undo()
        else:
            self._disable_undo()
    def _enable_undo(self):
        pass
    def _disable_undo(self):
        pass
