# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'PlayerDriver' ]

from .exceptions import IncompleteMove
import logging
import Overlord
from Overlord.misc import classproperty

logger = logging.getLogger('Omaha.Core.player_driver')

class PlayerDriver(Overlord.Parametrizable):
    """
    Class for objects that drive a player in a game.
    """
    def __init__(self, gui, player, **kwargs):
        super(PlayerDriver, self).__init__(**kwargs)
        self.name = None
        self.gui = gui
        self.player = player

    @classproperty
    @classmethod
    def context_key(cls):
        return PlayerDriver

    def connect(self):
        raise NotImplementedError()
    def disconnect(self):
        raise NotImplementedError()

    def supports_undo(self):
        return False

    # FIXME: specific to those that accumulate into current_move
    def do_move(self):
        try:
            self.gui.game.make_move(self.player, self.current_move)
        except IncompleteMove as ex:
            paramlist = dict( (paramid, Overlord.Param(decl, self.gui.plugin_manager))
                              for paramid, decl in ex.paramlist.items() )
            self.gui.tk.askuser_params("Finalize move", paramlist, app_ui=self.gui)
            for attribute, param in paramlist.items():
                logger.debug("Finalizing move with %s=%s", attribute, param.value)
                setattr(self.current_move, attribute, param.value)
            # retry with this updated move
            self.do_move()

class IsInteractive(Overlord.PluginPattern):
    def match_distance(self, plugin):
        if  (plugin.has_datafield("X-Omaha-Interactive") and
             plugin.datafield("X-Omaha-Interactive") == "true"):
            return 0
        return None
