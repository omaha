# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'Piece' ]

from .player import Player as CorePlayer

class Piece(object):
    """Class for the generic concept of a piece.

    Subclassed or referred-to by Game subclasses.

    Used as hierarchy root, but does not provide proper __init__
    chaining for multiple inheritance (some subclasses need move args
    to __init__).
    """

    def __init__(self, player, piecetype):
        assert isinstance(player, CorePlayer)
        self.player = player
        self.type = piecetype
        self.holder = None

    @property
    def location(self):
        if self.holder is None:
            return None
        return self.holder.piece_location(self)

    def clone(self):
        return type(self)(self.player, self.type)

    def __str__(self):
        return ("Piece type %s of player %s (in %s) at %s" %
                (self.type, self.player, self.holder, self.location))
    def __repr__(self):
        return ("<%s(%s, %s)>" %
                (type(self).__name__, self.player, self.type))
