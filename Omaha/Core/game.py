# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'Game', 'UndoableGame' ]

from .player import Player as CorePlayer
from .piece import Piece as CorePiece
from .moves import Move as CoreMove
from .notations import MoveNotation
from .exceptions import InvalidMove
from .misc import Enum
from Overlord.parametrizable import *
from Overlord.misc import classproperty
import Overlord

import logging

class MoveList(object):
    """
    A list of Move objects to encapsulate game history.
    """
    def __init__(self, game):
        self.__game = game
        self.__moves = []
        self.__current = 0

    @property
    def nmoves_done(self):
        """Number of moves done since the game started."""
        return self.__current

    def add_move(self, move):
        """Add given move in the list and increment pointer."""
        self.__moves[self.__current:] = [move]
        self.__current += 1

    def undo(self):
        """
        Undo last move.

        We do not forget it yet (think "redo"), only move the pointer.
        """
        if self.__current > 0:
            self.__current -= 1

    def redo(self):
        """Redo undone move."""
        if self.__current < len(self.__moves):
            self.__current += 1
        else:
            raise IndexError()

    # TODO: that should indeed be a "view" of __moves
    @property
    def played(self):
        """The list of moves effectively played, excluding undone ones."""
        return self.__moves[:self.__current]

    @property
    def last(self):
        """Last move recorded."""
        if self.__current == 0:
            raise IndexError("game has not started")
        return self.__moves[self.__current - 1]

    @property
    def pretty_printed(self):
        result = "[ "
        for i, move in enumerate(self.__moves):
            if self.__current == i:
                result += "<> "
            result += self.__game.default_notation.move_serialization(move) + " "
        if self.__current == len(self.__moves):
            result += "<> "
        result += "]"
        return result


class GameEvent(object):
    """A change in the state of a game."""
    def __init__(self, game, **kwargs):
        super(GameEvent, self).__init__(**kwargs)
        self.__game = game
    @property
    def game(self):
        return self.__game

class MoveEvent(GameEvent):
    def __init__(self, move, **kwargs):
        super(MoveEvent, self).__init__(**kwargs)
        self.__move = move
    @property
    def move(self):
        return self.__move

class UndoEvent(GameEvent):
    pass

class PhaseChangeEvent(GameEvent):
    def __init__(self, oldphase, newphase, **kwargs):
        super(PhaseChangeEvent, self).__init__(**kwargs)
        self.__oldphase, self.__newphase = oldphase, newphase
    @property
    def oldphase(self):
        return self.__oldphase
    @property
    def newphase(self):
        return self.__newphase

class GameOutcomes(Enum):
    Winner = "winner"
    IsDraw = "draw"
    Resign = "resign"

class GamePhases(Enum):
    SettingUp = "setting up"
    Playing = "playing"
    Suspended = "suspended"
    Over = "over"

class Game(Parametrizable):
    """Abstract base class."""

    __metaclass__ = Overlord.ParametrizableOuterclass
    __innerclasses__ = {'Piece', 'Move',
                        'MoveEvent', 'UndoEvent', 'PhaseChangeEvent',
                        'DefaultMoveLocation'}

    __logger = logging.getLogger('Omaha.Core.game.Game')

    Piece = CorePiece
    Move = CoreMove

    Phases = GamePhases
    Outcomes = GameOutcomes

    MoveEvent = MoveEvent
    UndoEvent = UndoEvent
    PhaseChangeEvent = PhaseChangeEvent

    DefaultMoveLocation = MoveNotation

    player_names = NotImplemented

    def __init__(self, import_mode=False, **kwargs):
        super(Game, self).__init__(**kwargs)
        self.__importmode = import_mode
        self.is_set_up = False
        self.phase = self.Phases.SettingUp
        self.pieces = []
        self.__moves = MoveList(self)
        self.players = None
        self.whose_turn = None
        self.__listeners = []
        self.context["holder_types"] = set(self.holder_classes)
        self.default_notation = self.DefaultMoveLocation(self)

        self.__ui_ready = False
        self.__players_ready = None
        # endgame outcome
        self.outcome = {}
        # game outcome proposed by each player
        self.proposed_outcomes = {}

        self.create_players()

    def clone(self):
        c = super(Game, self).clone()
        c.phase = self.phase
        c.__importmode = self.__importmode

        c.players = []
        for i, player in enumerate(self.players):
            newplayer = CorePlayer(player.name, prev=c.players[-1] if i else None)
            newplayer.orientation = player.orientation
            newplayer.homerow = player.homerow
            c.players.append(newplayer)
        c.whose_turn = c.players[self.players.index(self.whose_turn)]

        for piece in self.pieces:
            newpiece = piece.clone()
            newpiece.player = c.players[self.players.index(piece.player)]
            c.create_piece(newpiece)

        return c

    def _update_cloned_pieces(self, original, holder):
        for piece in holder.pieces:
            assert piece.holder is not holder
            loc = holder.piece_location(piece)
            newpiece = self.pieces[original.pieces.index(piece)]
            assert newpiece.type is piece.type
            assert newpiece.player is self.players[original.players.index(piece.player)]
            assert newpiece.player.name == piece.player.name
            orig_pieces = set(holder.pieces)
            holder.replace_piece(loc, newpiece)
            newpiece.holder = holder
            symdiff = orig_pieces.symmetric_difference(set(holder.pieces))
            assert len(symdiff) == 2, \
                "_update_cloned_pieces: len(symdiff) should be 2: %s" % (symdiff,)
            assert piece in symdiff
            assert newpiece in symdiff

    def clone_location(self, loc, clone):
        raise NotImplementedError()

    @classproperty
    @classmethod
    def context_key(cls):
        return Game

    @property
    def is_importing(self):
        return self.__importmode
    def done_importing(self):
        self.__importmode = False

    def setup(self):
        assert not self.is_set_up
        self.setup_players()
        self.create_holders()
        self.set_start_position()
        self.is_set_up = True

    def setup_players(self):
        self.whose_turn = self.players[0]
        self.__players_ready = [ False for p in self.players ]

    def change_phase(self, newphase):
        oldphase = self.phase
        self.phase = newphase
        for l in self.__listeners:
            l(self.PhaseChangeEvent(game=self,
                                    oldphase=oldphase, newphase=newphase))
        self.__logger.info("(%x) phase now %s.", id(self), newphase)

    def ui_suspend(self):
        "Suspend game on behalf of UI"
        self.__ui_ready = False
        self.__logger.debug("ui_suspend() => %s, UI=%s",
                            self.__players_ready, self.__ui_ready)
        if self.phase is self.Phases.Playing:
            self.change_phase(self.Phases.Suspended)
    def ui_resume(self):
        """Start/resume game on behalf of UI.

        The game will really (re)start only when players are ready.
        """
        self.__ui_ready = True
        self.__logger.debug("ui_resume() => %s, UI=%s",
                            self.__players_ready, self.__ui_ready)
        if ( self.phase is not self.Phases.Playing
             and False not in self.__players_ready ):
            self.change_phase(self.Phases.Playing)

    def signal_ready(self, player, ready):
        assert player in self.players
        assert ready in [ True, False ]
        self.__players_ready[self.players.index(player)] = ready
        self.__logger.debug("signal_ready(%s, %s) => %s, UI=%s",
                            player, ready, self.__players_ready, self.__ui_ready)
        if False in self.__players_ready:
            if self.phase is self.Phases.Playing:
                self.change_phase(self.Phases.Suspended)
        elif self.__ui_ready:
            # everyone is ready: launch game
            if self.phase is not self.Phases.Playing:
                self.change_phase(self.Phases.Playing)

    def declare_resignation(self, player):
        self.outcome[player] = self.Outcomes.Resign
        player.prev.next = player.next
        player.next.prev = player.prev
        if player.next.next == player.next:
            self.outcome[player.next] = self.Outcomes.Winner
            self.change_phase(self.Phases.Over)
        elif self.whose_turn == player:
            self.next_player()

    def propose_outcome(self, player, outcome):
        "Let player propose an outcome, and settle the game when everybody OKs"
        self.proposed_outcomes[player] = outcome
        # don't settle if one player opinion differs
        if set(self.proposed_outcomes.keys()) == set(self.players):
            if not reduce(lambda x, y: x if x == y else False,
                          self.proposed_outcomes.values()):
                return

        # when only 2 players, settle when one claims the other wins
        elif len(self.players) == 2:
            if  (outcome != {player.next: self.Outcomes.Winner} and
                 outcome != {player: self.Outcomes.Resign}):
                return

        # no idea!
        else:
            return

        # we have settled !
        self.outcome = outcome
        self.change_phase(self.Phases.Over)

    def create_piece(self, piece):
        """Add a piece to the game."""
        self.pieces.append(piece)

    def new_piece_at(self, location, *piece_args):
        """
        Create a new piece, put it on given location, and return it.

        The piece is created as instance of the Piece subclass
        specified by the Game subclass being used, and that piece
        constructor is passed the requested arguments.
        """
        piece = self.Piece(*piece_args)
        self.create_piece(piece)
        location.holder.put_piece(location, piece)
        return piece

    def make_move(self, player, move, anticipate=True):
        assert player is self.whose_turn
        self.__logger.debug("(%x) make_move %s", id(self),
                            self.default_notation.move_serialization(move))
        self.premove_check(player, move)
        self.supplement_and_check_move(player, move, anticipate=anticipate)
        # publish move early, so opponents get a chance to refuse
        # before we really apply anything - FIXME: not a good reason
        # (too little time, use "refuse" protocol instead)
        for l in self.__listeners:
            l(self.MoveEvent(game=self, move=move))
        # now really apply the move
        self.do_move(player, move)
        self.postmove_record(player, move)

    def premove_check(self, player, move):
        """Check the move wrt game state (as opposed to game position).

        Default implementation refuses the move when:
         * we are in a known non-playing phase of the game;
         * it is not the player's turn."""

        if not isinstance(move, self.Move):
            raise InvalidMove("Move %r has Wrong type %r, expected %r" %
                              (move, type(move), self.Move))

        if self.is_importing:
            if self.phase is not self.Phases.SettingUp:
                raise InvalidMove("game is not being set up, phase is %s" %
                                  (self.phase,))
        else:
            self.check_playing_phase()

        # wheck whose turn it is
        if self.whose_turn != player:
            raise InvalidMove("Not player's turn.")

    def supplement_and_check_move(self, player, move, anticipate):
        """Check the move wrt game position, augment it with forced and optional changes.

        Also anticipate or not resulting position for more checks
        (eg. forbidding to move one's king into check).
        """
        assert player in self.players
        move.assert_locations(self.holders)

    def check_has_game_started(self):
        if self.phase is self.Phases.SettingUp:
            raise InvalidMove("game has not started yet")

    def check_playing_phase(self):
        self.check_has_game_started()
        if self.phase is not self.Phases.Playing:
            msg = {self.Phases.Suspended: "game has been suspended",
                   self.Phases.Over: "game is over"
                  }[self.phase]
            raise InvalidMove(msg)

    def is_playing_phase(self):
        try:
            self.check_playing_phase()
            return True
        except InvalidMove as ex:
            return False

    def postmove_record(self, player, move):
        """Record the move and switch to next player."""
        # record move
        self.__moves.add_move(move)
        if not self.is_importing:
            self.__logger.debug("(%x) Move history now: %s", id(self), self.__moves.pretty_printed)
        self.next_player()

    def next_player(self):
        self.whose_turn = self.whose_turn.next

    def do_move(self, player, move):
        raise NotImplementedError()

    def register_listener(self, l):
        "Register function of one argument to be called for game event."
        self.__listeners.append(l)
    def unregister_listener(self, l):
        self.__listeners.remove(l)

    def create_players(self):
        player = None
        players = []
        for name in self.player_names:
            player = CorePlayer(name, prev=player)
            players.append(player)
        self.players = tuple(players)

    def create_holders(self):
        "To be overriden by subclasses."
        # FIXME: we should ensure this is consistent with self.holder_classes
        pass

    def set_start_position(self):
        raise NotImplementedError()

    @property
    def holders(self):
        return []
    @classproperty
    @classmethod
    def holder_classes(cls):
        return []

    @property
    def moves(self):
        return self.__moves

    ## I/O

    def save(self, filename):
        "Raw dump of game moves in default notation."
        outfile = open(filename, "w")
        for move in self.__moves.played:
            outfile.write(self.default_notation.move_serialization(move))
            outfile.write("\n")

    ## undo-related

    def undo_move(self):
        if not isinstance(self, UndoableGame):
            raise InvalidMove("Game %r does not support undo" % type(self))
        self.check_has_game_started()

        try:
            move = self.moves.last
        except IndexError:
            self.__logger.warning("nothing to undo")
            return

        # back to the turn of the player we're undoing the move
        self.whose_turn = self.whose_turn.prev
        # unrecord
        self.moves.undo()

        self._undo_move(move)

        # publish undo
        for l in self.__listeners:
            l(self.UndoEvent(game=self))

        self.__logger.debug("(%x) Move history now: %s", id(self), self.moves.pretty_printed)

    def _undo_move(self, move):
        raise NotImplementedError()

    def redo_move(self):
        if not isinstance(self, UndoableGame):
            raise InvalidMove("Game %r does not support undo" % type(self))
        self.check_has_game_started()

        self.moves.redo()
        for l in self.__listeners:
            l(self.MoveEvent(game=self, move=self.moves.last))
        self.do_move(self.whose_turn, self.moves.last)
        self.__logger.debug("(%x) Move history now: %s", id(self), self.moves.pretty_printed)
        self.next_player()

    def refuse_move(self, move, player):
        "Let `player` declare he refuses `move` in the movelist."

        if not isinstance(self, UndoableGame):
            # FIXME: there should not be any such dependency on undo here
            raise InvalidMove("Game %r does not support undo" % type(self))
        self.check_playing_phase()

        # Only allow refusing last move
        assert move is self.moves.last
        # We have no adjudication support yet, let anyone refuse :)
        self.undo_move()

    ## tooltip support

    has_tooltip = False

    def location_tooltip_text(self, loc):
        raise NotImplementedError()

class UndoableGame(Game):
    __metaclass__ = ParametrizableOuterclassABCMeta
