# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'Player' ]

class Player(object):
    """
    Base class for players.
    Used as hierarchy root for proper __init__ chaining, and
    for checking by introspection.
    """
    def __init__(self, name, prev=None):
        self.name = name
        if prev is None:
            self.next = self
            self.prev = self
        else:
            self.next = prev.next
            prev.next = self
            self.prev = prev
            self.next.prev = self
    def __str__(self):
        return '<Player "%s" at 0x%x>' % (self.name, id(self))
