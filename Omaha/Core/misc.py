# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import flufl.enum
from flufl.enum import _enum

__all__ = ['Enum']

if flufl.enum.__version__.startswith("3.3"):
    class EnumValue(_enum.EnumValue):
        @property
        def value(self):
            return self._value
        def __repr__(self):
            return '<EnumValue: {0}.{1} [{2}]>'.format(
                self._enum.__name__, self._name, self._value)

    class EnumMetaclass(_enum.EnumMetaclass):
        def __repr__(cls):
            enums = ['{0}: {1}'.format(
                cls._enums[k], k) for k in sorted(cls._enums)]
            return '<{0} {{{1}}}>'.format(cls.__name__,
                                          _enum.COMMASPACE.join(enums))
        def __init__(cls, name, bases, attributes):
            super(EnumMetaclass, cls).__init__(name, bases, attributes)
            for key, value in cls._enums.items():
                ev = getattr(cls, value)
                setattr(cls, value, EnumValue(ev._enum, ev._value, ev._name))

    class Enum(_enum.Enum):
        __metaclass__ = EnumMetaclass
        def __repr__(self):
            return '<EnumValue: {0}.{1} [{2}]>'.format(
                self._enum.__name__, self._name, self._value)
else:
    Enum = _enum.Enum

###

def log_stack_trace(logger):
    import traceback
    logger.warn(''.join(traceback.format_stack()))
