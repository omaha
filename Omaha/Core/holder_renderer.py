# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'HolderRenderer', 'LocHighlights' ]

import Overlord
from Overlord.misc import classproperty

class HolderRenderer(Overlord.Parametrizable):
    """
    Base class for holder renderers.
    Used as hierarchy root for proper __init__ chaining, and
    for checking by introspection.
    """
    def __init__(self, holder, gui, **kwargs):
        super(HolderRenderer, self).__init__(**kwargs)
        self.holder = holder
        self.gui = gui

    @classproperty
    @classmethod
    def context_key(cls):
        return HolderRenderer

    def tile_center(self, location):
        raise NotImplementedError()

    def draw(self, drawing_context, rectangle):
        pass

    def point_to_location(self, x, y):
        """
        Map coordinates in canvas to a holder location.
        """
        raise NotImplementedError()

class LocHighlights(object):
    "Namespacing-only class for type of location highlights"

    # locations that are part of the move being composed interactively
    COMPOSING = "composing"
    # locations that may become part of the move being composed
    MAYBE = "maybe"
    # locations that are available as continuations of the move being composed
    REACHABLE = "reachable"
