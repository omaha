# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.GenericChesslikeMoves import PreciousKings
from .abstract.ShogiGame import ShogiGameWithDrops
from .abstract.StandardShogi import StandardShogi
from .abstract.StandardShogi import EnglishShogiWithDropsNotation
from .piecetypes.shogi import ShogiFacetype

class Shogi(ShogiGameWithDrops, StandardShogi, PreciousKings):
    DefaultMoveLocation = EnglishShogiWithDropsNotation

    def has_king_status(self, piece):
        return piece.type is ShogiFacetype.KING

    def save(self, filename):
        assert filename.endswith(".pgn")
        from Omaha.GameIO.PGNWriter import PGNGameWriter
        writer = PGNGameWriter(self)
        with open(filename, "w+") as f:
            writer.write_to(f)
