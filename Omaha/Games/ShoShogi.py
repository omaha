# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.GenericChesslikeMoves import LastKingStanding
from .abstract.ShogiGame import ShogiGameWithoutDrops
from .abstract.StandardShogi import StandardShogi
from .abstract.StandardShogi import EnglishShogiNotation
from .piecetypes.shogi import ShogiFacetype
from .piecetypes.shoshogi import ShoShogiFacetype
from . import movelib

class ShoShogi(StandardShogi, ShogiGameWithoutDrops, LastKingStanding):

    MOVETYPES = dict(StandardShogi.MOVETYPES)
    MOVETYPES.update({
        ShoShogiFacetype.DRUNKENELEPHANT: (
            (movelib.default_move, movelib.MOVETYPE_CHU_DRUNKEN, None),),
        ShoShogiFacetype.CROWNPRINCE: (
            (movelib.default_move, movelib.MOVETYPE_CHESS_KING, None),),
        })

    DefaultMoveLocation = EnglishShogiNotation

    def set_start_position(self):
        super(ShoShogi, self).set_start_position()
        for player in self.players:
            self.new_piece_on_board(
                self.player_relative_pos(player, 4, 1), player,
                ShoShogiFacetype.DRUNKENELEPHANT,
                ShoShogiFacetype.CROWNPRINCE)

    def has_king_status(self, piece):
        return piece.type in (ShogiFacetype.KING, ShoShogiFacetype.CROWNPRINCE)
