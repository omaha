# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Core import InvalidMove
import Overlord

import logging

logger = logging.getLogger('Omaha.Games.abstract.GenericChesslikeMoves')

MOVERANGE_INF = "infinity"

class Capture(object):
    "Container tracking info on a move's capture, for applying and undoing the capture"
    def __init__(self, piece):
        assert piece
        self.piece = piece
    def __str__(self):
        return "Capture(%s)" % (self.piece,)

class GenericChesslikeMove(Core.SimpleDisplaceMove):
    __metaclass__ = Overlord.OuterClass
    __innerclasses__ = {'Capture'}
    Capture = Capture
    def __init__(self, **kwargs):
        self.piece = None       # The piece being moved
        super(GenericChesslikeMove, self).__init__(**kwargs)
        self.type = None        # castling, en-passant, etc.
        self.capture = None # a Capture object, or None
    def clone(self, game, game_clone):
        newmove = super(GenericChesslikeMove, self).clone(game, game_clone)
        if self.capture is not None:
            newpiece = game_clone.pieces[game.pieces.index(self.capture.piece)]
            assert newpiece.type is self.capture.piece.type
            newmove.capture = self.Capture(newpiece)
        return newmove
    def set_source(self, location):
        super(GenericChesslikeMove, self).set_source(location)
        assert self.source is location
        if self.source is not None:
            self.piece = self.source.holder.piece_at(self.source)
            assert self.piece, "no piece at {0}".format(self.source)
    def store_captured_piece(self, piece):
        "Store `piece` as being captured by the move, with enough info for undo"
        assert self.capture is None, \
            "capture already %s (wants to capture %s)" % (self.capture, piece)
        self.capture = self.Capture(piece)

class GenericChesslikeMovesGame(Core.Game):

    # class variable to overload with the actual declaration
    # for a given game
    MOVETYPES = None

    Move = GenericChesslikeMove

    def supplement_and_check_move(self, player, move, anticipate):
        super(GenericChesslikeMovesGame, self).supplement_and_check_move(player, move, anticipate)

        if move.source is None:
            raise InvalidMove("move must have a source location")
        if move.target is None:
            # FIXME: should be IncompleteMove ?
            raise InvalidMove("move must have a target location")

        targetpiece = move.target.holder.piece_at(move.target)
        # basic checks
        if move.piece is None:
            raise InvalidMove("no piece to move")
        assert move.piece.player in self.players, \
            "%s has bad owner, expecting one of %s" % (move.piece, self.players)
        if move.piece.player is not player:
            raise InvalidMove("player %s can only move his own pieces, not %s" %
                              (player, move.piece))
        if targetpiece:
            if targetpiece.player is player:
                raise InvalidMove("player cannot capture his own pieces")
            if move.capture is not None:
                # capture was already filled, eg. because we're a clone
                assert move.capture.piece is targetpiece
            else:
                move.store_captured_piece(targetpiece)

    def check_standard_moves(self, player, move, anticipate):
        if move.piece.type in self.MOVETYPES:
            # select movetypes entry
            assert len(self.MOVETYPES[move.piece.type]) > 0
            for predicate, movetypes, typeid in self.MOVETYPES[move.piece.type]:
                # check if this move is allowed in this movetypes set
                for movetype_direction, movetype_range in movetypes:
                    if not move.source.holder. \
                            move_follows_direction(move, movetype_direction,
                                                   player.orientation):
                        continue
                    if movetype_range is MOVERANGE_INF:
                        if predicate(move, movetype_direction, None):
                            # allowed, stop searching
                            break
                    nunits = move.source.holder.nunits_in_direction(
                        move, movetype_direction, player.orientation)
                    if isinstance(movetype_range, tuple):
                        range_min, range_max = movetype_range
                        if  (nunits and nunits <= range_max
                             and nunits >= range_min):
                            if predicate(move, movetype_direction, nunits):
                                # allowed, stop searching
                                break
                    else:
                        if nunits and nunits <= movetype_range:
                            if predicate(move, movetype_direction, nunits):
                                # allowed, stop searching
                                break
                else:
                    # move not accepted by this movetypes set, try next
                    continue

                # move was accepted, skip next movetypes sets
                break

            else:
                # all movetypes sets exhausted without a match
                raise InvalidMove("cannot move there")

            # register what type this movetype has
            move.type = typeid
        else:
            raise InvalidMove("no moves defined for %s" % move.piece.type)

        # forbid moving into check
        if anticipate:
            clone = self.clone()
            clone_player = clone.whose_turn # FIXME: accurate ?
            assert clone_player.name is player.name
            clone_move = move.clone(self, clone)
            if move.source is not None:
                assert clone_move.source is not None
            assert clone_move.piece.player is clone_player, \
                "%s != %s" % (clone_move.piece.player, clone_player)
            logger.debug("move: "+str(clone_move))
            try:
                clone.make_move(clone_player, clone_move, anticipate=False)
            except Exception as ex:
                logger.error("move failed on clone")
                raise
            checking_piece = clone.checking_piece(clone_player)
            if checking_piece:
                raise InvalidMove("player's king would be in check by %s" %
                                  (checking_piece,))

    ###

    def _process_captured_piece(self, target_piece, player):
        """Do whatever is needed with `target_piece` captured by `player`.

        Nothing to do by default (we just forget about that piece).
        """
        pass

    def do_move(self, player, move):
        assert isinstance(player, Core.Player)
        assert move.source.holder in self.holders
        assert move.target.holder is self.board

        # Capturing
        if move.capture is not None:
            capture_loc = move.capture.piece.location
            captured_piece = capture_loc.holder.take_piece(capture_loc)
            self._process_captured_piece(captured_piece, player)

        # remove piece from original location
        piece = move.source.holder.take_piece(move.source)
        assert piece is move.piece

        # we don't put piece back into place yet, as some games like
        # Chess handle promotions by using a new piece instead

    def _move_source_for_undo(self, move):
        "Returns Location where to put piece pack when undoing move."
        return move.source

    def _undo_capture(self, move):
        raise NotImplementedError()

    def _undo_move(self, move):
        # remove piece from target location
        piece = move.target.holder.take_piece(move.target)
        # put piece back to source loc
        move.source.holder.put_piece(self._move_source_for_undo(move), piece)

        # put captured piece back into place, promoted if needed
        if move.capture is not None:
            self._undo_capture(move)

    ###

    def checking_piece(self, player):
        "Return a piece causing `player` to be in check, or `None`."
        raise NotImplementedError()

    def is_attacked(self, player, location):
        for piece in self.pieces:
            if piece.location is None:
                # piece permanenly removed from the game, cannot attack
                continue
            if piece.player is player:
                # don't check player's own pieces
                continue
            logger.debug("(%x)  does %s attack %s ?", id(self), piece, location)
            move = self._move_for_attack_test(source=piece.location, target=location)
            try:
                self.supplement_and_check_move(piece.player, move, anticipate=False)
                # move is correct: location is attacked
                return piece
            except InvalidMove:
                # piece does not attack, try next one
                continue
            except Core.IncompleteMove as ex:
                import traceback
                assert False, "_move_for_attack_test caused IncompleteMove(%s), %s" % (
                    ex.paramlist.keys(), traceback.format_exc(ex),)
        return False

    def _move_for_attack_test(self, source, target):
        """Returns a move suitable for checking whether `source` attacks `target`.

        MUST NOT return a move that can raise IncompleteMove;
        subclasses must overload to ensure this."""
        return self.Move(source=source, target=target)

    def valid_move_completions(self, incomplete_move):
        """
        The list of moves that would be accepted given (incomplete) move.
        """
        if incomplete_move.source is None:
            return []
        # FIXME: what if incomplete_move.target is not None ?

        # check if Location has Delta support
        try:
            Delta = incomplete_move.source.holder.Location.Delta
        except AttributeError:
            return []

        piece = incomplete_move.piece

        if piece.type in self.MOVETYPES:
            valid_moves = []
            for predicate, movetypes, typeid in self.MOVETYPES[piece.type]:
                for movetype_direction, movetype_range in movetypes:
                    # generate moves according to direction and range
                    delta = Delta(*movetype_direction) \
                            .player_oriented(self.whose_turn.orientation)
                    target = incomplete_move.source
                    target += delta
                    # loop while target is valid
                    if isinstance(movetype_range, tuple):
                        range_min, range_max = movetype_range
                    else:
                        range_min, range_max = (1, movetype_range)
                    deltacount = 1 # how far we are currently looking
                    while (target.holder.valid_location(target) and
                           (range_max is MOVERANGE_INF or
                            deltacount <= range_max)):
                        # build the move and check the movetype's predicate
                        move = self.Move(source = incomplete_move.source,
                                         target = target)
                        if  (deltacount >= range_min and
                             predicate(move, movetype_direction, None)):
                            valid_moves.append(move)
                        # next iteration
                        target += delta
                        deltacount += 1
            return valid_moves
        else:
            return []

    ###

    has_tooltip = True

    def location_tooltip_text(self, loc):
        if loc is None:
            return None
        piece = loc.holder.piece_at(loc)
        if piece is None:
            return None
        return piece.type.value

###

class PreciousKings(GenericChesslikeMovesGame):
    """A chess-like game where the game is won by mating any king.

    Of course, applies to standard/FIDE chess, as a variant with a
    single king."""

    def has_king_status(self, piece):
        "Must return True if the piece is considered like a king for mating."
        raise NotImplementedError()

    def checking_piece(self, player):
        "Return a piece checking (one of) player's king(s)."
        for p in self.pieces:
            if p.location:
                assert p.location.holder in self.holders
        for p in self.pieces:
            if p.player is not player:
                continue # only consider player's own king(s)
            if not self.has_king_status(p):
                continue
            checking_piece = self.is_attacked(player, p.location)
            if checking_piece:
                return checking_piece
            break # King is OK
        else:
            assert False, "%s has no king" % (player,)
        # not in check
        return False


class LastKingStanding(GenericChesslikeMovesGame):
    "A chess-like game where the game is won by mating the opponent's last king."

    def has_king_status(self, piece):
        "Must return True if the piece is considered like a king for mating."
        raise NotImplementedError()

    def checking_piece(self, player):
        logger.debug("LastKingStanding.checking_piece")
        for p in self.pieces:
            if p.location:
                assert p.location.holder in self.holders
        kings_count = 0
        for p in self.pieces:
            if p.player is not player:
                continue # only consider player's own king
            if not self.has_king_status(p):
                continue
            if p.location is None:
                continue
            kings_count += 1
            checking_piece = self.is_attacked(player, p.location)
            if not checking_piece:
                return False # One royal piece is not attacked
        assert kings_count, "%s has no king" % (player,)
        if kings_count == 1:
            # single remaining royal piece is in check
            return checking_piece
        # all royal pieces are in check, but at least one could survive
        return False
