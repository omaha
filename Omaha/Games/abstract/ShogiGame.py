# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Generic support for standard/modern shogi variants on a square board.

This means standard type of promotion, but not necessarily drops.
"""
from .GenericChesslikeMoves import GenericChesslikeMovesGame
from .RectBoardGame import RectBoardGame
from ..piecetypes.shogi import ShogiFacetype
from Omaha.Core import IncompleteMove, InvalidMove
from Omaha import Core
import Overlord

import re, logging

logger = logging.getLogger('Omaha.Games.abstract.ShogiGame')

class Capture(GenericChesslikeMovesGame.Move.Capture):
    def __init__(self, piece):
        super(Capture, self).__init__(piece)
        self.promoted = None

class ShogiMove(GenericChesslikeMovesGame.Move):
    Capture = Capture
    def __init__(self, **kwargs):
        super(ShogiMove, self).__init__(**kwargs)
        self.originally_promoted = False # Whether that piece was
                                         # promoted before moving
        self.promotes = None

    def clone(self, game, game_clone):
        newmove = super(ShogiMove, self).clone(game, game_clone)
        newmove.promotes = self.promotes
        logger.debug("cloning, capture=%s", self.capture)
        if self.capture is not None:
            newmove.capture.promoted = self.capture.piece.promoted
        return newmove

    def store_captured_piece(self, piece):
        super(ShogiMove, self).store_captured_piece(piece)
        self.promoted = piece.promoted

    def __str__(self):
        result = super(ShogiMove, self).__str__()
        if self.capture:
            result += " [x%s]" % self.capture.piece
        return result

class ShogiPiece(RectBoardGame.Piece):
    def __init__(self, player, piecetype, promoted_type=None):
        super(ShogiPiece, self).__init__(player, piecetype)
        self.__unpromoted_type = piecetype
        self.__promoted_type = promoted_type
    def clone(self):
        c = super(ShogiPiece, self).clone()
        c.__unpromoted_type = self.__unpromoted_type
        c.__promoted_type = self.__promoted_type
        return c
    def has_promotion(self):
        return self.__promoted_type is not None
    @property
    def promoted_type(self):
        return self.__promoted_type
    @property
    def unpromoted_type(self):
        return self.__unpromoted_type
    @property
    def promoted(self):
        return self.type == self.__promoted_type
    def promote(self):
        self.type = self.__promoted_type
    def unpromote(self):
        self.type = self.__unpromoted_type

class WesternShogiPieceNotation(Core.PieceNotation):
    "Piece notation for Shogi using the latin alphabet"
    # enum-like for piece_name(face=...)
    promoted=object()
    unpromoted=object()
    def piece_name(self, piece, face=None):
        raise NotImplementedError()

class WesternShogiNotation(Core.MoveNotation):
    """Notation for Shogi with coordinates in european "1a" style"""
    PieceNotation = WesternShogiPieceNotation
    class LocationNotation(Core.SingleBoardCoordinateNotation,
                           Core.NumberColumnNotation,
                           Core.LetterRowNotation):
        "LocationNotation for WesternShogiNotation"

    def move_serialization(self, move):
        return "%s%s%s%s%s" % (
            self.piece_notation.piece_name(move.piece,
                                           face=(self.piece_notation.promoted
                                                 if move.originally_promoted
                                                 else self.piece_notation.unpromoted)),
            self.location_notation.location_name(move.source),
            "x" if move.capture is not None else "-",
            self.location_notation.location_name(move.target),
            "+" if move.promotes else "")

    def move_deserialization(self, string, player):
        # check for a normal move
        # FIXME: check for type/capture/promotion
        m = re.match(r"((?:\+?[A-Z]+)?)((\d+)([a-w]+))([-x]?)(\d+)([a-w]+)(\+?)$", string)
        if m:
            source = self.game.board.Location(self.game.board,
                                              self.location_notation.col(m.group(3)),
                                              self.location_notation.row(m.group(4)))
            target = self.game.board.Location(self.game.board,
                                              self.location_notation.col(m.group(6)),
                                              self.location_notation.row(m.group(7)))
            move = self.game.Move(source=source, target=target)
            if len(m.group(8)) == 0:
                move.promotes = False
            else:
                move.promotes = True
            return move

        raise Core.ParseError("Could not parse '%s' as a serialized shogi move" %
                              string)

class ShogiGame(RectBoardGame, GenericChesslikeMovesGame):

    SIDE_SENTE = "sente"
    SIDE_GOTE  = "gote"

    Piece = ShogiPiece
    Move = ShogiMove

    notations = {}

    player_names = ("Sente", "Gote")

    def __init__(self, **kwargs):
        super(ShogiGame, self).__init__(**kwargs)
        self.boardwidth, self.boardheight = None, None
        self.promotion_rows_count = None

    def setup_players(self):
        super(ShogiGame, self).setup_players()
        self.players[0].color = self.SIDE_SENTE
        self.players[1].color = self.SIDE_GOTE
        if self.handicap is not ():
            self.whose_turn = self.players[1]

    def _in_promotion_zone(self, location, player):
        return (location.holder is self.board and
                abs(location.row - player.next.homerow) + 1 <=
                self.promotion_rows_count)

    def supplement_and_check_move(self, player, move, anticipate):
        super(ShogiGame, self).supplement_and_check_move(player, move, anticipate)

        self.check_standard_moves(player, move, anticipate)

        # adjustements
        if move.piece.promoted:
            move.originally_promoted = True

        # handle piece promotion
        if  ((self._in_promotion_zone(move.source, player) or
              self._in_promotion_zone(move.target, player)) and
             move.source.holder is self.board and
             move.piece.has_promotion() and
             not move.piece.promoted):
            logger.debug("(%x) move has unspecified promotion: %s", id(self), move)
            if move.promotes is None:
                raise IncompleteMove({
                    'promotes': Overlord.params.Choice(
                        label = "Promotes",
                        alternatives = dict(yes = True,
                                            no = False),
                        default = True)
                    })
        elif move.promotes is True:
            raise InvalidMove("illegal promotion request")

    def _move_for_attack_test(self, source, target):
        m = super(ShogiGame, self)._move_for_attack_test(source, target)
        m.promotes = False
        return m

    def do_move(self, player, move):
        super(ShogiGame, self).do_move(player, move)

        # promotion
        if move.promotes is True:
            move.piece.promote()

        # put (possibly modified) piece at target location
        self.board.put_piece(move.target, move.piece)

    def _undo_capture(self, move):
        # give back to its player
        move.capture.piece.player = self.whose_turn.next
        # re-promote piece it was when captured
        if move.capture.promoted:
            move.capture.piece.promote()
        move.source.holder.put_piece(move.target, move.capture.piece)

    def _undo_move(self, move):
        super(ShogiGame, self)._undo_move(move)

        # un-promote
        if move.promotes is True:
            move.piece.unpromote()

    @property
    def handicap(self):
        raise NotImplementedError()

    ###

    def location_tooltip_text(self, loc):
        # this overrides the GenericChesslikeMovesGame implementation
        if loc is None:
            return None
        piece = loc.holder.piece_at(loc)
        if piece is None:
            return None
        if piece.has_promotion():
            if not piece.promoted:
                return "{0.type.value}\nPromotes to {0.promoted_type.value}".format(piece)
            else:
                return "{0.type.value}\nPromoted from {0.unpromoted_type.value}".format(piece)
        else:
            return piece.unpromoted_type.value

###

class ShogiGameWithoutDrops(ShogiGame):
    "Trivial class minimally distinct from `ShogiGameWithDrops`"
    pass

###

from PlayerPools import PlayerPools

class WesternShogiWithDropsNotation(WesternShogiNotation):
    def move_serialization(self, move):
        if move.source.holder is not self.game.board:
            return "%s*%s" % (
                self.piece_notation.piece_name(move.piece,
                                               face=self.piece_notation.unpromoted),
                self.location_notation.location_name(move.target))
        return super(WesternShogiWithDropsNotation, self).move_serialization(move)

    def move_deserialization(self, string, player):
        # check for a drop
        m = re.match(r"(.)\*(\d+)([a-w]+)$", string)
        if m:
            # identify requested piece type
            piece_type = self.piece_notation.piece_type(m.group(1))
            # check for piece to drop
            source = self.game.pools[player].piece_location_by_type(piece_type)
            if source is None:
                raise Core.ParseError("No such piece to drop")
            return self.game.Move(
                source=source,
                target=self.game.board.Location(self.game.board,
                                                self.location_notation.col(m.group(2)),
                                                self.location_notation.row(m.group(3))))

        return super(WesternShogiWithDropsNotation, self).move_deserialization(string, player)


class ShogiGameWithDrops(PlayerPools, ShogiGame):
    def _process_captured_piece(self, target_piece, player):
        """Move captured piece to player's pool"""
        target_piece.player = player
        target_piece.unpromote()
        self.pools[player].put_piece(
            self.pools[player].Location(self.pools[player], None),
            target_piece)

    def _move_source_for_undo(self, move):
        if move.source.holder is not self.board:
            # undoing a drop: must not specify precise Location inside pool
            return move.source.holder.Location(move.source.holder, index=None)
        return super(ShogiGameWithDrops, self)._move_source_for_undo(move)

    def _undo_move(self, move):
        # first remove any captured piece from pool, to avoid any bad
        # interaction with its move back onto the board
        if move.capture is not None:
            self.pools[self.whose_turn].take_piece(
                self.pools[self.whose_turn] \
                    .piece_location(move.capture.piece))
        super(ShogiGameWithDrops, self)._undo_move(move)

    def check_standard_moves(self, player, move, anticipate):
        if move.source.holder is self.board:
            super(ShogiGameWithDrops, self).check_standard_moves(player, move, anticipate)
        else:
            # check drop validity
            if move.target.holder.piece_at(move.target) is not None:
                raise InvalidMove("cannot drop on occupied location")
            # FIXME: lacks "no more than 2 unpromoted pawns in file"
            # FIXME: lacks "dropped pawn must not checkmate"

###

class SANNotation(Core.MoveNotation):
    PieceNotation = WesternShogiPieceNotation
    class LocationNotation(Core.SingleBoardCoordinateNotation,
                           Core.LetterColumnNotation, Core.NumberRowNotation):
        "MoveNotation for ShogiGame.SANNotation"

    movename = "shogi SAN move"

    def move_serialization(self, move):
        # drops
        if move.source.holder is not self.game.board:
            return "%s@%s" % (self.piece_notation.piece_name(
                move.piece, face=self.piece_notation.unpromoted),
                              self.location_notation.location_name(move.target))

        # pawn special case
        if move.piece.type is ShogiFacetype.PAWN:
            if move.capture is not None:
                s = "%sx%s" % (self.location_notation.colname(move.source),
                               self.location_notation.location_name(move.target))
            else:
                s = self.location_notation.location_name(move.target)

        # FIXME: spec requires emitting a minimized source
        return (self.piece_notation.piece_name(move.piece,
                                               face=(self.piece_notation.promoted
                                                     if move.originally_promoted
                                                     else self.piece_notation.unpromoted))
                + self.location_notation.location_name(move.source)
                + ("x" if move.capture is not None else "")
                + self.location_notation.location_name(move.target)
                + ("+" if move.promotes else ""))

    regexp = r"(\+?[A-Z]?)([a-w]*)(\d*)(x?)([a-w]+)(\d+)(\+?)(?:#?)"
    def move_deserialization(self, string, player):
        logger.debug("move_deserialization(%s, player=%s)", string, player)
        # check for a drop
        m = re.match(r"(.)@([a-w]+)(\d+)(?:#?)$", string)
        if m:
            # identify requested piece type
            piece_type = self.piece_notation.piece_type(m.group(1))
            # check for piece to drop
            source = self.game.pools[player].piece_location_by_type(piece_type)
            if source is None:
                raise Core.ParseError("No such piece to drop")
            return self.game.Move(
                source=source,
                target=self.game.board.Location(self.game.board,
                                                self.location_notation.col(m.group(2)),
                                                self.location_notation.row(m.group(3))))

        source_col = source_row = None
        match = re.match(self.regexp + "$", string)
        if not match:
            raise Core.ParseError("Could not parse %r as a move" % (string,))

        if match.group(1):
            if match.group(1)[0] == '+':
                if len(match.group(1)) == 1:
                    raise Core.ParseError(
                        "Could not parse %r as a promoted piece piece in %r" %
                        (match.group(1), string))
                piece_type = self.piece_notation.piece_type(match.group(1)[1])
                promoted = True
            else:
                piece_type = self.piece_notation.piece_type(match.group(1))
                promoted = False
        else:
            piece_type = ShogiFacetype.PAWN
            promoted = False

        if match.group(2):
            source_col = self.location_notation.col(match.group(2))
        if match.group(3):
            source_row = self.location_notation.row(match.group(3))

        target = self.game.board.Location(self.game.board,
                                          self.location_notation.col(match.group(5)),
                                          self.location_notation.row(match.group(6)))

        try:
            target_piece = target.holder.piece_at(target)
        except IndexError:
            raise Core.ParseError("Move %r refers to non-existent square" % (string,))
        if match.group(4) and target_piece is None:
            raise Core.ParseError("Move to empty %s cannot be a capture" % (target,))

        promotes = (len(match.group(7)) != 0)

        candidates = []
        for piece in self.game.pieces:
            if player and piece.player is not player:
                continue
            if piece.unpromoted_type is not piece_type:
                continue
            if piece.promoted != promoted:
                continue
            loc = self.game.board.piece_location(piece)
            if loc is None:     # not on board, probably captured
                continue
            if source_col is not None and source_col != loc.col:
                continue
            if source_row is not None and source_row != loc.row:
                continue
            if piece_type is ShogiFacetype.PAWN:
                try:
                    target_piece = target.holder.piece_at(target)
                except IndexError:
                    raise Core.ParseError("Move %r refers to non-existent square" % (string,))
                if loc.col != target.col:
                    continue
            #
            move = self.game.Move(source=loc, target=target)
            move.promotes = promotes
            try:
                self.game.supplement_and_check_move(self.game.whose_turn, move, anticipate=True)
            except Core.InvalidMove as ex:
                logger.debug("%s not suitable for %s: %s", loc, string, ex.message)
                continue
            candidates.append(move)
        if len(candidates) == 0:
            # too detailed ...
            raise Core.ParseError("Found no suitable %s for %s" % (piece_type, string))
        if len(candidates) > 1:
            raise Core.ParseError("Could not disambiguate %s between: %s" %
                                  (string, ' or '.join(str(c.source) for c in candidates)))
        move = candidates[0]

        return move

###

Core.UndoableGame.register(ShogiGame)
