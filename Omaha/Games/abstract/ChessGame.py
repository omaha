# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .GenericChesslikeMoves import GenericChesslikeMovesGame
from .SingleBoardGame import SingleBoardGame
from ..piecetypes.chess import ChessFacetype
from Omaha import Core
from Omaha.Core import InvalidMove
import re, logging

logger = logging.getLogger('Omaha.Games.abstract.ChessGame')

class Capture(GenericChesslikeMovesGame.Move.Capture):
    def __init__(self, piece):
        super(Capture, self).__init__(piece)
        self.location = None

class ChessMove(GenericChesslikeMovesGame.Move):
    Capture = Capture
    def __init__(self, **kwargs):
        super(ChessMove, self).__init__(**kwargs)
        self.promotion = None      # piece after promotion if promoting a pawn
        self.side_effect = None    # eg. move of castling rook
        self.piece_had_moved = None

    def clone(self, game, game_clone):
        newmove = super(ChessMove, self).clone(game, game_clone)
        newmove.promotion = self.promotion
        logger.debug("cloning, capture=%s", self.capture)
        if self.capture is not None:
            newmove.capture.location = game.clone_location(self.capture.location,
                                                           game_clone)
        return newmove

    def assert_locations(self, holders):
        super(ChessMove, self).assert_locations(holders)
        if self.capture is not None:
            assert self.capture.location.holder in holders

    def store_captured_piece(self, piece):
        # store piece captured by normal capture, en-passant captures
        # are set directly by the game
        super(ChessMove, self).store_captured_piece(piece)
        self.capture.location = self.target

    def __str__(self):
        result = super(ChessMove, self).__str__()
        if self.capture:
            result += " [x%s]" % self.capture.piece
        return result

class ChessPiece(GenericChesslikeMovesGame.Piece):
    def __init__(self, player, piecetype):
        super(ChessPiece, self).__init__(player, piecetype)
        self.has_moved = False
    def clone(self):
        c = super(ChessPiece, self).clone()
        c.has_moved = self.has_moved
        return c

class EnglishChessNotation(Core.PieceNotation):
    PIECE_TEXT = {
        ChessFacetype.KING   : "K",
        ChessFacetype.QUEEN  : "Q",
        ChessFacetype.BISHOP : "B",
        ChessFacetype.KNIGHT : "N",
        ChessFacetype.ROOK   : "R",
        ChessFacetype.PAWN   : "P"
    }
    def piece_name(self, piece):
        return self.PIECE_TEXT[piece.type]
    def piecename_bytype(self, typ):
        return self.PIECE_TEXT[typ]
    def piece_type(self, name):
        for typ, typname in self.PIECE_TEXT.items():
            if typname == name:
                return typ
        else:
            raise LookupError("Invalid piece type '%s'" % name)

class ChessCoordinateNotation(Core.ChessLikeCoordinateNotation):
    movename = "chess coordinates move"
    PieceNotation = EnglishChessNotation
    PROMOTION_NOTATIONS = dict(q=ChessFacetype.QUEEN,
                               n=ChessFacetype.KNIGHT,
                               r=ChessFacetype.ROOK,
                               b=ChessFacetype.BISHOP)

    def move_serialization(self, move):
        if move.promotion:
            if isinstance(move.promotion, ChessPiece):
                promotype = move.promotion.type
            else:
                promotype = move.promotion
            letters = [ letter
                        for letter, piecetype in self.PROMOTION_NOTATIONS.items()
                        if piecetype == promotype ]
            assert len(letters) == 1, \
                "Cannot express promotion to %r" % promotype
            promotion = letters[0]
        else:
            promotion = ""
        return (super(ChessCoordinateNotation, self).move_serialization(move)
                + promotion)

    regexp = (r"(?:.?)" +
              Core.ChessLikeCoordinateNotation.regexp +
              r"([a-w]?)")
    def move_from_match(self, match, player):
        move = super(ChessCoordinateNotation, self).move_from_match(match, player)
        if len(match.group(3)) == 0:
            move.promotion = None
        else:
            move.promotion = self.PROMOTION_NOTATIONS[match.group(3)]
        return move

class ChessGame(SingleBoardGame, GenericChesslikeMovesGame):
    COLOR_WHITE = "white"
    COLOR_BLACK = "black"

    CASTLING_KINGSIDE  = "o-o"
    CASTLING_QUEENSIDE = "o-o-o"
    ENPASSANT          = "enpassant"
    STARTUP_BOOST      = "startup boost"

    Move = ChessMove
    Piece = ChessPiece
    DefaultMoveLocation = ChessCoordinateNotation

    player_names = ("White", "Black")

    # no handicap supported yet
    handicap = ()

    def setup_players(self):
        super(ChessGame, self).setup_players()
        self.players[0].color = self.COLOR_WHITE
        self.players[1].color = self.COLOR_BLACK

    def _move_for_attack_test(self, source, target):
        m = super(ChessGame, self)._move_for_attack_test(source, target)
        piece = self.board.piece_at(source)
        if  (piece.type is ChessFacetype.PAWN and
             target.row == piece.player.next.homerow):
            # the effective choice does not matter (we're testing for
            # a capture using the pawn), but a lack of promotion would
            # be invalid
            m.promotion = ChessFacetype.QUEEN
        else:
            m.promotion = None
        return m

    def do_move(self, player, move):
        assert isinstance(move, ChessMove)
        super(ChessGame, self).do_move(player, move)

        move.piece_had_moved = move.piece.has_moved
        move.piece.has_moved = True

        # promotion
        if move.promotion is None:
            # put piece at target location
            self.board.put_piece(move.target, move.piece)
            assert self.board.piece_at(move.target) is move.piece
        else:
            if isinstance(move.promotion, self.Piece):
                self.board.put_piece(move.target, move.promotion)
            else:
                move.promotion = self.new_piece_at(move.target, player,
                                                   move.promotion)

        if move.side_effect is not None:
            self.do_move(player, move.side_effect)

    def _undo_capture(self, move):
        move.source.holder.put_piece(move.capture.location, move.capture.piece)

    def _undo_move(self, move):
        # undo side-effects first
        if move.side_effect is not None:
            self._undo_move(move.side_effect)

        # demote promoting move before moving pawn back
        if move.promotion:
            move.target.holder.take_piece(move.target)
            move.target.holder.put_piece(move.target, move.piece)

        move.piece.has_moved = move.piece_had_moved

        super(ChessGame, self)._undo_move(move)

class SANNotation(Core.MoveNotation):
    PieceNotation = EnglishChessNotation
    class LocationNotation(Core.SingleBoardCoordinateNotation,
                           Core.LetterColumnNotation, Core.NumberRowNotation):
        "LocationNotation for ChessGame.SANNotation"

    movename = "chess SAN move"
    SPECIALMOVE_TEXT = {
        ChessGame.CASTLING_KINGSIDE  : "O-O",
        ChessGame.CASTLING_QUEENSIDE : "O-O-O",
        }

    def move_serialization(self, move):
        # special cases
        if move.type in self.SPECIALMOVE_TEXT:
            return self.SPECIALMOVE_TEXT[move.type]
        if move.piece.type is ChessFacetype.PAWN:
            if move.capture is not None:
                s = "%sx%s" % (self.location_notation.colname(move.source),
                               self.location_notation.location_name(move.target))
            else:
                s = self.location_notation.location_name(move.target)
            # promotion
            if move.promotion:
                if isinstance(move.promotion, ChessPiece):
                    promotype = move.promotion.type
                else:
                    promotype = move.promotion
                return s + "=" + self.piecename_bytype(promotype)
            return s

        # FIXME: spec requires emitting a minimized source
        return (self.piece_notation.piece_name(move.piece) + self.location_notation.location_name(move.source)
                + ("x" if move.capture is not None else "")
                + self.location_notation.location_name(move.target))

    regexp = r"([A-Z]?)([a-w]*)(\d*)(x?)([a-w]+)(\d+)((?:=[A-Z])?)(?:[#\+]?)"
    def move_deserialization(self, string, player):
        if string.startswith('O-O'):
            move = self.game.Move()
            if string in ('O-O', 'O-O+'):
                move.type = ChessGame.CASTLING_KINGSIDE
            elif string in ('O-O-O', 'O-O-O+'):
                move.type = ChessGame.CASTLING_QUEENSIDE
            else:
                raise Core.ParseError("Could not parse %r as a move" % (string,))

            self.game.supplement_and_check_move(self.game.whose_turn, move, anticipate=True)
            return move

        source_col = source_row = None
        match = re.match(self.regexp + "$", string)
        if not match:
            raise Core.ParseError("Could not parse %r as a move" % (string,))

        if match.group(1):
            piece_type = self.piece_notation.piece_type(match.group(1))
        else:
            piece_type = ChessFacetype.PAWN

        if match.group(2):
            source_col = self.location_notation.col(match.group(2))
        if match.group(3):
            source_row = self.location_notation.row(match.group(3))

        target = self.game.board.Location(self.game.board,
                                          self.location_notation.col(match.group(5)),
                                          self.location_notation.row(match.group(6)))

        try:
            target_piece = target.holder.piece_at(target)
        except IndexError:
            raise Core.ParseError("Move %r refers to non-existent square" % (string,))
        if match.group(4) and target_piece is None:
            raise Core.ParseError("Move to empty %s cannot be a capture" % (target,))

        if len(match.group(7)) == 0:
            promotes = None
        else:
            promotes = self.piece_notation.piece_type(match.group(7)[1])

        candidates = []
        for piece in self.game.pieces:
            if player and piece.player is not player:
                continue
            if piece.type is not piece_type:
                continue
            loc = self.game.board.piece_location(piece)
            if loc is None:     # not on board, probably captured
                continue
            if source_col is not None and source_col != loc.col:
                continue
            if source_row is not None and source_row != loc.row:
                continue
            if piece_type is ChessFacetype.PAWN:
                try:
                    target_piece = target.holder.piece_at(target)
                except IndexError:
                    raise Core.ParseError("Move %r refers to non-existent square" % (string,))
                if target_piece:
                    if abs(loc.col - target.col) != 1:
                        continue
                else:
                    if loc.col != target.col:
                        continue
            #
            move = self.game.Move(source=loc, target=target)
            move.promotion = promotes
            try:
                self.game.supplement_and_check_move(self.game.whose_turn, move, anticipate=True)
            except InvalidMove as ex:
                logger.debug("%s not suitable for %s: %s", loc, string, ex.message)
                continue
            candidates.append(move)
        if len(candidates) == 0:
            # too detailed ...
            raise Core.ParseError("Found no suitable %s for %s" % (piece_type, string))
        if len(candidates) > 1:
            raise Core.ParseError("Could not disambiguate %s between: %s" %
                                  (string, ' or '.join(str(c.source) for c in candidates)))
        move = candidates[0]

        return move

Core.UndoableGame.register(ChessGame)
