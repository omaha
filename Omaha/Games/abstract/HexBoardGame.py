# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .SingleBoardGame import SingleBoardGame
from Omaha.Holders.HexBoard import HexBoard
from Omaha import Core
from Overlord.misc import classproperty

from string import ascii_lowercase

# FIXME does not fit well in existing framework
class HexBentlinesNotation(Core.ChessLikeCoordinateNotation.LocationNotation):
    '''"Bent-lines" notation for hexagonal boards, not using 'j'.

    Notation making use of as many "bent lines" of matrix_width
    length as possible, and uses shorter lines for the rest'''

    locname = "hexagonal bent-lines chesslike coordinates"

    def location_from_match(self, match):
        col = self.col(match.group(1))
        row = self._bentlines_row_number(match.group(2), col)
        return self.game.board.Location(self.game.board, col, row)

    def colname(self, loc):
        if loc.col >= 9:
            col = loc.col + 1           # 'j' is not used
        else:
            col = loc.col
        return ascii_lowercase[col]
    def rowname(self, loc):
        # base calculation, correct for the right (horizontal) part of
        # the board
        row = self.game.board.matrix_height - loc.row
        if loc.col < (self.game.board.matrix_width - self.game.boardwidth0):
            # slant correction
            row += loc.col - (self.game.board.matrix_height - self.game.boardwidth1)
        return "%s" % row

    def col(self, string):
        if string < "j":
            return ascii_lowercase.index(string)
        else:
            return ascii_lowercase.index(string) - 1
    def _bentlines_row_number(self, string, col):
        row = self.game.board.matrix_height - int(string)
        if col < (self.game.board.matrix_width - self.game.boardwidth0):
            row += col - (self.game.board.matrix_height - self.game.boardwidth1)
        return row

class HexChessLikeBentlinesCoordinateNotation(Core.ChessLikeCoordinateNotation):
    movename = "hexagonal bent-lines chesslike coordinates move"
    LocationNotation = HexBentlinesNotation

class HexBoardGame(SingleBoardGame):
    DefaultMoveLocation = HexChessLikeBentlinesCoordinateNotation

    def __init__(self, **kwargs):
        super(HexBoardGame, self).__init__(**kwargs)
        self.board = None
        self.boardwidth0, self.boardwidth1, self.boardwidth2 = None, None, None

    def create_holders(self):
        self.board = HexBoard(parentcontext=self.context,
                              paramvalues=dict(width0=self.boardwidth0,
                                               width1=self.boardwidth1,
                                               width2=self.boardwidth2))

    @property
    def holders(self):
        """List of holder objects used by this game."""
        assert self.board, "HexBoardGame.holder queried before board construction"
        holders = super(HexBoardGame, self).holders
        holders.append(self.board)
        return holders
    @classproperty
    @classmethod
    def holder_classes(cls):
        holder_classes = super(HexBoardGame, cls).holder_classes
        holder_classes.append(HexBoard)
        return holder_classes

    def setup_players(self):
        super(HexBoardGame, self).setup_players()

    # FIXME: 2-corner-based-players specific
    def set_start_position(self):
        self.players[0].orientation = (0, -1)
        self.players[1].orientation = (0, +1)
        self.players[0].homerow = self.board.matrix_height - 1
        self.players[1].homerow = 0

    # FIXME: 2-corner-based-players specific
    # FIXME: use a transformation matrix instead
    def player_relative_pos(self, player, col, row):
        # adjust row to side
        row = player.homerow + row * player.orientation[1]
        # adjust to board slant
        if col < 0:
            row -= player.orientation[1] * col

        # FIXME: not adapted to irregular boards
        # adjust col to side
        col *= -player.orientation[1]
        # center to corner
        col += self.board.topmost_col

        return (col, row)
