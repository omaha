# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .SingleBoardGame import SingleBoardGame
from Omaha.Holders.RectBoard import RectBoard
from Overlord.misc import classproperty

class RectBoardGame(SingleBoardGame):
    """Abstract base class for 2-player RectBoard-based games."""

    def __init__(self, **kwargs):
        super(RectBoardGame, self).__init__(**kwargs)
        self.board = None
        self.boardwidth, self.boardheight = None, None

    def create_holders(self):
        super(RectBoardGame, self).create_holders()
        self.board = RectBoard(parentcontext=self.context,
                               paramvalues=dict(ncolumns=self.boardwidth,
                                                nrows=self.boardheight))

    @property
    def holders(self):
        """List of holder objects used by this game."""
        assert self.board, "RectBoardGame.holder queried before board construction"
        holders = super(RectBoardGame, self).holders
        holders.append(self.board)
        return holders
    @classproperty
    @classmethod
    def holder_classes(cls):
        holder_classes = super(RectBoardGame, cls).holder_classes
        holder_classes.append(RectBoard)
        return holder_classes

    # FIXME: indeed a 2-player-specific thing
    def setup_players(self):
        super(RectBoardGame, self).setup_players()
        self.players[0].orientation = (0, -1)
        self.players[1].orientation = (0, +1)
        self.players[0].homerow = self.boardheight - 1
        self.players[1].homerow = 0

    def player_relative_row(self, player, row):
        return player.homerow + row * player.orientation[1]

    def player_relative_column(self, player, col):
        if player.orientation[1] == 1:
            return self.boardwidth - 1 - col
        else:
            return col

    def player_relative_pos(self, player, col, row):
        return (self.player_relative_column(player, col),
                self.player_relative_row(player, row))
