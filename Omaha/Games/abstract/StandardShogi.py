# -*- coding: utf-8 -*-

# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .ShogiGame import ShogiGame, WesternShogiPieceNotation
from .ShogiGame import WesternShogiNotation, WesternShogiWithDropsNotation
from .ShogiGame import SANNotation as ShogiSANNotation
from ..piecetypes.shogi import ShogiFacetype
from ..piecetypes.shoshogi import ShoShogiFacetype
from Omaha import Core
import Overlord
from .. import movelib
import logging

logger = logging.getLogger('Omaha.Games.abstract.StandardShogi')

class StandardShogi(ShogiGame):
    """Pieces, start position, and handicap for 9x9 shogi.

    This class willingly does not pull support for drops, to be
    possible to share with variants without drops.
    """

    MOVETYPES = {
        ShogiFacetype.KING: (
            (movelib.default_move, movelib.MOVETYPE_CHESS_KING, None),),
        ShogiFacetype.GOLD: (
            (movelib.default_move, movelib.MOVETYPE_SHOGI_GOLD, None),),
        ShogiFacetype.SILVER: (
            (movelib.default_move, movelib.MOVETYPE_SHOGI_SILVER, None),),
        ShogiFacetype.KNIGHT: (
            (movelib.default_move, movelib.MOVETYPE_SHOGI_KNIGHT, None),),
        ShogiFacetype.LANCE: (
            (movelib.range_free, movelib.MOVETYPE_SHOGI_LANCE, None),),
        ShogiFacetype.BISHOP: (
            (movelib.range_free, movelib.MOVETYPE_CHESS_BISHOP, None),),
        ShogiFacetype.ROOK: (
            (movelib.range_free, movelib.MOVETYPE_CHESS_ROOK, None),),
        ShogiFacetype.PAWN: (
            (movelib.default_move, movelib.MOVETYPE_CHESS_PAWN, None),),

        ShogiFacetype.TOKIN: (
            (movelib.default_move, movelib.MOVETYPE_SHOGI_GOLD, None),),
        ShogiFacetype.NARIGIN: (
            (movelib.default_move, movelib.MOVETYPE_SHOGI_GOLD, None),),
        ShogiFacetype.NARIKEI: (
            (movelib.default_move, movelib.MOVETYPE_SHOGI_GOLD, None),),
        ShogiFacetype.NARIKYO: (
            (movelib.default_move, movelib.MOVETYPE_SHOGI_GOLD, None),),
        ShogiFacetype.DRAGONKING: (
            (movelib.range_free, movelib.MOVETYPE_SHOGI_DRAGONKING, None),),
        ShogiFacetype.DRAGONHORSE: (
            (movelib.range_free, movelib.MOVETYPE_SHOGI_DRAGONHORSE, None),),
    }


    def __init__(self, **kwargs):
        super(StandardShogi, self).__init__(**kwargs)
        self.boardwidth, self.boardheight = 9,9
        self.promotion_rows_count = 3

    @Overlord.parameter
    def handicap(cls, context):
        return Overlord.params.Choice(
            label = 'Handicap',
            alternatives = {
                "0 - None": (),
                "1 - Left lance": (ShogiFacetype.LANCE,),
                "2 - Bishop": (ShogiFacetype.BISHOP,),
                "3 - Rook": (ShogiFacetype.ROOK,),
                "4 - Rook an left lance": (ShogiFacetype.ROOK,
                                           ShogiFacetype.LANCE),
                "5 - Rook and bishop": (ShogiFacetype.ROOK,
                                        ShogiFacetype.BISHOP),
                "6 - Rook, bishop, lances": (ShogiFacetype.ROOK,
                                             ShogiFacetype.BISHOP,
                                             ShogiFacetype.LANCE), # *2
                "7 - Rook, bishop, lances, knights": (
                    ShogiFacetype.ROOK, ShogiFacetype.BISHOP,
                    ShogiFacetype.LANCE, ShogiFacetype.KNIGHT), # *2
                "8 - Rook, bishop, lances, knights, silvers": (
                    ShogiFacetype.ROOK, ShogiFacetype.BISHOP,
                    ShogiFacetype.LANCE, ShogiFacetype.KNIGHT, ShogiFacetype.SILVER), # *2
                },
            default = ())

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""

        for player in self.players:
            if  (player is self.players[0] or
                 ShogiFacetype.LANCE not in self.handicap):
                self.new_piece_on_board(self.player_relative_pos(player, 0, 0),
                                        player, ShogiFacetype.LANCE,
                                        ShogiFacetype.NARIKYO)
            if  (player is self.players[0] or
                 ShogiFacetype.KNIGHT not in self.handicap):
                self.new_piece_on_board(self.player_relative_pos(player, 1, 0),
                                        player, ShogiFacetype.KNIGHT,
                                        ShogiFacetype.NARIKEI)
            if  (player is self.players[0] or
                 ShogiFacetype.SILVER not in self.handicap):
                self.new_piece_on_board(self.player_relative_pos(player, 2, 0),
                                        player, ShogiFacetype.SILVER,
                                        ShogiFacetype.NARIGIN)
            self.new_piece_on_board(self.player_relative_pos(player, 3, 0),
                                    player, ShogiFacetype.GOLD)
            self.new_piece_on_board(self.player_relative_pos(player, 4, 0),
                                    player, ShogiFacetype.KING)
            self.new_piece_on_board(self.player_relative_pos(player, 5, 0),
                                    player, ShogiFacetype.GOLD)
            if  (player is self.players[0] or
                 ShogiFacetype.SILVER not in self.handicap):
                self.new_piece_on_board(self.player_relative_pos(player, 6, 0),
                                        player, ShogiFacetype.SILVER,
                                        ShogiFacetype.NARIGIN)
            if  (player is self.players[0] or
                 ShogiFacetype.KNIGHT not in self.handicap):
                self.new_piece_on_board(self.player_relative_pos(player, 7, 0),
                                        player, ShogiFacetype.KNIGHT,
                                        ShogiFacetype.NARIKEI)
            if  (player is self.players[0] or
                 not(ShogiFacetype.LANCE in self.handicap and
                     ShogiFacetype.BISHOP in self.handicap)):
                self.new_piece_on_board(self.player_relative_pos(player, 8, 0),
                                        player, ShogiFacetype.LANCE,
                                        ShogiFacetype.NARIKYO)

            if  (player is self.players[0] or
                 ShogiFacetype.BISHOP not in self.handicap):
                self.new_piece_on_board(self.player_relative_pos(player, 1, 1),
                                        player, ShogiFacetype.BISHOP,
                                        ShogiFacetype.DRAGONHORSE)
            if  (player is self.players[0] or
                 ShogiFacetype.ROOK not in self.handicap):
                self.new_piece_on_board(self.player_relative_pos(player, 7, 1),
                                        player, ShogiFacetype.ROOK,
                                        ShogiFacetype.DRAGONKING)

            for i in range(0, self.boardwidth):
                self.new_piece_on_board(self.player_relative_pos(player, i, 2),
                                        player, ShogiFacetype.PAWN,
                                        ShogiFacetype.TOKIN)

class EnglishShogiPieceNotation(WesternShogiPieceNotation):
    PIECE_TEXT = {
        ShogiFacetype.KING   : "K",
        ShogiFacetype.GOLD   : "G",
        ShogiFacetype.SILVER : "S",
        ShogiFacetype.KNIGHT : "N",
        ShogiFacetype.LANCE  : "L",
        ShogiFacetype.BISHOP : "B",
        ShogiFacetype.ROOK   : "R",
        ShogiFacetype.PAWN   : "P",

        ShoShogiFacetype.DRUNKENELEPHANT : "DE",
    }

    def piece_name(self, piece, face=None):
        basetext = self.PIECE_TEXT[piece.unpromoted_type]
        if  (face is None and piece.promoted
             or face is self.promoted):
            return "+" + basetext
        elif face in (None, self.unpromoted):
            return basetext
        else:
            raise RuntimeError("unsupported face %r" % (face,))
    def piece_type(self, name):
        for typ, typname in self.PIECE_TEXT.items():
            if typname == name:
                return typ
        else:
            raise LookupError("Invalid piece type '%s'" % name)

class EnglishShogiNotation(WesternShogiNotation):
    PieceNotation = EnglishShogiPieceNotation

class EnglishShogiWithDropsNotation(WesternShogiWithDropsNotation):
    PieceNotation = EnglishShogiPieceNotation

class OneKanjiShogiNotation(Core.PieceNotation):
    PIECE_TEXT = {
        ShogiFacetype.KING   : u"王",
#        ShogiFacetype.JADE   : u"玉",
        ShogiFacetype.GOLD   : u"金",
        ShogiFacetype.SILVER : u"銀",
        ShogiFacetype.KNIGHT : u"桂",
        ShogiFacetype.LANCE  : u"香",
        ShogiFacetype.BISHOP : u"角",
        ShogiFacetype.ROOK   : u"飛",
        ShogiFacetype.PAWN   : u"歩",

        ShogiFacetype.TOKIN       : u"と",
        ShogiFacetype.NARIGIN     : u"全",
        ShogiFacetype.NARIKEI     : u"圭",
        ShogiFacetype.NARIKYO     : u"杏",
        ShogiFacetype.DRAGONKING  : u"龍",
        ShogiFacetype.DRAGONHORSE : u"馬",

        ShoShogiFacetype.DRUNKENELEPHANT : u"酔",
        ShoShogiFacetype.CROWNPRINCE     : u"太",
    }
    def piece_name(self, piece):
        return self.PIECE_TEXT[piece.type]

    def piece_type(self, name):
        for typ, typname in self.PIECE_TEXT.items():
            if typname == name:
                return typ
        else:
            raise LookupError("Invalid piece type '%s'" % name)

class TwoKanjiShogiNotation(Core.PieceNotation):
    PIECE_TEXT = {
        ShogiFacetype.KING   : u"王将",
#        ShogiFacetype.JADE   : u"玉将",
        ShogiFacetype.GOLD   : u"金将",
        ShogiFacetype.SILVER : u"銀将",
        ShogiFacetype.KNIGHT : u"桂馬",
        ShogiFacetype.LANCE  : u"香車",
        ShogiFacetype.BISHOP : u"角行",
        ShogiFacetype.ROOK   : u"飛車",
        ShogiFacetype.PAWN   : u"歩兵",

        ShogiFacetype.TOKIN       : u"と金",
        ShogiFacetype.NARIGIN     : u"成銀",
        ShogiFacetype.NARIKEI     : u"成桂",
        ShogiFacetype.NARIKYO     : u"成香",
        ShogiFacetype.DRAGONKING  : u"龍王",
        ShogiFacetype.DRAGONHORSE : u"龍馬",

        ShoShogiFacetype.DRUNKENELEPHANT : u"酔象",
        ShoShogiFacetype.CROWNPRINCE     : u"太子",
    }

    def piece_name(self, piece):
        # FIXME: vertical layout forced through inserted newlines
        return '\n'.join(self.PIECE_TEXT[piece.type])

class SANNotation(ShogiSANNotation):
    movename = "SAN move for standard Shogi" # and some variants that extend it
    PieceNotation = EnglishShogiPieceNotation
