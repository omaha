# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Holders.Pool import PiecePool
from Overlord.misc import classproperty
from Omaha import Core

class PlayerPools(Core.Game):

    def __init__(self, **kwargs):
        super(PlayerPools, self).__init__(**kwargs)
        # one pool per player
        self.pools = {}

    def clone(self):
        c = super(PlayerPools, self).clone()
        c.pools = {}
        for player, pool in self.pools.items():
            for p in pool.pieces:
                assert p.player is player
            clone_player = c.players[self.players.index(player)]
            assert clone_player.name == player.name
            clone_pool = pool.clone()
            c.pools[clone_player] = clone_pool
            c._update_cloned_pieces(self, clone_pool)
            for p in clone_pool.pieces:
                assert p.player is clone_player, "%s != %s" % (p.player, clone_player)
        return c

    def clone_location(self, loc, clone):
        for player, pool in self.pools.items():
            if pool is loc.holder:
                clone_player = clone.players[self.players.index(player)]
                clone_pool = clone.pools[clone_player]
                return clone_pool.Location(clone_pool, loc.index)

        return super(PlayerPools, self).clone_location(loc, clone)

    def create_holders(self):
        super(PlayerPools, self).create_holders()
        for player in self.players:
            self.pools[player] = PiecePool(parentcontext=self.context)

    @property
    def holders(self):
        assert self.pools, "PlayerPools.holder queried before pools construction"
        holders = super(PlayerPools, self).holders
        holders.extend(self.pools.values())
        return holders
    @classproperty
    @classmethod
    def holder_classes(cls):
        holder_classes = super(PlayerPools, cls).holder_classes
        holder_classes.append(PiecePool)
        return holder_classes
