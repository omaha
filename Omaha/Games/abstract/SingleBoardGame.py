# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

class SingleBoardGame(Core.Game):
    def __init__(self, **kwargs):
        super(SingleBoardGame, self).__init__(**kwargs)
        self.board = None

    def clone(self):
        c = super(SingleBoardGame, self).clone()
        c.board = self.board.clone()
        c._update_cloned_pieces(self, c.board)
        for p in self.pieces:
            assert p.player in self.players, "%s not in %s" % (p.player, self.players)
        return c

    def clone_location(self, loc, clone):
        if loc.holder is self.board:
            return clone.board.Location(clone.board, loc.col, loc.row)
        return super(SingleBoardGame, self).clone_location(loc, clone)

    def new_piece_on_board(self, coords, *piece_args):
        """Convenience method to create a new piece on self.board.

        This is only meaningful for single-board games."""
        self.new_piece_at(self.board.Location(self.board, *coords),
                          *piece_args)

    def dump_board(self):
        "Debug helper: dump the state of the board to stdout."
        for row in range(self.board.nrows):
            for column in range(self.board.ncolumns):
                piece = self.board.piece_at(self.board.Location(self.board, column, row))
                print self.default_notation.piece_notation.piece_name(piece) if piece else ".",
            print
