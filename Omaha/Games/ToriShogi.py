# -*- coding: utf-8 -*-

# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.GenericChesslikeMoves import PreciousKings
from .abstract.ShogiGame import WesternShogiPieceNotation
from .abstract.ShogiGame import WesternShogiWithDropsNotation
from .abstract.ShogiGame import ShogiGameWithDrops
from .piecetypes.torishogi import ToriShogiFacetype
from Omaha import Core
from . import movelib
import logging

logger = logging.getLogger('Omaha.Games.ToriShogi')

MOVETYPE_PHEASANT = (((0, 2), 1), ((-1, -1), 1), ((1, -1), 1))
MOVETYPE_LQUAIL   = (((0, 1), movelib.MOVERANGE_INF),
                     ((-1, -1), 1), ((1, -1), movelib.MOVERANGE_INF))
MOVETYPE_RQUAIL   = (((0, 1), movelib.MOVERANGE_INF),
                     ((1, -1), 1), ((-1, -1), movelib.MOVERANGE_INF))
MOVETYPE_EAGLE    = (((-1, 1), movelib.MOVERANGE_INF),
                     ((1, 1), movelib.MOVERANGE_INF),
                     ((0, -1), movelib.MOVERANGE_INF),
                     ((0, 1), 1), ((-1, 0), 1), ((1, 0), 1),
                     ((-1, -1), 2), ((1, -1), 2))
MOVETYPE_GOOSE    = (((-2, 2), 1), ((2, 2), 1), ((0, -2), 1))

class EnglishPieceNotation(WesternShogiPieceNotation):
    PIECE_TEXT = {
        ToriShogiFacetype.PHOENIX  : "Ph",
        ToriShogiFacetype.CRANE    : "Cr",
        ToriShogiFacetype.PHEASANT : "Pt",
        ToriShogiFacetype.RQUAIL   : "RQ",
        ToriShogiFacetype.LQUAIL   : "LQ",
        ToriShogiFacetype.FALCON   : "Fa",
        ToriShogiFacetype.SWALLOW  : "Sw",
    }

    def piece_name(self, piece, face=None):
        basetext = self.PIECE_TEXT[piece.unpromoted_type]
        if  (face is None and piece.promoted
             or face is self.promoted):
            return "+" + basetext
        elif face in (None, self.unpromoted):
            return basetext
        else:
            raise RuntimeError("unsupported face %r" % (face,))
    def piece_type(self, name):
        for typ, typname in self.PIECE_TEXT.items():
            if typname == name:
                return typ
        else:
            raise LookupError("Invalid piece type '%s'" % name)

class EnglishToriNotation(WesternShogiWithDropsNotation):
    PieceNotation = EnglishPieceNotation

class ToriShogi(ShogiGameWithDrops, PreciousKings):
    """Pieces, start position, and handicap for Tori Shogi.
    """

    MOVETYPES = {
        ToriShogiFacetype.PHOENIX: (
            (movelib.default_move, movelib.MOVETYPE_CHESS_KING, None),),

        ToriShogiFacetype.FALCON: (
            (movelib.default_move, movelib.MOVETYPE_CHU_DRUNKEN, None),),
        ToriShogiFacetype.CRANE: (
            (movelib.default_move, movelib.MOVETYPE_CHU_LEOPARD, None),),
        ToriShogiFacetype.PHEASANT: (
            (movelib.default_move, MOVETYPE_PHEASANT, None),),
        ToriShogiFacetype.LQUAIL: (
            (movelib.range_free, MOVETYPE_LQUAIL, None),),
        ToriShogiFacetype.RQUAIL: (
            (movelib.range_free, MOVETYPE_RQUAIL, None),),
        ToriShogiFacetype.SWALLOW: (
            (movelib.default_move, movelib.MOVETYPE_CHESS_PAWN, None),),

        ToriShogiFacetype.EAGLE:(
            (movelib.range_free, MOVETYPE_EAGLE, None),),
        ToriShogiFacetype.GOOSE:(
            (movelib.default_move, MOVETYPE_GOOSE, None),),
    }

    DefaultMoveLocation = EnglishToriNotation

    def __init__(self, **kwargs):
        super(ToriShogi, self).__init__(**kwargs)
        self.boardwidth, self.boardheight = 7, 7
        self.promotion_rows_count = 2

    def has_king_status(self, piece):
        return piece.type is ToriShogiFacetype.PHOENIX

    # no handicap supported yet
    handicap = ()

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""

        for player in self.players:
            self.new_piece_on_board(self.player_relative_pos(player, 0, 0),
                                    player, ToriShogiFacetype.LQUAIL)
            self.new_piece_on_board(self.player_relative_pos(player, 1, 0),
                                    player, ToriShogiFacetype.PHEASANT)
            self.new_piece_on_board(self.player_relative_pos(player, 2, 0),
                                    player, ToriShogiFacetype.CRANE)
            self.new_piece_on_board(self.player_relative_pos(player, 3, 0),
                                    player, ToriShogiFacetype.PHOENIX)
            self.new_piece_on_board(self.player_relative_pos(player, 4, 0),
                                    player, ToriShogiFacetype.CRANE)
            self.new_piece_on_board(self.player_relative_pos(player, 5, 0),
                                    player, ToriShogiFacetype.PHEASANT)
            self.new_piece_on_board(self.player_relative_pos(player, 6, 0),
                                    player, ToriShogiFacetype.LQUAIL)

            self.new_piece_on_board(self.player_relative_pos(player, 3, 1),
                                    player, ToriShogiFacetype.FALCON,
                                    ToriShogiFacetype.EAGLE)

            for i in range(0, self.boardwidth):
                self.new_piece_on_board(self.player_relative_pos(player, i, 2),
                                        player, ToriShogiFacetype.SWALLOW,
                                        ToriShogiFacetype.GOOSE)
            self.new_piece_on_board(self.player_relative_pos(player, 4, 3),
                                    player, ToriShogiFacetype.SWALLOW,
                                    ToriShogiFacetype.GOOSE)

class OneKanjiNotation(Core.PieceNotation):
    PIECE_TEXT = {
        ToriShogiFacetype.PHOENIX  : u"鵬",
        ToriShogiFacetype.CRANE    : u"鶴",
        ToriShogiFacetype.PHEASANT : u"雉",
        ToriShogiFacetype.RQUAIL   : u"鶉", # FIXME
        ToriShogiFacetype.LQUAIL   : u"鶉", # FIXME
        ToriShogiFacetype.FALCON   : u"雉",
        ToriShogiFacetype.SWALLOW  : u"燕",
        
        ToriShogiFacetype.EAGLE    : u"鵰",
        ToriShogiFacetype.GOOSE    : u"鴈",
    }
    def piece_name(self, piece):
        return self.PIECE_TEXT[piece.type]

    def piece_type(self, name):
        for typ, typname in self.PIECE_TEXT.items():
            if typname == name:
                return typ
        else:
            raise LookupError("Invalid piece type '%s'" % name)
