# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.ChessGame import ChessGame, ChessCoordinateNotation
from .abstract.GenericChesslikeMoves import PreciousKings
from .abstract.HexBoardGame import HexBoardGame, HexChessLikeBentlinesCoordinateNotation
from .piecetypes.chess import ChessFacetype
from . import movelib

class HexChessBentlinesCoordinateNotation(HexChessLikeBentlinesCoordinateNotation,
                                          ChessCoordinateNotation):
    pass

class GlinskiHexChess(HexBoardGame, ChessGame, PreciousKings):
    MOVETYPES = {
        ChessFacetype.KING: (
            (movelib.default_move, movelib.MOVETYPE_HEXCHESS_KING, None),),
        ChessFacetype.QUEEN: (
            (movelib.range_free, movelib.MOVETYPE_HEXCHESS_QUEEN, None),),
        ChessFacetype.BISHOP: (
            (movelib.range_free, movelib.MOVETYPE_HEXCHESS_BISHOP, None),),
        ChessFacetype.ROOK: (
            (movelib.range_free, movelib.MOVETYPE_HEXCHESS_ROOK, None),),
        ChessFacetype.KNIGHT: (
            (movelib.default_move, movelib.MOVETYPE_HEXCHESS_KNIGHT, None),),
        ChessFacetype.PAWN: (
            (movelib.replace_capture,
             movelib.MOVETYPE_GLINSKICHESS_PAWNCAPTURE, None),
            # FIXME: BOOST does not express need of not being blocked
            (movelib.And(movelib.never_moved, movelib.unoccupied_target,
                         movelib.range_free),
             (((0, 1), 2),), ChessGame.STARTUP_BOOST),
            (movelib.unoccupied_target, movelib.MOVETYPE_CHESS_PAWN, None))
    }

    DefaultMoveLocation = HexChessBentlinesCoordinateNotation

    def __init__(self, **kwargs):
        super(GlinskiHexChess, self).__init__(**kwargs)
        self.boardwidth0, self.boardwidth1, self.boardwidth2 = 6, 6, 6

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""
        super(GlinskiHexChess, self).set_start_position()

        # first, symetrically-placed pieces
        for player in self.players:
            for i in range(5):
                self.new_piece_on_board(self.player_relative_pos(player,
                                                                 i - 4, i),
                                        player, ChessFacetype.PAWN)
            self.new_piece_on_board(self.player_relative_pos(player, -3, 0),
                                    player, ChessFacetype.ROOK)
            self.new_piece_on_board(self.player_relative_pos(player, -2, 0),
                                    player, ChessFacetype.KNIGHT)
            for i in range(3):
                self.new_piece_on_board(self.player_relative_pos(player, 0, i),
                                        player, ChessFacetype.BISHOP)
            self.new_piece_on_board(self.player_relative_pos(player, 2, 0),
                                    player, ChessFacetype.KNIGHT)
            self.new_piece_on_board(self.player_relative_pos(player, 3, 0),
                                    player, ChessFacetype.ROOK)
            for i in range(4):
                self.new_piece_on_board(self.player_relative_pos(player,
                                                                 4 - i, i),
                                        player, ChessFacetype.PAWN)

        # then, king and queen facing their counterparts
        player = self.players[0]
        self.new_piece_on_board(self.player_relative_pos(player, 1, 0),
                                player, ChessFacetype.KING)
        self.new_piece_on_board(self.player_relative_pos(player, -1, 0),
                                player, ChessFacetype.QUEEN)
        player = self.players[1]
        self.new_piece_on_board(self.player_relative_pos(player, -1, 0),
                                player, ChessFacetype.KING)
        self.new_piece_on_board(self.player_relative_pos(player, 1, 0),
                                player, ChessFacetype.QUEEN)

    def supplement_and_check_move(self, player, move, anticipate):
        super(GlinskiHexChess, self).supplement_and_check_move(player, move, anticipate)
        # FIXME: only allow standard moves, no side-effects supported
        self.check_standard_moves(player, move, anticipate)

    def has_king_status(self, piece):
        return piece.type is ChessFacetype.KING
