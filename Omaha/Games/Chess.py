# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.ChessGame import ChessGame
from .abstract.GenericChesslikeMoves import PreciousKings
from .abstract.RectBoardGame import RectBoardGame
from Omaha.Core import InvalidMove, IncompleteMove
from .piecetypes.chess import ChessFacetype
from . import movelib
import Overlord

import logging
logger = logging.getLogger('Omaha.Games.Chess')

class Chess(RectBoardGame, ChessGame, PreciousKings):
    MOVETYPES = {
        ChessFacetype.KING: (
            (movelib.default_move, movelib.MOVETYPE_CHESS_KING, None),),
        ChessFacetype.QUEEN: (
            (movelib.range_free, movelib.MOVETYPE_CHESS_QUEEN, None),),
        ChessFacetype.BISHOP: (
            (movelib.range_free, movelib.MOVETYPE_CHESS_BISHOP, None),),
        ChessFacetype.ROOK: (
            (movelib.range_free, movelib.MOVETYPE_CHESS_ROOK, None),),
        ChessFacetype.KNIGHT: (
            (movelib.default_move, movelib.MOVETYPE_CHESS_KNIGHT, None),),
        ChessFacetype.PAWN: (
            (movelib.replace_capture, movelib.MOVETYPE_CHESS_PAWNCAPTURE, None),
            (movelib.And(movelib.never_moved, movelib.unoccupied_target,
                         movelib.range_free),
             (((0, 1), (2, 2)),), ChessGame.STARTUP_BOOST),
            (movelib.unoccupied_target, movelib.MOVETYPE_CHESS_PAWN, None))
    }

    def __init__(self, **kwargs):
        super(Chess, self).__init__(**kwargs)
        self.boardwidth, self.boardheight = 8, 8

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""
        for player in self.players:
            self.new_piece_on_board(self.player_relative_pos(player, 0, 0),
                                    player, ChessFacetype.ROOK)
            self.new_piece_on_board(self.player_relative_pos(player, 1, 0),
                                    player, ChessFacetype.KNIGHT)
            self.new_piece_on_board(self.player_relative_pos(player, 2, 0),
                                    player, ChessFacetype.BISHOP)
            self.new_piece_on_board((3, self.player_relative_row(player, 0)),
                                    player, ChessFacetype.QUEEN)
            self.new_piece_on_board((4, self.player_relative_row(player, 0)),
                                    player, ChessFacetype.KING)
            self.new_piece_on_board(self.player_relative_pos(player, 5, 0),
                                    player, ChessFacetype.BISHOP)
            self.new_piece_on_board(self.player_relative_pos(player, 6, 0),
                                    player, ChessFacetype.KNIGHT)
            self.new_piece_on_board(self.player_relative_pos(player, 7, 0),
                                    player, ChessFacetype.ROOK)

            for i in range(0, self.boardwidth):
                self.new_piece_on_board(
                    self.player_relative_pos(player, i, 1),
                    player, ChessFacetype.PAWN)

    def has_king_status(self, piece):
        return piece.type is ChessFacetype.KING

    def supplement_and_check_move(self, player, move, anticipate):

        # preprocess explicit castling
        if move.type in (ChessGame.CASTLING_KINGSIDE,
                         ChessGame.CASTLING_QUEENSIDE):
            rookdelta = {ChessGame.CASTLING_KINGSIDE: 3,
                         ChessGame.CASTLING_QUEENSIDE: 4}[move.type]
            rooks = []
            for piece in self.pieces:
                if piece.player is not player:
                    continue
                if piece.type is ChessFacetype.KING:
                    if piece.has_moved:
                        raise InvalidMove("cannot castle king after moving")
                    move.piece = piece
                    move.set_source(self.board.piece_location(piece))
                elif (piece.type is ChessFacetype.ROOK and
                      not piece.has_moved):
                    rooks.append(piece)
            for rookloc in [self.board.piece_location(rook) for rook in rooks]:
                delta = rookloc - move.source
                if abs(delta.dcol) == rookdelta:
                    # found correct rook: move 2 steps in its direction
                    delta = delta.normalized
                    move.set_target(move.source + delta + delta)
                    break
            else:
                raise InvalidMove("no rook for castling")

        super(Chess, self).supplement_and_check_move(player, move, anticipate)

        try:
            ## check for special moves
            # en passant
            if   (move.piece.type is ChessFacetype.PAWN and
                  move.target.row - move.source.row == player.orientation[1] and
                  abs(move.target.col - move.source.col) == 1 and
                  move.capture is None and
                  self.moves.nmoves_done > 0):
                captured_pos = self.board.Location(
                    self.board, move.target.col, move.source.row)
                captured_piece = move.target.holder.piece_at(captured_pos)
                prev_move = self.moves.last
                if  (captured_piece is None or
                     captured_piece.type is not ChessFacetype.PAWN or
                     captured_piece.player is player or
                     prev_move.target != captured_pos or
                     prev_move.type is not self.STARTUP_BOOST):
                    raise InvalidMove("illegal attempt at en-passant")
                move.type = self.ENPASSANT
                move.capture = move.Capture(captured_piece)
                move.capture.location = captured_pos

            # castling
            elif (move.piece.type is ChessFacetype.KING and
                  not move.piece.has_moved and
                  move.target.row == move.source.row and
                  abs(move.target.col - move.source.col) == 2):
                # FIXME: check that no square crossed by king are under attack
                move_direction = (move.target - move.source).normalized

                if self.board.piece_at(move.source + move_direction):
                    raise InvalidMove("another piece blocks castling")
                maybe_rook_pos = move.target + move_direction
                maybe_rook = self.board.piece_at(maybe_rook_pos)
                if  (maybe_rook is not None and
                     maybe_rook.type is ChessFacetype.ROOK and
                     not maybe_rook.has_moved):
                    move.type = self.CASTLING_KINGSIDE
                    move.side_effect = self.Move(
                        source=maybe_rook_pos,
                        target=move.source + move_direction)
                else:
                    if self.board.piece_at(move.target + move_direction):
                        raise InvalidMove("another piece blocks castling")
                    maybe_rook_pos = (move.target + move_direction +
                                      move_direction)
                    maybe_rook = self.board.piece_at(maybe_rook_pos)
                    if  (maybe_rook is not None and
                         maybe_rook.type is ChessFacetype.ROOK and
                         not maybe_rook.has_moved):
                        move.type = self.CASTLING_QUEENSIDE
                        move.side_effect = self.Move(
                            source=maybe_rook_pos,
                            target=move.source + move_direction)
                    else:
                        raise InvalidMove("no suitable rook for castling")

            # generic move-checking
            else:
                self.check_standard_moves(player, move, anticipate)

        except InvalidMove:
            raise
        else:
            ## after move-validation, detect event-triggering moves

            # handle pawn promotion
            if  (move.piece.type is ChessFacetype.PAWN and
                 move.target.row == player.next.homerow):
                if move.promotion is None:
                    raise IncompleteMove({
                        'promotion': Overlord.params.Choice(
                            label = "Promote to",
                            alternatives = dict(queen = ChessFacetype.QUEEN,
                                                rook = ChessFacetype.ROOK,
                                                bishop = ChessFacetype.BISHOP,
                                                knight = ChessFacetype.KNIGHT),
                            default = ChessFacetype.QUEEN) })
                else:
                    if move.promotion not in (
                            ChessFacetype.QUEEN, ChessFacetype.KNIGHT,
                            ChessFacetype.ROOK, ChessFacetype.BISHOP ):
                        raise InvalidMove("pawn cannot promote to %s" %
                                          move.promotion)
            elif move.promotion is not None:
                raise InvalidMove("illegal promotion request %r" % (move.promotion,))

    def save(self, filename):
        assert filename.endswith(".pgn")
        from Omaha.GameIO.PGNWriter import PGNGameWriter
        writer = PGNGameWriter(self)
        with open(filename, "w+") as f:
            writer.write_to(f)
