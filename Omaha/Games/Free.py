# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.SingleBoardGame import SingleBoardGame
from Omaha import Core
import Overlord

class FreeGame(SingleBoardGame):
    "Free-move game (without pieces ;)"

    player_names = ("Dummy", )

    @Overlord.parameter
    def boardclass(cls, context):
        return Overlord.params.Plugin(
            label = 'Board type',
            plugintype = 'X-Omaha-Holder',
            context=context,
            pattern = Overlord.plugin_pattern.Any
            )

    def set_params(self, params):
        super(FreeGame, self).set_params(params)

        # propagate boardclass change
        param = type(self).boardclass.fget_param(self)
        if param.value is None:
            raise Overlord.NoSuchPlugin("No Board for %s" %
                                        type(self.game))

        self.board = param.value.Class(parentcontext=self.context)
        if param.subparams and param.value.name in param.subparams:
            self.board.set_params(
                param.subparams[param.value.name])

    @property
    def holders(self):
        """List of holder objects used by this game."""
        holders = super(FreeGame, self).holders
        holders.append(self.board)
        return holders

    # stubs for unneeded functionality

    Move = Core.Move

    def set_start_position(self):
        pass
