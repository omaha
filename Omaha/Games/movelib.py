# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""A library of common moves from various games."""

from .abstract.GenericChesslikeMoves import MOVERANGE_INF

# Library of predicates for conditional moves
def default_move(move, direction, nunits):
    return True
def replace_capture(move, direction, nunits):
    return move.target.holder.piece_at(move.target) is not None
def unoccupied_target(move, direction, nunits):
    return move.target.holder.piece_at(move.target) is None
def never_moved(move, direction, nunits):
    return not move.piece.has_moved

def range_free(move, direction, nunits):
    if nunits is None:
        nunits = move.source.holder.nunits_in_direction(
            move, direction, move.piece.player.orientation)
    delta = move.source.holder.Location.Delta(*direction). \
            player_oriented(move.piece.player.orientation)
    location = move.source
    for increment in range(1, nunits):
        location += delta
        if location.holder.piece_at(location):
            return False
    else:
        return True

def And(*predicates):
    def pred_conjunction(move, direction, nunits):
        for pred in predicates:
            if not pred(move, direction, nunits):
                return False
        else:
            return True
    return pred_conjunction

# Library of moves
MOVETYPE_CHESS_KING = (((-1, 1), 1), ((0, 1), 1), ((1, 1), 1),
                       ((-1, 0), 1), ((1, 0), 1),
                       ((-1, -1), 1), ((0, -1), 1), ((1, -1), 1))
MOVETYPE_CHESS_QUEEN = (((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
                        ((-1, 0), MOVERANGE_INF), ((1, 0), MOVERANGE_INF),
                        ((-1, 1), MOVERANGE_INF), ((1, 1), MOVERANGE_INF),
                        ((-1, -1), MOVERANGE_INF), ((1, -1), MOVERANGE_INF))
MOVETYPE_CHESS_BISHOP = (((-1, 1), MOVERANGE_INF), ((1, 1), MOVERANGE_INF),
                         ((-1, -1), MOVERANGE_INF), ((1, -1), MOVERANGE_INF))
MOVETYPE_CHESS_ROOK = (((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
                       ((-1, 0), MOVERANGE_INF), ((1, 0), MOVERANGE_INF))
MOVETYPE_CHESS_KNIGHT = (((-1, 2), 1), ((1, 2), 1),
                         ((-2, 1), 1), ((2, 1), 1),
                         ((-2, -1), 1), ((2, -1), 1),
                         ((-1, -2), 1), ((1, -2), 1))
MOVETYPE_CHESS_PAWN = (((0, 1), 1),)
MOVETYPE_CHESS_PAWNCAPTURE = (((-1, 1), 1), ((1, 1), 1))

MOVETYPE_HEXCHESS_KING = (((-1, 0), 1), ((1, 0), 1),
                          ((0, 1), 1), ((0, -1), 1),
                          ((-1, 1), 1), ((1, -1), 1),
                          # ^^ 6 orthogonals
                          # vv 6 diagonals
                          ((-1, 2), 1), ((1, 1), 1),
                          ((2, -1), 1), ((1, -2), 1),
                          ((-1, -1), 1), ((-2, 1), 1),
                         )
MOVETYPE_HEXCHESS_QUEEN = (((-1, 0), MOVERANGE_INF), ((1, 0), MOVERANGE_INF),
                           ((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
                           ((-1, 1), MOVERANGE_INF), ((1, -1), MOVERANGE_INF),
                           # ^^ 6 orthogonals
                           # vv 6 diagonals
                           ((-1, 2), MOVERANGE_INF), ((1, 1), MOVERANGE_INF),
                           ((2, -1), MOVERANGE_INF), ((1, -2), MOVERANGE_INF),
                           ((-1, -1), MOVERANGE_INF), ((-2, 1), MOVERANGE_INF),
                          )
MOVETYPE_HEXCHESS_BISHOP = (((-1, 2), MOVERANGE_INF), ((1, 1), MOVERANGE_INF),
                            ((2, -1), MOVERANGE_INF), ((1, -2), MOVERANGE_INF),
                            ((-1, -1), MOVERANGE_INF), ((-2, 1), MOVERANGE_INF))
MOVETYPE_HEXCHESS_ROOK = (((-1, 0), MOVERANGE_INF), ((1, 0), MOVERANGE_INF),
                          ((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
                          ((-1, 1), MOVERANGE_INF), ((1, -1), MOVERANGE_INF))
MOVETYPE_HEXCHESS_KNIGHT = (((-1, 3), 1), ((1, 2), 1),
                            ((2, 1), 1), ((3, -1), 1),
                            ((3, -2), 1), ((2, -3), 1),
                            ((1, -3), 1), ((-1, -2), 1),
                            ((-2, -1), 1), ((-3, 1), 1),
                            ((-3, 2), 1), ((-2, 3), 1),
                           )

MOVETYPE_GLINSKICHESS_PAWNCAPTURE = (((-1, 1), 1), ((1, 0), 1))

MOVETYPE_SHOGI_GOLD = (((-1, 1), 1), ((0, 1), 1), ((1, 1), 1),
                       ((-1, 0), 1), ((1, 0), 1),
                       ((0, -1), 1))
MOVETYPE_SHOGI_SILVER = (((-1, 1), 1), ((0, 1), 1), ((1, 1), 1),
                         ((-1, -1), 1), ((1, -1), 1))
MOVETYPE_SHOGI_KNIGHT = (((-1, 2), 1), ((1, 2), 1))
MOVETYPE_SHOGI_LANCE = (((0, 1), MOVERANGE_INF),)
MOVETYPE_SHOGI_DRAGONKING = (
    ((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
    ((-1, 0), MOVERANGE_INF), ((1, 0), MOVERANGE_INF),
    ((-1, 1), 1), ((1, 1), 1),
    ((-1, -1), 1), ((1, -1), 1))
MOVETYPE_SHOGI_DRAGONHORSE = (
    ((0, 1), 1), ((0, -1), 1),
    ((-1, 0), 1), ((1, 0), 1),
    ((-1, 1), MOVERANGE_INF), ((1, 1), MOVERANGE_INF),
    ((-1, -1), MOVERANGE_INF), ((1, -1), MOVERANGE_INF))

MOVETYPE_CHU_DRUNKEN = (((-1, 1), 1), ((0, 1), 1), ((1, 1), 1),
                        ((-1, 0), 1),              ((1, 0), 1),
                        ((-1, -1), 1),             ((1, -1), 1))
MOVETYPE_CHU_LEOPARD = (((-1, 1), 1),  ((0, 1), 1),  ((1, 1), 1),
                        ((-1, -1), 1), ((0, -1), 1), ((1, -1), 1))
