# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.SingleBoardAndPools import SingleBoardAndPools
from .AsciiChess import AsciiChessRenderer
from ..Games.MiniShogi import MiniShogi

class AsciiShogiRenderer(SingleBoardAndPools, AsciiChessRenderer):
    MARGIN = 20 # percent

    def __init__(self, **kwargs):
        super(AsciiShogiRenderer, self).__init__(**kwargs)
        # FIXME: dependant on square-size
        self.pool_height = 20

    def _draw_piece(self, piece, drawing_context):
        if piece.player.color is MiniShogi.SIDE_GOTE:
            bold = False
        else:
            bold = True

        if piece.promoted:
            color = "red"
        else:
            color = None

        xcenter, ycenter = self.holder_renderers[piece.holder]. \
                           tile_center(piece.location)
        xcenter, ycenter = int(xcenter), int(ycenter)
        self.gui.tk.draw_centered_text(
            drawing_context,
            xcenter, ycenter, self.game.default_notation.piece_notation.piece_name(piece),
            color=color, bold=bold)

        # draw piece outline
        half_square_size = int(
            self.holder_renderers[self.game.board].delta_x / 2 *
            (100 - self.MARGIN)) // 100
        slant_offset = half_square_size * 3 // 10
        self.gui.tk.draw_polygon(
            drawing_context,
            (   # bottom left
                (xcenter - half_square_size,
                 ycenter - piece.player.orientation[1] * half_square_size),
                # top left
                (xcenter - (half_square_size - slant_offset),
                 ycenter + piece.player.orientation[1] * (half_square_size -
                                                          slant_offset)),
                # top wedge
                (xcenter,
                 ycenter + piece.player.orientation[1] * half_square_size),
                # top right
                (xcenter + (half_square_size - slant_offset),
                 ycenter + piece.player.orientation[1] * (half_square_size -
                                                          slant_offset)),
                # bottom right
                (xcenter + half_square_size,
                 ycenter - piece.player.orientation[1] * half_square_size),
                ),
            color=self.gui.tk.color(self.gui.canvas, "black"))
