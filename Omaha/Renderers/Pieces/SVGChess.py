# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.StackedVector import StackedVectorPieceRenderer, svgLayer
from Omaha.Games.Chess import Chess
from Omaha.Games.piecetypes.chess import ChessFacetype

class SVGChessPieceRenderer(StackedVectorPieceRenderer):

    @staticmethod
    def _piece_key(piece):
        return (piece.player.color, piece.type)

    filename_start = { Chess.COLOR_WHITE: "w",
                       Chess.COLOR_BLACK: "b" }
    filename_end = { ChessFacetype.KING: "k",
                     ChessFacetype.QUEEN: "q",
                     ChessFacetype.BISHOP: "b",
                     ChessFacetype.KNIGHT: "n",
                     ChessFacetype.ROOK: "r",
                     ChessFacetype.PAWN: "p" }

    # skin interface definition for Chess
    def skin_schema(self, piece_key):
        piece_color, piece_type = piece_key
        return ( (svgLayer, (0, 0, 100, 100,
                             self.filename_start[piece_color] +
                             self.filename_end[piece_type])) ,)
