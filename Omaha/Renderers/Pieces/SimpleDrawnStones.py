# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord

# FIXME: find better
from Omaha.Games.Chess import Chess
from Omaha.Games.Checkers import Checkers

class SimpleDrawnStones(Core.PieceRenderer):
    def __init__(self, **kwargs):
        super(SimpleDrawnStones, self).__init__(**kwargs)
        self.radius = {}

    @Overlord.parameter
    def black_color(cls, context):
        return Overlord.params.String(label = 'Black stones color',
                                      default = "black")
    @Overlord.parameter
    def white_color(cls, context):
        return Overlord.params.String(label = 'White stones color',
                                      default = "white")

    @black_color.xformer
    def black_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)
    @white_color.xformer
    def white_color(self,value):
        return self.gui.tk.color(self.gui.canvas, value)

    def set_piece_size(self, set_id, newsize, angle=0):
        assert angle == 0
        self.radius[set_id] = int(newsize // 2)

    def draw_image(self, drawing_context, center, set_id, key):
        center_x, center_y = center
        # key is the piece object
        color, typ = key.player.color, key.type
        if typ is Checkers.PIECETYPE_KING:
            offset = self.radius[set_id] // 4
        if color is Chess.COLOR_WHITE:
            self.gui.tk.fill_circle(drawing_context, center_x, center_y,
                                    self.radius[set_id], color=self.white_color)
            self.gui.tk.draw_circle(drawing_context, center_x, center_y,
                                    self.radius[set_id], color=self.black_color)
            if typ is Checkers.PIECETYPE_KING:
                self.gui.tk.draw_circle(drawing_context, center_x, center_y,
                                        self.radius[set_id] - offset, color=self.black_color)

        else:
            self.gui.tk.fill_circle(drawing_context, center_x, center_y,
                                    self.radius[set_id], color=self.black_color)
            if typ is Checkers.PIECETYPE_KING:
                self.gui.tk.draw_circle(drawing_context, center_x, center_y,
                                        self.radius[set_id] - offset, color=self.white_color)
