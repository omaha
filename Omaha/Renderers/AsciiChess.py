# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.Fullscreen import FullscreenGameRenderer
from ..Games.Chess import Chess
from Omaha import Core
import Overlord
from Overlord import plugin_pattern

# TODO: support incremental updates

# FIXME: font size should depend on canvas size

class AsciiChessRenderer(FullscreenGameRenderer):

    def __init__(self, **kwargs):
        super(AsciiChessRenderer, self).__init__(**kwargs)
        self.__notation = None

    def _draw_piece(self, piece, drawing_context):
        xcenter, ycenter = self.holder_renderers[piece.holder]. \
            tile_center(piece.location)

        if piece.player.color is Chess.COLOR_BLACK:
            bold = True
        else:
            bold = False

        self.gui.tk.draw_centered_text(
            drawing_context,
            xcenter, ycenter, self.notation.piece_notation.piece_name(piece), bold=bold)

    # FIXME: the following is duplicated from
    # NotationBasedPieceRenderer because we're not a using a
    # PieceGameRenderer

    @Overlord.parameter
    def notation_class(cls, context):
        game_class = context[Core.Game]
        return Overlord.params.Plugin(
            label = 'Character set',
            plugintype = 'X-Omaha-PieceNotation',
            context = context,
            pattern = plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_class),
            #default = # FIXME: by luck, selects kanji
            )

    @property
    def notation(self):
        notationcls = self.notation_class.Class
        # (re)instanciate notation if required
        if not isinstance(self.__notation, notationcls):
            self.__notation = notationcls(self.gui.game)
        return self.__notation
