# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.RectBoard import RectBoard

class GoBan(RectBoard):

    def draw(self, drawing_context, rectangle):
        """Draw board within specified rectangle of self.gui."""

        super(GoBan, self).draw(drawing_context, rectangle)

        black = self.gui.tk.color(self.gui.canvas, "black")
        for i in range(0, self.holder.matrix_width):
            x = self.tile_center_x(i)
            self.gui.tk.draw_line(
                drawing_context,
                x, self.tile_center_y(0),
                x, self.tile_center_y(self.holder.matrix_height - 1),
                color=black, width=1)
        for i in range(0, self.holder.matrix_height):
            y = self.tile_center_y(i)
            self.gui.tk.draw_line(
                drawing_context,
                self.tile_center_x(0), y,
                self.tile_center_x(self.holder.matrix_width - 1), y,
                color=black, width=1)

        for hoshi in self.gui.game.hoshi_iter():
            self.gui.tk.fill_circle(drawing_context,
                                    self.tile_center_x(hoshi.col),
                                    self.tile_center_y(hoshi.row),
                                    int(self.delta_x/5), color=black)
