# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.HexBoard import HexBoardRenderer, HorizHexBoard, VertHexBoard
import Overlord

# FIXME: first hex corners are out of rectangle
# FIXME: neighbour-hex edges are not identical

class CheckeredHexBoard(HexBoardRenderer):
    @Overlord.parameter
    def black_color(cls, context):
        return Overlord.params.String(label = 'Dark-square color',
                                      default = "#555555")
    @Overlord.parameter
    def white_color(cls, context):
        return Overlord.params.String(label = 'Light-square color',
                                      default = "white")
    @Overlord.parameter
    def grey_color(cls, context):
        return Overlord.params.String(label = 'Grey-square color',
                                      default = "grey")

    @black_color.xformer
    def black_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)
    @white_color.xformer
    def white_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)
    @grey_color.xformer
    def grey_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)

class VertCheckeredHexBoard(CheckeredHexBoard, VertHexBoard):
    def draw(self, drawing_context, rectangle):
        """Draw board within specified rectangle of self.gui."""

        super(VertCheckeredHexBoard, self).draw(drawing_context, rectangle)

        for loc in self.holder.all_locations:
            x, y = self.tile_center(loc)
            color = (self.white_color, self.grey_color, self.black_color)[(loc.col + loc.row) % 3]
            self.gui.tk.fill_polygon(
                drawing_context, (
                    (x + self.tile_radius, y),
                    (x + self.tile_radius / 2, y - self.tile_inscribed_radius),
                    (x - self.tile_radius / 2, y - self.tile_inscribed_radius),
                    (x - self.tile_radius, y),
                    (x - self.tile_radius / 2, y + self.tile_inscribed_radius),
                    (x + self.tile_radius / 2, y + self.tile_inscribed_radius),
                ), color=color)

class HorizCheckeredHexBoard(CheckeredHexBoard, HorizHexBoard):
    def draw(self, drawing_context, rectangle):
        """Draw board within specified rectangle of self.gui."""

        super(HorizCheckeredHexBoard, self).draw(drawing_context, rectangle)

        for loc in self.holder.all_locations:
            x, y = self.tile_center(loc)
            color = (self.white_color, self.black_color, self.grey_color)[(loc.col + loc.row) % 3]
            self.gui.tk.fill_polygon(
                drawing_context, (
                    (x, y + self.tile_radius),
                    (x + self.tile_inscribed_radius, y + self.tile_radius / 2),
                    (x + self.tile_inscribed_radius, y - self.tile_radius / 2),
                    (x, y - self.tile_radius),
                    (x - self.tile_inscribed_radius, y - self.tile_radius / 2),
                    (x - self.tile_inscribed_radius, y + self.tile_radius / 2),
                ), color=color)
