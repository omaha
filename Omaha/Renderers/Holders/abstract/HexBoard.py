# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .Euclidian2D import Euclidian2DRenderer
from Omaha.Holders.HexBoard import HexBoard
import math

class HexBoardRenderer(Euclidian2DRenderer):
    def __init__(self, **kwargs):
        super(HexBoardRenderer, self).__init__(**kwargs)
        assert isinstance(self.holder, HexBoard)
        self.rectangle = None

    def tile_center(self, location):
        return (self.hextile_center_x(location),
                self.hextile_center_y(location))

    def _compute_deltas(self, rectangle):
        # base values, ignoring slant effect, which must be adjusted in subclasses
        self.delta_x = float(rectangle.width) / self.holder.matrix_width
        self.delta_y = float(rectangle.height) / self.holder.matrix_height

    def draw(self, drawing_context, rectangle):
        """Draw the board.

        This implementation contains common actions to be taken on
        board redraw.  When we get redrawn, we record geometry
        information about the curently-drawn board, to help placing
        pieces and translating pixel coordinates into board
        coordinates.
        """
        self._compute_deltas(rectangle)
        super(HexBoardRenderer, self).draw(drawing_context, rectangle)

    def hextile_center_x(self, location):
        raise NotImplementedError()
    def hextile_center_y(self, location):
        raise NotImplementedError()

# FIXME: those hexagons are not regular ones.

class HorizHexBoard(HexBoardRenderer):
    """
    HexBoad renderer with one diagonal rendered horizontally.
    """
    def __init__(self, **kwargs):
        super(HorizHexBoard, self).__init__(**kwargs)
        self.baserow = self.holder.leftmost_row
    def _compute_deltas(self, rectangle):
        super(HorizHexBoard, self)._compute_deltas(rectangle)
        self.delta_y = self.delta_x * math.sqrt(3) / 2
    def hextile_center_x(self, loc):
        # adjustment for the hexagon slant
        adj = (loc.row - self.baserow) * 0.5
        return int(self.rectangle.x + (loc.col - adj + 0.5) * self.delta_x)
    def hextile_center_y(self, loc):
        return int(self.rectangle.y + (loc.row + 0.5) * self.delta_y)

    @property
    def tile_radius(self):
        return self.delta_y * 2 / 3
    @property
    def tile_inscribed_radius(self):
        return self.delta_x / 2

    # FIXME: lacks point_to_location(self, x, y):

class VertHexBoard(HexBoardRenderer):
    """
    HexBoad renderer with one diagonal rendered vertically.
    """
    def __init__(self, **kwargs):
        super(VertHexBoard, self).__init__(**kwargs)
        self.basecol = self.holder.topmost_col
    def _compute_deltas(self, rectangle):
        super(VertHexBoard, self)._compute_deltas(rectangle)
        self.delta_x = self.delta_y * math.sqrt(3) / 2
    def hextile_center_x(self, loc):
        return int(self.rectangle.x + (loc.col + 0.5) * self.delta_x)
    def hextile_center_y(self, loc):
        # adjustment for the hexagon slant
        adj = (loc.col - self.basecol) * 0.5
        return int(self.rectangle.y + (loc.row - adj + 0.5) * self.delta_y)

    @property
    def tile_radius(self):
        return self.delta_x * 2 / 3
    @property
    def tile_inscribed_radius(self):
        return self.delta_y / 2

    def point_to_location(self, x, y):
        """Get logical square from physical coords within gui."""
        # Do as for a square board, but ajust vertically afterwards.
        # FIXME: adjustment near hex boundaries is needed for exactness.
        # Do float divisions in this case for simplicity.
        col = int((x - self.rectangle.x) / self.delta_x)
        # adjustment for the hexagon slant
        adj = (col - self.basecol) * 0.5
        row = int(adj + (y - self.rectangle.y) / self.delta_y)

        if  (col >= 0 and col < self.holder.matrix_width and
             row >= 0 and row < self.holder.matrix_height):
            loc = self.holder.Location(self.holder, col, row)
            # check if it is really on the board
            try:
                self.holder.piece_at(loc)
            except IndexError:
                return None
            return loc
        else:
            return None
