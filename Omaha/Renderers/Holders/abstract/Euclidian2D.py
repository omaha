# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

class Euclidian2DRenderer(Core.HolderRenderer):
    """
    Base class for 2-dimentional boardrenderers.
    """
    def __init__(self, **kwargs):
        super(Euclidian2DRenderer, self).__init__(**kwargs)
        self.rectangle = None
        self.delta_x, self.delta_y = None, None

    def draw(self, drawing_context, rectangle):
        """Draw the holder.

        This implementation contains common actions to be taken on
        holder redraw.  When we get redrawn, we must record geometry
        information about the curently-drawn holder, to help placing
        pieces and translating pixel coordinates into holder
        coordinates.  Subclasses must therefore compute values here
        for delta_x and delta_y.
        """
        self.rectangle = rectangle
        super(Euclidian2DRenderer, self).draw(drawing_context, rectangle)

    @property
    def tile_inscribed_radius(self):
        """Radius of a circle inscribed in a tile of the holder."""
        raise NotImplementedError()
