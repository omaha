# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .Euclidian2D import Euclidian2DRenderer

class DotLocation(Euclidian2DRenderer):
    """
    Generic class for showing locations using dots.
    Requires all_locations property on board.
    """

    def draw(self, drawing_context, rectangle):
        super(DotLocation, self).draw(drawing_context, rectangle)

        dotsize = max(1, min(int(self.delta_x), int(self.delta_y)) // 10)
        for loc in self.holder.all_locations:
            x, y = self.tile_center(loc)
            self.gui.tk.fill_circle(drawing_context, x, y, dotsize,
                                    color=self.gui.tk.color(self.gui.canvas, "black"))

    def tile_center(self, location):
        raise NotImplementedError()
