# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord
from .Euclidian2D import Euclidian2DRenderer

class RectBoard(Euclidian2DRenderer):
    """
    Base class for 2-dimentional boardrenderers for square boards.
    """

    @Overlord.parameter
    def border_width_percent(cls, context):
        return Overlord.params.Int(label = 'Border width %',
                                   minval = 0, maxval = 20,
                                   default = 5)
    @Overlord.parameter
    def border_color(cls, context):
        return Overlord.params.String(label = 'Border color',
                                      default = "#A66D39")
    @border_color.xformer
    def border_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)

    def point_to_location(self, x, y):
        """Get logical square from physical coords within gui."""
        # Approximate the tile coords by integer division...
        # (the real value would require a float division)
        col = (x - self.rectangle.x) // int(self.delta_x)
        row = (y - self.rectangle.y) // int(self.delta_y)

        # ... and do the necessary adjustment.
        if x < self.tile_corner_x(col):
            col -= 1
        if y < self.tile_corner_y(row):
            row -= 1

        if  (col >= 0 and col < self.holder.matrix_width and
             row >= 0 and row < self.holder.matrix_height):
            return self.holder.Location(self.holder, col, row)
        else:
            return None

    def draw(self, drawing_context, rectangle):
        """Draw the board.

        This implementation contains common actions to be taken on
        board redraw.  When we get redrawn, we record geometry
        information about the curently-drawn board, to help placing
        pieces and translating pixel coordinates into board
        coordinates.
        """
        w = float(rectangle.width) * (100 - 2*self.border_width_percent) / 100
        h = float(rectangle.height) * (100 - 2*self.border_width_percent) / 100
        self.delta_x = w / self.holder.matrix_width
        self.delta_y = h / self.holder.matrix_height

        # border
        if self.border_width_percent:
            self.gui.tk.fill_rectangle(drawing_context,
                                       rectangle.x, rectangle.y,
                                       rectangle.width, rectangle.height,
                                       color=self.border_color)

        super(RectBoard, self).draw(drawing_context, rectangle)

    # tile-positionning methods

    @property
    def board_x0(self):
        return (self.rectangle.x +
                self.rectangle.width *
                (self.border_width_percent) / 100)
    @property
    def board_xn(self):
        return (self.rectangle.x +
                self.rectangle.width *
                (100 - self.border_width_percent) / 100)
    @property
    def board_y0(self):
        return (self.rectangle.y +
                self.rectangle.height *
                (self.border_width_percent) / 100)
    @property
    def board_yn(self):
        return (self.rectangle.y +
                self.rectangle.height *
                (100 - self.border_width_percent) / 100)

    def tile_corner_x(self, col):
        return int(self.board_x0 + col * self.delta_x)
    def tile_corner_y(self, row):
        return int(self.board_y0 + row * self.delta_y)

    def tile_center_x(self, col):
        return int(self.board_x0 + (col + 0.5) * self.delta_x)
    def tile_center_y(self, row):
        return int(self.board_y0 + (row + 0.5) * self.delta_y)

    def tile_center(self, location):
        return (self.tile_center_x(location.col),
                self.tile_center_y(location.row))

    def tile_square(self, location):
        """Square where given location gets drawn."""
        assert location.holder is self.holder
        return Core.Rectangle(
            self.tile_corner_x(location.col),
            self.tile_corner_y(location.row),
            (self.tile_corner_x(location.col + 1) -
             self.tile_corner_x(location.col)),
            (self.tile_corner_y(location.row + 1) -
             self.tile_corner_y(location.row)))

    @property
    def tile_inscribed_radius(self):
        return min(self.delta_x, self.delta_y) / 2
