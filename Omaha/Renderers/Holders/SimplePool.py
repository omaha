# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Holders.Pool import PiecePool
from .abstract.Euclidian2D import Euclidian2DRenderer

class SimplePool(Euclidian2DRenderer):
    def __init__(self, **kwargs):
        super(SimplePool, self).__init__(**kwargs)
        assert isinstance(self.holder, PiecePool)
        self.rectangle = None

    def draw(self, drawing_context, rectangle):
        super(SimplePool, self).draw(drawing_context, rectangle)
        self.rectangle = rectangle
        self.delta_y = float(self.rectangle.height)
        self.delta_x = self.delta_y
        self.gui.tk.draw_rectangle(
            drawing_context,
            rectangle.x, rectangle.y, rectangle.width, rectangle.height,
            color=self.gui.tk.color(self.gui.canvas, "black"))

    def tile_topleft(self, location):
        return (self.rectangle.x + self.delta_x * location.index,
                self.rectangle.y)

    def tile_center(self, location):
        x, y = self.tile_topleft(location)
        return (x + self.rectangle.height // 2,
                y + self.rectangle.height // 2)

    def tile_square(self, location):
        x, y = self.tile_topleft(location)
        return (
            Core.Rectangle(
                x, y, self.rectangle.height, self.rectangle.height))

    def point_to_location(self, area, x, y):
        # ensure there is a piece at this place in pool
        try:
            loc = self.holder.Location(self.holder,
                                       (x - area.x) / self.rectangle.height)
            self.holder.piece_at(loc)
        except IndexError:
            return None

        return loc

    @property
    def tile_inscribed_radius(self):
        return self.rectangle.height // 2
