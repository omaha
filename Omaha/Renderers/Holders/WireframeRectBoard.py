# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.RectBoard import RectBoard

class WireframeRectBoard(RectBoard):

    width = 2

    def draw(self, drawing_context, rectangle):
        """Draw board within specified rectangle of self.gui."""

        super(WireframeRectBoard, self).draw(drawing_context, rectangle)

        black = self.gui.tk.color(self.gui.canvas, "black")
        for i in range(0, self.holder.matrix_width + 1):
            x = self.tile_corner_x(i)
            self.gui.tk.draw_line(drawing_context,
                                  x, self.board_y0, x, self.board_yn,
                                  color=black, width=self.width)
        for i in range(0, self.holder.matrix_height + 1):
            y = self.tile_corner_y(i)
            self.gui.tk.draw_line(drawing_context,
                                  self.board_x0, y, self.board_xn, y,
                                  color=black, width=self.width)
