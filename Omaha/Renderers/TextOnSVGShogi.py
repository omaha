# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from .abstract.SingleBoard import SingleBoardGameRenderer
from .abstract.Fullscreen import FullscreenGameRenderer
from .abstract.SingleBoardAndPools import SingleBoardAndPools
from ..Games.abstract.ShogiGame import ShogiGame

import math

# set id's for the piece renderer
BOARD_PIECE = { ShogiGame.SIDE_SENTE: "board_sente",
                ShogiGame.SIDE_GOTE:  "board_gote" }
POOL_PIECE = { ShogiGame.SIDE_SENTE: "pool_sente",
               ShogiGame.SIDE_GOTE:  "pool_gote" }

# How much to rotate each piece type
# FIXME: should be computed from player's game direction
PIECE_ROTATION = { ShogiGame.SIDE_SENTE: 0,
                   ShogiGame.SIDE_GOTE: math.pi }

class AbstractTextOnSVGShogiRenderer(SingleBoardGameRenderer,
                                     Core.PieceDelegatingGameRenderer):
    """Sample SVG chess renderer for Gtk.

    This one has lots of hardcoded stuff, it is meant as a working
    base to get generic skinnable SVG support.
    """

    @property
    def _piece_sets(self):
        return BOARD_PIECE.values()

    def _piece_set(self, piece):
        # select set of pieces to use
        return BOARD_PIECE[piece.player.color]

    def _side_piece_sets(self, side):
        return [ BOARD_PIECE[side] ]

    def _piece_set_rotation(self, set_id):
        # find which side this set belongs to
        for side in (ShogiGame.SIDE_SENTE, ShogiGame.SIDE_GOTE):
            if set_id in self._side_piece_sets(side):
                return PIECE_ROTATION[side]
        else:
            raise RuntimeError("Unknown piece set")

    def _piece_set_size(self, set_id):
        if set_id in BOARD_PIECE.values():
            # FIXME: should be factorized in SingleBoardGameRenderer
            board_renderer = self.holder_renderers[self.game.board]
            size = min(board_renderer.delta_x, board_renderer.delta_y)
            size = max(1, (100 - 2 * self.MARGIN) * size / 100)
            return size
        raise RuntimeError("Unknown piece set")

class TextOnSVGShogiRenderer(FullscreenGameRenderer,
                             AbstractTextOnSVGShogiRenderer):
    pass

class TextOnSVGPoolShogiRenderer(SingleBoardAndPools,
                                 FullscreenGameRenderer,
                                 AbstractTextOnSVGShogiRenderer):
    """Sample SVG chess renderer for Gtk.

    This one has lots of hardcoded stuff, it is meant as a working
    base to get generic skinnable SVG support.
    """

    def __init__(self, **kwargs):
        super(TextOnSVGPoolShogiRenderer, self).__init__(**kwargs)
        # FIXME: dependant on square-size
        self.pool_height = 35 #8 * self.gui.canvas_size[1] / 100

    @property
    def _piece_sets(self):
        return (super(TextOnSVGPoolShogiRenderer, self)._piece_sets +
                POOL_PIECE.values())

    def _piece_set(self, piece):
        # select set of pieces to use
        if piece.holder is self.game.board:
            return super(TextOnSVGPoolShogiRenderer, self)._piece_set(piece)
        else:
            return POOL_PIECE[piece.player.color]

    def _side_piece_sets(self, side):
        return [ BOARD_PIECE[side], POOL_PIECE[side] ]

    def _piece_set_size(self, set_id):
        if set_id in POOL_PIECE.values():
            return self.pool_height
        return super(TextOnSVGPoolShogiRenderer, self)._piece_set_size(set_id)
