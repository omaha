# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# FIXME: display outcome and disable game controls when Finished

from Omaha import Core
from ..abstract.Play import Play as AbstractPlay

import sip
sip.setapi('QString', 2)
sip.setapi('QVariant', 2)
from PyQt4 import QtCore, QtGui

from Omaha.Toolkits.Qt4 import Canvas, Qt4 as Toolkit

# FIXME: still lacks: subparams, tooltips

class Qt4Play(AbstractPlay):
    def __init__(self, **kwargs):
        self.actions = {}
        self.__moveentry_callbacks = set()
        super(Qt4Play, self).__init__(**kwargs)

    def __add_toolbutton(self, label, key=None, **kwargs):
        action = QtGui.QAction(label, self.window, **kwargs)
        self.toolbar.addAction(action)
        if key:
            self.actions[key] = action

    def _create_gui(self):
        self.window = QtGui.QMainWindow()
        game_name = getattr(type(self.game), 'x-plugin-desc').name
        self.window.setWindowTitle("omaha - {0}".format(game_name))

        vbox = QtGui.QWidget()
        vboxlayout = QtGui.QVBoxLayout(vbox)
        self.window.setCentralWidget(vbox)

        # toolbar
        self.toolbar = QtGui.QToolBar("main toolbar", self.window)
        self.window.addToolBar(QtCore.Qt.TopToolBarArea, self.toolbar)
        # tool buttons
        self.__add_toolbutton("&Close", shortcut=QtGui.QKeySequence.Close,
                              triggered=self._quit_hook)
        self.__add_toolbutton("&Save", shortcut=QtGui.QKeySequence.Save,
                              triggered=self._saveas_hook)
        self.__add_toolbutton("Undo", 'undo', triggered=self._undo_hook)
        self.__add_toolbutton("Redo", 'redo', triggered=self._redo_hook)
        self.__add_toolbutton("Refuse", 'refuse',
                              triggered=self._refuse_hook)
        self.__add_toolbutton("Resign",
                              triggered=self._resign_hook)
        self.__add_toolbutton("Prefs",
                              triggered=self._prefs_hook)

        # canvas
        self.canvas = Canvas(app_ui=self, default_size=QtCore.QSize(800, 600))
        vboxlayout.addWidget(self.canvas, 1)

        # textfield for manually-enterered moves (not shown initially)
        self.moveentry_hbox = QtGui.QWidget()
        layout = QtGui.QHBoxLayout(self.moveentry_hbox)
        layout.addWidget(QtGui.QLabel("Move:"))
        self.move_entry = QtGui.QLineEdit()
        layout.addWidget(self.move_entry)
        vboxlayout.addWidget(self.moveentry_hbox, 0, QtCore.Qt.AlignBottom)
        self.moveentry_hbox.hide()

        self.window.show()

    @property
    def canvas_size(self):
        size = self.canvas.size()
        return (size.width(), size.height())

    def _saveas_hook(self):
        # FIXME: should allow selecting format/suffix
        filename = QtGui.QFileDialog.getSaveFileName(self.window, "Save game")
        if filename:
            self.game.save(filename)

    def add_game_action(self, label, callback):
        action = QtGui.QAction(label, self.window,
                               triggered=callback)
        self.toolbar.addAction(action)
        return action

    def remove_game_action(self, game_action):
        self.toolbar.removeAction(game_action)

    @property
    def main_window(self):
        return self.window

    ###

    def _enable_undo(self):
        for btn_name in ('undo', 'redo', 'refuse'):
            self.actions[btn_name].setEnabled(True)
    def _disable_undo(self):
        for btn_name in ('undo', 'redo', 'refuse'):
            self.actions[btn_name].setEnabled(False)

    ###

    def ack_moveentry(self):
        # stop propagation of click event (to other MouseChesslikePlayer's)
        self.move_entry.stop_emission("activate")

    def set_manual_move_entry_callback(self, callback):
        self.move_entry.textChanged.connect(callback)
        self.__moveentry_callbacks.add(callback)
        self.moveentry_hbox.show()
    def unset_manual_move_entry_callback(self, callback):
        self.move_entry.textChanged.disconnect(callback)
        self.__moveentry_callbacks.remove(callback)
        if len(self.__moveentry_callbacks) == 0:
            self.moveentry_hbox.hide()
    def clear_manual_move_entry(self):
        self.move_entry.clear()
