# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

import sip
sip.setapi('QString', 2)
sip.setapi('QVariant', 2)
from PyQt4 import QtCore, QtGui

from Omaha.Toolkits.Qt4 import Qt4 as Toolkit
from ..abstract.Launcher import Launcher as AbstractLauncher

class Launcher(AbstractLauncher):

    def __init__(self, args, **kwargs):
        super(Launcher, self).__init__(toolkit=Toolkit(cli_args=args),
                                       args=args, **kwargs)

    def _create_gui(self):
        self.window = QtGui.QMainWindow()
        self.window.setWindowTitle("omaha - launcher")

        # default menus
        self.toolbar = QtGui.QToolBar("main toolbar", self.window)
        self.window.addToolBar(QtCore.Qt.TopToolBarArea, self.toolbar)

        self.actionQuit = QtGui.QAction("&Quit", self.window,
                                        shortcut=QtGui.QKeySequence.Quit,
                                        triggered=QtGui.qApp.exit)
        self.toolbar.addAction(self.actionQuit)

        self.actionLoad = QtGui.QAction("&Load", self.window,
                                        triggered=self._loadgame_hook)
        self.toolbar.addAction(self.actionLoad)

        self.actionPlay = QtGui.QAction("&Play", self.window,
                                        triggered=self._playgame_hook)
        self.toolbar.addAction(self.actionPlay)

        # chooser
        self.chooser = QtGui.QListWidget()
        self.window.setCentralWidget(self.chooser)
        self.pluginlist = dict(
            (QtGui.QListWidgetItem(plugin.name), plugin)
            for plugin in self.plugin_manager.get_plugins_list('X-Omaha-Game'))
        for list_item, plugin in self.pluginlist.items():
            self.chooser.addItem(list_item)
        self.chooser.itemDoubleClicked.connect(self._playgameitem_hook)

        self.window.show()

    def _playgame_hook(self, *args): # FIXME: args==?
        self.app(gamedesc=self.pluginlist[self.chooser.currentItem()]).run()

    def _loadgame_hook(self):
        try:
            self.app().run()
        except Core.UserNotification as ex:
            self.tk.notify(ex.title, ex.text)
            return

    def _playgameitem_hook(self, list_item):
        self._run_app(self.pluginlist[list_item])

    def app(self, **kwargs):
        from Omaha.Apps.Qt4.Play import Qt4Play as App
        return App(plugin_manager=self.plugin_manager, toolkit=self.tk,
                   prefs=self.prefs, parentcontext={}, **kwargs)
