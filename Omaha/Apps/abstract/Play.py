# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

import logging
logger = logging.getLogger('Omaha.Apps.Gtk.Play')

class Play(Core.GameApp):

    def __init__(self, **kwargs):
        super(Play, self).__init__(**kwargs)
        self.window = None

    def _quit_hook(self, w):
        self.free_resources()
        self.disconnect_players()
        self.game = None
        self.game_renderer = None
        # FIXME: should cleanup click handlers and such ?

        self.window.destroy()
        self.window = None

    def _resign_hook(self, w):
        # FIXME: should ask confirmation
        self.game.declare_resignation(self.game.whose_turn)

    ###

    def _prefs_hook(self, w):
        params = self.prefs.get(self)

        # suspend game first because of potential PlayerDriver changes
        self.game.ui_suspend()
        # go ask
        self.tk.open_params_dialog(title="UI parameters",
                                   params=params, app_ui=self,
                                   accept_cb=self.set_params,
                                   parent_window=self.main_window,
                                   synchronous=True)
        self.game.ui_resume()

    def set_params(self, params):
        super(Play, self).set_params(params)
        # redraw with new parameters, if the canvas is here already
        if self.canvas:
            self.tk.invalidate_canvas(self.canvas)

    ###

    def _undo_hook(self):
        if isinstance(self.game, Core.UndoableGame):
            try:
                self.game.undo_move()
                self.tk.invalidate_canvas(self.canvas)
            except Core.InvalidMove as ex:
                logger.warning("cannot undo: %s", ex)
        else:
            logger.info("undo not supported")

    def _redo_hook(self):
        try:
            self.game.redo_move()
            self.tk.invalidate_canvas(self.canvas)
        except IndexError:
            logger.info("nothing to redo")
        except Core.InvalidMove as ex:
            logger.warning("cannot redo: %s", ex)

    def _refuse_hook(self):
        try:
            # FIXME: should belong to PlayerDriver
            self.game.refuse_move(self.game.moves.last, self.game.whose_turn)
            self.tk.invalidate_canvas(self.canvas)
        except IndexError:
            logger.error("nothing to refuse")
        except Core.InvalidMove as ex:
            logger.warning("cannot refuse: %s", ex)
