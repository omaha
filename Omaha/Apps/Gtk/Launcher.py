# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import gtk, os

from Omaha.Toolkits.Gtk import Gtk as Toolkit
from ..abstract.Launcher import Launcher as AbstractLauncher

class Launcher(AbstractLauncher):

    def __init__(self, **kwargs):
        self.window = None
        self.widgets = {}
        super(Launcher, self).__init__(toolkit=Toolkit(), **kwargs)

    def _create_gui(self):
        """Create a launcher window.

        A game is selected through a gtk.IconView, which then gets
        repkaced by a gtk.DrawingArea serving as a canvas for the
        renderer."""

        # The container window and vbox
        self.window = gtk.Window()
        self.window.connect("destroy", lambda w: gtk.main_quit())
        self.window.set_title("omaha - launcher")
        self.window.set_default_size(200, 200)

        vbox = gtk.VBox()
        self.window.add(vbox)

        # Toolbar
        self.widgets['launcher_toolbar'] = gtk.Toolbar()
        vbox.pack_start(
            self.widgets['launcher_toolbar'], expand=False)

        # quit
        button = gtk.ToolButton(stock_id=gtk.STOCK_QUIT)
        button.connect("clicked", self._quit_hook)
        self.widgets['launcher_toolbar'].add(button)

        # load
        button = gtk.ToolButton(stock_id=gtk.STOCK_OPEN)
        button.connect("clicked", self._load_hook)
        self.widgets['launcher_toolbar'].add(button)

        # The IconView game launcher itself
        self.widgets['iconview'] = gtk.IconView()
        vbox.pack_start(self.widgets['iconview'])

        model = gtk.ListStore(str, gtk.gdk.Pixbuf, str)
        self.widgets['iconview'].set_model(model)
        self.widgets['iconview'].set_text_column(0)
        self.widgets['iconview'].set_pixbuf_column(1)
        self.widgets['iconview'].set_tooltip_column(2)

        # this has to be a real list since the hook subscripts into it
        pluginlist = [x for x in self.plugin_manager.get_plugins_list(
            'X-Omaha-Game')]
        self.widgets['iconview'].connect("item-activated",
                                         self._iconview_hook, pluginlist)


        # bottom buttons bar
        self.widgets['actionsbar'] = gtk.HBox()
        vbox.pack_start(self.widgets['actionsbar'])

        button = gtk.Button(label="play")
        button.connect("clicked", self._get_launch_hook(
            self._iconview_hook, pluginlist))
        self.widgets['actionsbar'].add(button)

        button = gtk.Button(label="study")
        button.set_sensitive(False)
        #button.connect("clicked", self._quit_hook)
        self.widgets['actionsbar'].add(button)

        button = gtk.Button(label="scribe")
        button.set_sensitive(False)
        #button.connect("clicked", self._quit_hook)
        self.widgets['actionsbar'].add(button)


        # New-game "buttons" within the Iconview
        for plugin in pluginlist:
            if plugin.icon != '':
                if os.path.exists(plugin.icon):
                    pixbuf = gtk.gdk.pixbuf_new_from_file(plugin.icon)
                else:
                    pixbuf = self.widgets['iconview'].render_icon(
                        gtk.STOCK_MISSING_IMAGE, gtk.ICON_SIZE_BUTTON)
            else:
                pixbuf = self.widgets['iconview'].render_icon(
                    gtk.STOCK_FILE, gtk.ICON_SIZE_BUTTON)
            pixbuf.plugin = plugin
            model.append([plugin.name, pixbuf, plugin.comment or plugin.name])

        self.window.show_all()

    def _get_launch_hook(self, iconview_cb, pluginlist):
        def hook(w):
            iconview_cb(self.widgets['iconview'],
                        self.widgets['iconview'].get_selected_items()[0],
                        pluginlist)
        return hook

    @staticmethod
    def _quit_hook(w):
        gtk.main_quit()

    def _load_hook(self, w):
        try:
            self.app().run()
        except Core.UserNotification as ex:
            self.tk.notify(ex.title, ex.text)
            return

    def _iconview_hook(self, iconview, path, gamedescriptions=None):
        """Hook run when a game is selected for run in iconview."""
        self._run_app(gamedescriptions[path[0]])

    def app(self, **kwargs):
        from Omaha.Apps.Gtk.Play import GtkPlay as App
        return App(plugin_manager=self.plugin_manager, toolkit=self.tk,
                   prefs=self.prefs, parentcontext={}, **kwargs)
