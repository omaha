# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# FIXME: display outcome and disable game controls when Finished

from Omaha import Core
from ..abstract.Play import Play as AbstractPlay

import logging
import gtk

logger = logging.getLogger('Omaha.Apps.Gtk.Play')

class GtkPlay(AbstractPlay):

    def __init__(self, **kwargs):
        self.widgets = {}
        self.__moveentry_callbacks = {}
        self.__connections = []
        super(GtkPlay, self).__init__(**kwargs)

    @property
    def canvas_size(self):
        return self.canvas.window.get_size()

    def __add_toolbutton(self, hook, key=None, **kwargs):
        button = gtk.ToolButton(**kwargs)
        button.connect("clicked", hook)
        self.widgets['game_toolbar'].add(button)
        if key:
            self.widgets[key] = button

    @staticmethod
    def __wrap_cb(callback):
        def wrapped_callback(w):
            callback()
        return wrapped_callback

    def _create_gui(self):
        # The container window and vbox
        self.window = gtk.Window()
        self.__connections.append(
            (self.window, self.window.connect("destroy", self._quit_hook)) )
        game_name = getattr(type(self.game), 'x-plugin-desc').name
        self.window.set_title("omaha - {0}".format(game_name))
        self.window.set_default_size(800, 600)

        vbox = gtk.VBox()
        self.window.add(vbox)
        vbox.show()

        icon_theme = gtk.icon_theme_get_default()

        # A toolbar
        self.widgets['game_toolbar'] = gtk.Toolbar()
        vbox.pack_start(self.widgets['game_toolbar'], expand=False)
        icon_size = gtk.icon_size_lookup(
            self.widgets['game_toolbar'].get_icon_size())[0]
        # tool buttons
        self.__add_toolbutton(self._quit_hook, stock_id=gtk.STOCK_CLOSE)
        self.__add_toolbutton(self._saveas_hook, stock_id=gtk.STOCK_SAVE)
        self.__add_toolbutton(self.__wrap_cb(self._undo_hook), 'undo',
                              stock_id=gtk.STOCK_UNDO)
        self.__add_toolbutton(self.__wrap_cb(self._redo_hook), 'redo',
                              stock_id=gtk.STOCK_REDO)
        self.__add_toolbutton(
            self.__wrap_cb(self._refuse_hook), 'refuse',
            icon_widget = gtk.image_new_from_pixbuf(icon_theme.load_icon(
                "gtk-no", icon_size, 0)),
            label="Refuse")
        self.__add_toolbutton(
            self._resign_hook,
            icon_widget = gtk.image_new_from_pixbuf(icon_theme.load_icon(
                "gtk-stop", icon_size, 0)),
            label="Resign")
        self.__add_toolbutton(self._prefs_hook, stock_id=gtk.STOCK_PREFERENCES)

        self.widgets['game_toolbar'].show_all()

        # the canvas
        self.canvas = gtk.DrawingArea()
        self.canvas.set_events(gtk.gdk.BUTTON_PRESS_MASK |
                               gtk.gdk.BUTTON_RELEASE_MASK |
                               gtk.gdk.POINTER_MOTION_MASK |
                               gtk.gdk.POINTER_MOTION_HINT_MASK)
        vbox.pack_start(self.canvas)

        if self.game.has_tooltip:
            self.canvas.set_property('has-tooltip', True)
            self.canvas.connect("query-tooltip", self.set_tooltip)

        self.canvas.show_all()

        # textfield for manually-enterered moves (not shown initially)
        self.widgets['moveentry_hbox'] = gtk.HBox()
        self.widgets['moveentry_hbox'].pack_start(gtk.Label("Move:"),
                                                  expand=False)
        self.widgets['move_entry'] = gtk.Entry()
        self.widgets['moveentry_hbox'].pack_start(self.widgets['move_entry'])
        vbox.pack_end(self.widgets['moveentry_hbox'], expand=False)

        self.window.show()

        self.__connections.append(
            (self.canvas, self.canvas.connect("expose-event",
                                              lambda w, evt: self.game_renderer.draw())) )
        self.tk.invalidate_canvas(self.canvas)

    def free_resources(self):
        super(GtkPlay, self).free_resources()
        for widget, connection in self.__connections:
            widget.disconnect(connection)

    def add_game_action(self, label, callback):
        button = gtk.ToolButton(label=label)
        handle = button.connect("clicked", self.__wrap_cb(callback))
        self.widgets['game_toolbar'].add(button)
        button.show_all()
        return (button, handle)

    def remove_game_action(self, game_action):
        button, handle = game_action
        self.widgets['game_toolbar'].remove(button)
        button.disconnect(handle)
        button.destroy()

    @property
    def main_window(self):
        return self.window

    def _saveas_hook(self, w):
        try:
            # FIXME: should allow selecting format/suffix
            dlg = gtk.FileChooserDialog(
                action=gtk.FILE_CHOOSER_ACTION_SAVE,
                buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                         gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
            answer = dlg.run()
            if answer == gtk.RESPONSE_ACCEPT:
                self.game.save(dlg.get_filename())
        finally:
            dlg.destroy()

    ###

    def _enable_undo(self):
        for btn_name in ('undo', 'redo', 'refuse'):
            self.widgets[btn_name].set_sensitive(True)
    def _disable_undo(self):
        for btn_name in ('undo', 'redo', 'refuse'):
            self.widgets[btn_name].set_sensitive(False)

    ###

    def ack_moveentry(self):
        # stop propagation of click event (to other MouseChesslikePlayer's)
        self.widgets['move_entry'].stop_emission("activate")

    def set_manual_move_entry_callback(self, callback):
        self.__moveentry_callbacks[callback] = \
            self.widgets['move_entry'].connect("activate",
                                               lambda w: callback(w.get_text()))
        self.widgets['moveentry_hbox'].show_all()
    def unset_manual_move_entry_callback(self, callback):
        self.widgets['move_entry'].disconnect(
            self.__moveentry_callbacks[callback])
        del self.__moveentry_callbacks[callback]
        if len(self.__moveentry_callbacks) == 0:
            self.widgets['moveentry_hbox'].hide()
    def clear_manual_move_entry(self):
        self.widgets['move_entry'].set_text('')

    ###

    def set_tooltip(self, widget, x, y, keyboard_mode, tooltip):
        assert widget is self.canvas
        # FIXME: return early if recompute_areas was not called yet,
        # or presence of pointer in game window when loading game
        # triggers (harmless) exception
        location = self.game_renderer.point_to_location(x, y)
        text = self.game.location_tooltip_text(location)
        if text is None:
            return False
        tooltip.set_text(text)
        return True
