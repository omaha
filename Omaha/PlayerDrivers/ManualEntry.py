# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.NotationBased import NotationBasedPlayerDriver
from Omaha import Core
import logging

logger = logging.getLogger('Omaha.PlayerDrivers.ManualEntry')

class ManualEntryPlayerDriver(NotationBasedPlayerDriver):

    def connect(self):
        self.gui.set_manual_move_entry_callback(self.handle_move)
        self.gui.game.signal_ready(self.player, True)

    def disconnect(self):
        self.gui.game.signal_ready(self.player, False)
        self.gui.unset_manual_move_entry_callback(self.handle_move)

    def handle_move(self, move_text):
        if self.gui.game.whose_turn != self.player:
            # not my turn
            return

        try:
            move = self.notation.move_deserialization(move_text, self.player)
        except Core.ParseError as ex:
            self.gui.tk.notify("error",
                               "Syntax error: %s" % (ex,))
            return

        # send move to GUI
        try:
            self.gui.game.make_move(self.player, move)
            self.gui.ack_moveentry()
            self.gui.clear_manual_move_entry()
        except Core.InvalidMove as ex:
            logger.error("Move refused: %s", ex)

        # refresh display
        self.gui.tk.invalidate_canvas(self.gui.canvas)
