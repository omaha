# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord
from Overlord.misc import classproperty
import logging

logger = logging.getLogger('Omaha.PlayerDrivers.Multiplexer')

class MultiplexerPlayerDriver(Core.PlayerDriver):
    def __init__(self, **kwargs):
        """
        Create a player driver using gui for human interaction.

        It supports games with a board and per-player pools.
        """
        super(MultiplexerPlayerDriver, self).__init__(**kwargs)
        self.__drivers = []

    @classproperty
    @classmethod
    def context_key(cls):
        # This is a quick hack to avoid the multiplexer to take the
        # PlayerDriver context slot.
        return MultiplexerPlayerDriver

    def connect(self):
        for driver in self.driver_plugins:
            cls = driver.Class
            drv = cls(gui=self.gui, player=self.player,
                      parentcontext=self.context)
            drv.connect()
            self.__drivers.append(drv)
        logger.debug("connect: %s", self.__drivers)

    def disconnect(self):
        for driver in self.__drivers:
            driver.disconnect()
        self.__drivers = []

    @Overlord.parameter
    def driver_plugins(cls, context):
        # FIXME: this indeed depends on game
        return Overlord.params.MultiChoice(
            label = 'Drivers',
            alternatives=dict(
                [ (driver.name, driver)
                  for driver in context["app"].plugin_manager. \
                      get_plugins_list('X-Omaha-PlayerDriver') ]),
            )
