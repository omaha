# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Games.abstract.StandardShogi import EnglishShogiWithDropsNotation
import Overlord
import re, subprocess, logging

logger = logging.getLogger('Omaha.PlayerDrivers.XShogi')

class XShogiNotation(EnglishShogiWithDropsNotation):
    # xshogi does not accept piece name
    def move_serialization(self, move):
        if move.source.holder is not self.game.board:
            # must be a drop
            return "%s*%s" % (self.piece_notation.piece_name(move.piece),
                              self.location_notation.location_name(move.target))
        return (self.location_notation.location_name(move.source) +
                self.location_notation.location_name(move.target) +
                ("+" if move.promotes else ""))

# FIXME refuse "Illegal move"
# FIXME "undo"
class XShogiPlayerDriver(Core.PlayerDriver):

    def __init__(self, **kwargs):
        super(XShogiPlayerDriver, self).__init__(**kwargs)
        self.__notation = XShogiNotation(self.gui.game)
        self.__process = None
        if self.player is self.gui.game.players[0]:
            self.__letter = 'S'
        else:
            self.__letter = "G"

    def set_game(self):
        def _piece_position(p):
            pname = self.__notation.piece_notation.piece_name(p, self.__notation.piece_notation.unpromoted)
            if p.holder is self.gui.game.board:
                return (pname +
                        self.__notation.location_notation.location_name(p.location) +
                        ("+" if p.promoted else ""))
            else: # prisonner
                return pname + "*"

        self.write("edit")
        self.write("#") # clear board first
        # first, sente pieces
        for p in self.gui.game.pieces:
            if p.player is not self.gui.game.players[0]:
                continue
            self.write(_piece_position(p))
        # then, gote pieces
        self.write("c") # switch color
        for p in self.gui.game.pieces:
            if p.player is not self.gui.game.players[1]:
                continue
            self.write(_piece_position(p))
        self.write(".") # done

    def disconnect(self):
        logger.info("disconnecting XShogiPlayerDriver...")
        self.gui.game.signal_ready(self.player, False)
        self.gui.game.unregister_listener(self.handle_event)
        self.gui.tk.unset_stream_error_callback(self.handle_ioerror)
        self.gui.tk.unset_stream_read_callback(self.handle_data)
        self.gui.tk.unset_stream_read_callback(self.handle_stderr)
        self.write("quit")
        self.__process.communicate()

    @Overlord.parameter
    def program(cls, context):
        return Overlord.params.String(
            label = 'Command',
            #default="strace -f -o /tmp/foo.log gnuchess xboard",
            default="gnushogi",
            )
    @Overlord.parameter
    def depth(cls, context):
        return Overlord.params.Int(
            label = 'Exploration Depth',
            default = 10,           # 39 by default
            minval = 1,
            maxval = 59             # arbitrary
            )

    def connect(self):
        try:
            logger.debug("Launching command {0!r}".format(self.program))
            self.__process = subprocess.Popen(
                self.program,
                shell=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                universal_newlines=True)
        except OSError as ex:
            raise RuntimeError("could not exec %s: %s" %
                               (self.program, ex))

        self.gui.tk.set_stream_read_callback(self.__process.stdout,
                                             self.handle_data)
        self.gui.tk.set_stream_error_callback(self.__process.stdout,
                                              self.handle_ioerror)
        self.gui.tk.set_stream_read_callback(self.__process.stderr,
                                             self.handle_stderr)
        self.gui.game.register_listener(self.handle_event)

        if ( self.gui.game.moves.nmoves_done > 0 or
             self.gui.game.handicap ):
            logger.info("Injecting state position to AI...")
            self.set_game()

        # disable time control
        self.write("level 0 0")
        # request prompt for exploration depth
        self.write("depth")
        self.write(str(self.depth))
        self.write("junk") # hack to force prompt to get written

        if self.gui.game.whose_turn is self.gui.game.players[0]:
            self.write("black")
        else:
            self.write("white")

        self.gui.game.signal_ready(self.player, True)

    def write(self, data):
        logger.debug("%s>%s", self.__letter, data)
        self.__process.stdin.write(data + "\n")

    def handle_event(self, event):
        if isinstance(event, Core.Game.MoveEvent):
            if event.move.piece.player is not self.player:
                movestr = self.__notation.move_serialization(event.move)
                self.write(movestr)
        elif isinstance(event, Core.Game.PhaseChangeEvent):
            if event.newphase is self.gui.game.Phases.Playing:
                # launch game
                if self.player is self.gui.game.whose_turn:
                    self.write("go")
            elif event.newphase in (self.gui.game.Phases.Suspended,
                                    self.gui.game.Phases.Over):
                self.write("force")

    def handle_data(self, stream):
        line = stream.readline().strip()
        logger.debug("%s<%s", self.__letter, line)

        if self.name is None:
            self.name = line
            logger.info("%s is played by: %s", self.player.name, self.name)
            return

        # depth prompt ?
        if line.startswith('depth = '):
            return # the "junk" hack took care of it
            #self.write(str(self.depth))

        # accepted opponent move ?
        #match = re.match(r'\d+\. (%s) +(\d+)' % re.escape(movestr), line)
        match = re.match(r'\d+\. ([A-Z]\*)?[0-9a-z]+\+? +(\d+)', line)
        if match:
            return                  # move was accepted

        # illegal move from opponent ?
        match = re.match(r'Illegal move \((.*)\)', line)
        if match:
            logger.warning('AI refused move: "%s"', match.group(0))
            self.gui.game.refuse_move(self.gui.game.moves.last, self.player)
            # refresh display
            self.gui.tk.invalidate_canvas(self.gui.canvas)
            return

        # IA move ?
        match = re.match(r'\d+\. \.\.\. (\S+) +(-?\d+)', line)
        if match:
            move = self.__notation.move_deserialization(match.group(1),
                                                        self.player)
            try:
                self.gui.game.make_move(self.player, move)
            except Core.InvalidMove, e:
                logger.critical("Game refused AI move %s: %s", match.group(1), e)
                raise
            else:
                # refresh display
                self.gui.tk.invalidate_canvas(self.gui.canvas)
            return

        # victory/defeat ?
        match = re.match(r'(Black|White) mates', line)
        if match:
            winner_num = dict(Black=0, White=1)[match.group(1)]
            winner = self.gui.game.players[winner_num]
            outcome = {winner: self.gui.game.Outcomes.Winner}
            self.gui.game.propose_outcome(self.player, outcome)
            return

        if line.startswith('Drawn game!'):
            outcome = {self.player: self.gui.game.Outcomes.IsDraw,
                       self.player.next: self.gui.game.Outcomes.IsDraw}
            self.gui.game.propose_outcome(self.player, outcome)
            return

        logger.warning("unhandled IA output: %r", line)

    def handle_stderr(self, stream):
        line = stream.readline().strip()
        logger.debug("E:%s<%s", self.__letter, line)

    def handle_ioerror(self, stream):
        logger.warning("connection lost with engine")
        self.disconnect()
