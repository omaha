# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

import logging
logger = logging.getLogger('Omaha.PlayerDrivers.PointerBased')

class PointerBasedPlayerDriver(Core.PlayerDriver):
    def __init__(self, **kwargs):
        super(PointerBasedPlayerDriver, self).__init__(**kwargs)
        self.__handlers = {}

    def connect(self):
        logger.debug("connect")
        for specialmove_name, specialmove_args in self.gui.game.Move.special_moves.items():
            self.__handlers[specialmove_name] = \
                self.gui.add_game_action(specialmove_name,
                                         self.specialmove_handler(specialmove_name,
                                                                  specialmove_args))
        self.gui.tk.set_mouse_release_callback(self.gui.canvas,
                                               self.mouse_release)
        self.gui.tk.set_mouse_press_callback(self.gui.canvas,
                                             self.mouse_press)
        self.gui.tk.set_mouse_move_callback(self.gui.canvas,
                                            self.mouse_move)
        self.gui.game.signal_ready(self.player, True)

    def disconnect(self):
        logger.debug("disconnect")
        self.gui.game.signal_ready(self.player, False)
        self.gui.tk.unset_mouse_release_callback(self.gui.canvas,
                                                 self.mouse_release)
        self.gui.tk.unset_mouse_press_callback(self.gui.canvas,
                                               self.mouse_press)
        self.gui.tk.unset_mouse_move_callback(self.gui.canvas,
                                              self.mouse_move)
        for handler in self.__handlers.values():
            self.gui.remove_game_action(handler)

    def supports_undo(self):
        return True

    def specialmove_handler(self, name, args):
        def handler():
            self.gui.game.make_move(self.player, self.gui.game.Move(**args))
        handler.__name__ = 'specialmove_handler({0})'.format(name)
        return handler

    def mouse_press(self, x, y):
        if self.gui.game.whose_turn != self.player:
            return False

        location = self.gui.game_renderer.point_to_location(x, y)
        self.__highlight_maybe(location)
        return True

    def mouse_move(self, x, y, pressed):
        if self.gui.game.whose_turn != self.player:
            return False
        if pressed:
            location = self.gui.game_renderer.point_to_location(x, y)
            self.__highlight_maybe(location)
        return True

    def __highlight_maybe(self, location):
        if location is None:
            self.gui.game_renderer.clear_highlights(Core.LocHighlights.MAYBE)
            return

        hl = self.gui.game_renderer.highlights(Core.LocHighlights.MAYBE)
        if len(hl) == 0 or location != hl[0]:
            self.gui.game_renderer.clear_highlights(Core.LocHighlights.MAYBE)
            self.gui.game_renderer.add_highlights(Core.LocHighlights.MAYBE,
                                                  [location])

    def mouse_release(self, x, y):
        if self.gui.game.whose_turn != self.player:
            return False

        self.gui.game_renderer.clear_highlights(Core.LocHighlights.MAYBE)

        return True
