# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord
from Overlord import plugin_pattern
import logging

logger = logging.getLogger('Omaha.PlayerDrivers.NotationBased')

class NotationBasedPlayerDriver(Core.PlayerDriver):

    def __init__(self, **kwargs):
        super(NotationBasedPlayerDriver, self).__init__(**kwargs)
        self.__notation = None

    def supports_undo(self):
        return True

    @Overlord.parameter
    def notation_class(cls, context):
        game_class = context[Core.Game] # set early in __init__ and won't ever change
        game_plugin = getattr(game_class, 'x-plugin-desc')
        if game_plugin.has_datafield("X-Omaha-DefaultNotation"):
            default_notation = game_plugin.datafield("X-Omaha-DefaultNotation")
            logger.info("Using %s as requested by %s",
                        default_notation, game_plugin.name)
        else:
            default_notation = None
        return Overlord.params.Plugin(
            label = 'Move notation for %s' % game_class.__name__,
            plugintype = 'X-Omaha-MoveNotation',
            context = context,
            pattern = plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_class),
            default = default_notation
            )

    @property
    def notation(self):
        notationcls = self.notation_class.Class
        # (re)instanciate notation if required
        if not isinstance(self.__notation, notationcls):
            self.__notation = notationcls(self.gui.game)
        return self.__notation
