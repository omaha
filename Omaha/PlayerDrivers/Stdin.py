# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.NotationBased import NotationBasedPlayerDriver
from Omaha import Core
from sys import stdin
import logging

logger = logging.getLogger('Omaha.PlayerDrivers.Stdin')

class StdinPlayerDriver(NotationBasedPlayerDriver):

    def connect(self):
        self.gui.tk.set_stream_read_callback(stdin, self.handle_data)
        self.gui.game.signal_ready(self.player, True)

    def disconnect(self):
        self.gui.game.signal_ready(self.player, False)
        self.gui.tk.unset_stream_read_callback(self.handle_data)

    def handle_data(self, stream):
        line = stream.readline().strip()
        try:
            move = self.notation.move_deserialization(line, self.player)
        except Core.ParseError as ex:
            logger.error("Syntax error: %s", ex)
            return

        # send move to GUI
        try:
            self.gui.game.make_move(self.player, move)
        except Core.OmahaException as ex:
            logger.error("Move refused: %s", ex)
                # refresh display

        # refresh display
        self.gui.tk.invalidate_canvas(self.gui.canvas)
