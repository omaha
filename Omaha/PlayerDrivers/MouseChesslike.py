# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
A human player for chess-like games.  Only capable for now of moves
of the origin-click/destination-click type.
"""

from Omaha import Core
from .abstract.PointerBased import PointerBasedPlayerDriver
import logging

logger = logging.getLogger('Omaha.PlayerDrivers.MouseChesslike')

class MouseChesslikePlayerDriver(PointerBasedPlayerDriver):

    def __init__(self, **kwargs):
        """
        Create a player driver for moving pieces with the mouse.

        It supports games with a board and per-player pools.
        """
        super(MouseChesslikePlayerDriver, self).__init__(**kwargs)
        self.current_move = None

    def __reset_move(self):
        """Move was applied or cancelled, prepare for next one."""
        self.current_move = None
        self.gui.game_renderer.clear_highlights(Core.LocHighlights.COMPOSING)
        self.gui.game_renderer.clear_highlights(Core.LocHighlights.REACHABLE)

    def __set_source(self, location):
        self.current_move.set_source(location)
        self.gui.game_renderer.clear_highlights(Core.LocHighlights.COMPOSING)
        self.gui.game_renderer.add_highlights(Core.LocHighlights.COMPOSING,
                                              [location])
        self.gui.game_renderer.clear_highlights(Core.LocHighlights.REACHABLE)
        self.gui.game_renderer.add_highlights(
            Core.LocHighlights.REACHABLE,
            [ move.target for move in
              self.gui.game.valid_move_completions(self.current_move)] )

    def mouse_release(self, x, y):
        """Handler for a mouse release signal in gui."""
        if not super(MouseChesslikePlayerDriver, self).mouse_release(x, y):
            return False

        location = self.gui.game_renderer.point_to_location(x, y)

        if location is None:
            # click was out of all holders, restart move if any
            self.__reset_move()
            return True

        piece = location.holder.piece_at(location)

        if self.current_move is None:
            self.current_move = self.gui.game.Move()
        if self.current_move.source is None:
            if piece is None or piece.player is not self.player:
                # no piece in this location, or opponent's piece
                return True

            self.__set_source(location)
        else:
            # moves must be towards board, refuse moving to pools
            if location.holder != self.gui.game.board:
                if piece.player is self.player:
                    # reset source of move to piece in pool
                    self.__set_source(location)
                else:
                    return True

            self.current_move.set_target(location)

            # send move to game
            try:
                self.do_move()
                self.__reset_move()
            except Core.InvalidMove as ex:
                if piece is not None and piece.player is self.player:
                    logger.debug("invalid move, reseting source instead")
                    self.__set_source(location)
                else:
                    logger.error(
                        "invalid move %s: %s",
                        self.gui.game.default_notation.move_serialization(self.current_move), ex)
                    self.__reset_move()
            except Core.UserCancel:
                pass
        return True
