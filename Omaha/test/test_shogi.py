# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import Overlord
from Omaha import Core
from .lib import StubToolkit
from Omaha.GameIO.PGNReader import PGNGameReader

def test_loadpgn():
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    plugin_manager = Overlord.PluginManager("plugins")
    for filename in ['Omaha/test/data/shogi1.pgn']:
        with open(filename) as f:
            reader = PGNGameReader(f, plugin_manager,
                                   parentcontext=app.context)
            g = reader.game()
