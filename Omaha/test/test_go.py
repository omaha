# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import Overlord
from Omaha import Core
from .lib import StubToolkit, make_move
from Omaha.Games.Go import Go
from Omaha.GameIO.SGFReader import SGFGameReader
from nose.tools import eq_

def test_go():
    # create a game
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    g = Go(parentcontext=app.context)
    g.setup()
    g.change_phase(g.Phases.Playing) # FIXME!
    eq_(g.phase, g.Phases.Playing)

    # replay a small partial game
    moves = "r3 d16 q17 d3 r15 c5".split(' ')
    for move_str in moves:
        make_move(g, move_str)

    # check undo
    g.undo_move()
    make_move(g, "c4")
    g.undo_move()
    make_move(g, "c5")

    # compare effective moves with original list
    eq_([g.default_notation.move_serialization(m)
         for m in g.moves.played], moves)

def test_nonstandardgo():
    # create a game
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    g = Go(parentcontext=app.context, paramvalues=dict(boardsize=28,
                                                       handicap=3))
    g.setup()
    g.change_phase(g.Phases.Playing) # FIXME!
    eq_(g.phase, g.Phases.Playing)

    # replay a small partial game
    moves = "aa6 x3 bb9".split(' ')
    for move_str in moves:
        make_move(g, move_str)
    # compare effective moves with original list
    eq_([g.default_notation.move_serialization(m)
         for m in g.moves.played], moves)


def test_loadsgf():
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    plugin_manager = Overlord.PluginManager("plugins")
    for filename in ['Omaha/test/data/go19a.sgf']:
        with open(filename) as f:
            reader = SGFGameReader(f, plugin_manager,
                                   parentcontext=app.context)
            g = reader.game()
