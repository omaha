# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import Overlord
from Omaha import Core
from .lib import StubToolkit, make_move
from Omaha.Games.Chess import Chess
from Omaha.Games.abstract import ChessGame
from Omaha.GameIO.PGNReader import PGNGameReader
from nose.tools import eq_, ok_, assert_raises

def test_chess():
    # create a game
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    g = Chess(parentcontext=app.context)
    g.setup()
    g.change_phase(g.Phases.Playing) # FIXME!
    eq_(g.phase, g.Phases.Playing)

    # replay a small partial game
    moves = "e2e4 e7e5 d2d4 e5d4".split(' ')
    for move_str in moves:
        make_move(g, move_str)
    # compare effective moves with original list
    eq_([g.default_notation.move_serialization(m)
         for m in g.moves.played], moves)

    moves = "Nf3 Nc6 Bb5 Qe7 O-O Qxe4 Re1".split(' ')
    san = ChessGame.SANNotation(g)
    for move_str in moves:
        make_move(g, move_str, notation=san)

    # check pinning
    with assert_raises(Core.InvalidMove) as cm:
        make_move(g, "e4f5")
    ex = cm.exception
    ok_(ex.args[0].startswith("player's king would be in check"),
        "Pinned piece should be forbidden to move")

    # check undo
    g.undo_move()
    make_move(g, "Qe1", notation=san)
    g.undo_move()
    g.redo_move()

def test_loadpgn():
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    plugin_manager = Overlord.PluginManager("plugins")
    for filename in ['Omaha/test/data/chess1.pgn',
                     'Omaha/test/data/chess_O-O-O+.pgn',
                     'Omaha/test/data/chess_promotion.pgn',
                     'Omaha/test/data/chess_disambiguation.pgn']:
        with open(filename) as f:
            reader = PGNGameReader(f, plugin_manager,
                                   parentcontext=app.context)
            g = reader.game()
