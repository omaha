# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import Overlord
from Omaha import Core
from .lib import StubToolkit, make_move
from Omaha.Games.MiniShogi import MiniShogi
from Omaha.Games.piecetypes.shogi import ShogiFacetype
from nose.tools import eq_, ok_, assert_raises

class test(object):
    def setup(self):
        # create a game
        app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                          toolkit=StubToolkit(), parentcontext={})
        g = MiniShogi(parentcontext=app.context)
        g.setup()
        g.change_phase(g.Phases.Playing) # FIXME!
        self.game = g
    def test_minishogi(self):
        g = self.game
        eq_(g.phase, g.Phases.Playing)

        # replay a small partial game
        moves = "B2e-1d B4ax1d R1ex1d B*2e R1d-1e B2e-5b+".split(' ')
        for move_str in moves:
            make_move(g, move_str)

        # checking move
        make_move(g, "B*3c")

        # check refusal of a move that does not protect the king
        with assert_raises(Core.InvalidMove) as cm:
            make_move(g, "+B5b-4c")
        ex = cm.exception
        ok_(ex.args[0].startswith("player's king would be in check"),
            "Non-protecting move should be forbidden while in check")

        make_move(g, "G2a-2b")
        make_move(g, "B3c-4d")

        # check pinning
        with assert_raises(Core.InvalidMove) as cm:
            make_move(g, "G2b-2c")
        ex = cm.exception
        ok_(ex.args[0].startswith("player's king would be in check"),
            "Non-protecting move should be forbidden while in check")

        # undo the 3 moves used for in-check tests
        extramoves_num = 3
        for _ in range(extramoves_num): g.undo_move()

        # compare effective moves with original list
        eq_([g.default_notation.move_serialization(m)
             for m in g.moves.played], moves)

        # check promotion status of bishop
        bishop = g.board.piece_at(g.board.Location(g.board, 0, 1))
        assert bishop
        assert bishop.promoted

        # check that clone() returns a sufficiently-working and independant game
        g2 = g.clone()
        eq_(g2.boardwidth, g.boardwidth)
        eq_(g2.boardheight, g.boardheight)
        # FIXME: hm, how are they so correctly set ?
        eq_(g2.board.ncolumns, g.board.ncolumns)
        eq_(g2.board.nrows, g.board.nrows)
        for move_str in "G4e-4d +B5b-4b".split(' '):
            make_move(g, move_str)
            make_move(g2, move_str)

    def test_undo(self):
        g = self.game

        make_move(g, "R1ex1b")
        g.undo_move()

        # check restored state
        rook = g.board.piece_at(g.board.Location(g.board, 4, 4))
        assert rook
        eq_(rook.type, ShogiFacetype.ROOK)
        pawn = g.board.piece_at(g.board.Location(g.board, 4, 1))
        assert pawn
        eq_(pawn.type, ShogiFacetype.PAWN)

def test_handicap():
    # create a game
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    g = MiniShogi(parentcontext=app.context,
                  paramvalues=dict(handicap=(ShogiFacetype.ROOK,
                                             ShogiFacetype.BISHOP)))
    g.setup()
