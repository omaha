# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# Simple "import module" tests for those modules not otherwise covered
# by unit tests.

class test_PlayerDrivers(object):
    def test_GTP2Program(self):
        import Omaha.PlayerDrivers.GTP2Program
    def test_ManualEntry(self):
        import Omaha.PlayerDrivers.ManualEntry
    def test_MouseChesslike(self):
        import Omaha.PlayerDrivers.MouseChesslike
    def test_MouseSingleClick(self):
        import Omaha.PlayerDrivers.MouseSingleClick
    def test_MouseWaypoint(self):
        import Omaha.PlayerDrivers.MouseWaypoint
    def test_Multiplexer(self):
        import Omaha.PlayerDrivers.Multiplexer
    def test_Stdin(self):
        import Omaha.PlayerDrivers.Stdin
    def test_XBoard(self):
        import Omaha.PlayerDrivers.XBoard
    def test_XShogi(self):
        import Omaha.PlayerDrivers.XShogi

class test_Renderers(object):
    def test_Null(self):
        import Omaha.Renderers.Null
    def test_AsciiChess(self):
        import Omaha.Renderers.AsciiChess
    def test_AsciiShogi(self):
        import Omaha.Renderers.AsciiShogi
    def test_FullscreenPieceDelegating(self):
        import Omaha.Renderers.FullscreenPieceDelegating
    def test_FullscreenPoolPieceDelegating(self):
        import Omaha.Renderers.FullscreenPoolPieceDelegating
    def test_TextOnSVGShogi(self):
        import Omaha.Renderers.TextOnSVGShogi

class test_PieceRenderers(object):
    def test_Null(self):
        import Omaha.Renderers.Pieces.Null
    def test_SimpleDrawnStones(self):
        import Omaha.Renderers.Pieces.SimpleDrawnStones
    def test_SVGChess(self):
        import Omaha.Renderers.Pieces.SVGChess
    def test_TextOnSVGShogi(self):
        import Omaha.Renderers.Pieces.TextOnSVGShogi

class test_HolderRenderers(object):
    def test_CheckeredHexBoard(self):
        import Omaha.Renderers.Holders.CheckeredHexBoard
    def test_CheckeredRectBoard(self):
        import Omaha.Renderers.Holders.CheckeredRectBoard
    def test_GoBan(self):
        import Omaha.Renderers.Holders.GoBan
    def test_HexDotLocation(self):
        import Omaha.Renderers.Holders.HexDotLocation
    def test_SimplePool(self):
        import Omaha.Renderers.Holders.SimplePool
    def test_SquareDotLocation(self):
        import Omaha.Renderers.Holders.SquareDotLocation
    def test_WireframeRectBoard(self):
        import Omaha.Renderers.Holders.WireframeRectBoard

class test_LocationHighlighters(object):
    def test_CircledLocationHighlighter(self):
        import Omaha.Renderers.LocationHighlighters.CircledLocationHighlighter
    def test_ColorSquareHighlighter(self):
        import Omaha.Renderers.LocationHighlighters.ColorSquareHighlighter
    def test_CrossHairHighlighter(self):
        import Omaha.Renderers.LocationHighlighters.CrossHairHighlighter
    def test_SimpleSquareHighlighter(self):
        import Omaha.Renderers.LocationHighlighters.SimpleSquareHighlighter

class test_MoveHighlighters(object):
    def test_MoveTargetHighlighter(self):
        import Omaha.Renderers.MoveHighlighters.MoveTargetHighlighter

class test_Apps(object):
    def test_GtkLauncher(self):
        import Omaha.Apps.Gtk.Launcher
    def test_GtkPlay(self):
        import Omaha.Apps.Gtk.Play
    def test_Qt4Launcher(self):
        import Omaha.Apps.Qt4.Launcher
    def test_Qt4Play(self):
        import Omaha.Apps.Qt4.Play
