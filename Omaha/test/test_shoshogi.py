# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import Overlord
from Omaha import Core
from Omaha.Games.ShoShogi import ShoShogi

from .lib import StubToolkit, make_move
from Overlord.test.lib import known_failure
from nose.tools import eq_, ok_, assert_raises

def test_shoshogi():
    # create a game
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    g = ShoShogi(parentcontext=app.context)
    g.setup()
    g.change_phase(g.Phases.Playing) # FIXME!
    eq_(g.phase, g.Phases.Playing)

    # replay a small partial game
    moves = ("P5g-5f P5c-5d P5f-5e P5dx5e DE5h-5g P5e-5f DE5gx5f P6c-6d DE5f-5e "
             "P6d-6e DE5e-6d P6e-6f DE6d-6c+ DE5b-4b P4g-4f R8b-5b P4f-4e").split(' ')
    for move_str in moves:
        make_move(g, move_str)

    # compare effective moves with original list
    eq_([g.default_notation.move_serialization(m)
         for m in g.moves.played], moves)

    # check that clone() returns a sufficiently-working and independant game
    g2 = g.clone()
    eq_(g2.boardwidth, g.boardwidth)
    eq_(g2.boardheight, g.boardheight)
    # FIXME: hm, how are they so correctly set ?
    eq_(g2.board.ncolumns, g.board.ncolumns)
    eq_(g2.board.nrows, g.board.nrows)
    #for m in "".split(' '):
    #    make_move(g, move_str)
    #    make_move(g2, move_str)

def test_prince():
    # create a game
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    g = ShoShogi(parentcontext=app.context)
    g.setup()
    g.change_phase(g.Phases.Playing) # FIXME!
    eq_(g.phase, g.Phases.Playing)

    # replay a small partial game
    moves = ("P5g-5f P7c-7d DE5h-5g P7d-7e DE5g-6f P7e-7f DE6f-7e P7fx7g+ "
             "DE7e-7d +P7g-6h DE7d-7c+ +P6hx5i").split(' ')
    for move_str in moves:
        make_move(g, move_str)

    with assert_raises(Core.InvalidMove) as cm:
        make_move(g, "P5f-5e")
    ex = cm.exception
    ok_(ex.args[0].startswith("player's king would be in check"),
        "prince cannot be in check when the king is dead")

    make_move(g, "+DE7c-7d")
