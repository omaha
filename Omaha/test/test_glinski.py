# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import Overlord
from Omaha import Core
from .lib import StubToolkit, make_move
from Omaha.Games.GlinskiHexChess import GlinskiHexChess
from nose.tools import eq_

def test_chess():
    # create a game
    app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                      toolkit=StubToolkit(), parentcontext={})
    g = GlinskiHexChess(parentcontext=app.context)
    g.setup()
    g.change_phase(g.Phases.Playing) # FIXME!
    eq_(g.phase, g.Phases.Playing)

    # replay a small partial game
    # http://play.chessvariants.org/pbm/play.php?game=Glinski%27s+Hexagonal+Chess&log=rlavieri2003-ben_good-2004-80-035
    #moves = "h3-h5 h7-h6 g4-g5 e8-e7 i2-i4 i7-i5".split(' ')
    moves = "h3h5 h7h6 g4g5 e7e6 i2i4 i7i5".split(' ')
    for move_str in moves:
        make_move(g, move_str)
    # compare effective moves with original list
    eq_([g.default_notation.move_serialization(m)
         for m in g.moves.played], moves)
