# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2015  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import Overlord
from Omaha import Core
from .lib import StubToolkit, make_move
from Omaha.Games.Chess5x5 import Chess5x5
from Omaha.Games.piecetypes.chess import ChessFacetype
from nose.tools import eq_, ok_, assert_raises

class test(object):
    def setup(self):
        # create a game
        app = Core.UI.App(plugin_manager=None, prefs=Overlord.Preferences(),
                          toolkit=StubToolkit(), parentcontext={})
        g = Chess5x5(parentcontext=app.context)
        g.setup()
        g.change_phase(g.Phases.Playing) # FIXME!
        self.game = g
    def test_enpassant(self):
        eq_(self.game.phase, self.game.Phases.Playing)

        # 
        moves = "b2b3 c4b3 c2c4 b4c3".split(' ')
        for move_str in moves:
            make_move(self.game, move_str)
        ok_(self.game.moves.played[-1].capture != None,
            "en-passant should result in a capture")
    def test_enpassant_nonboost(self):
        eq_(self.game.phase, self.game.Phases.Playing)

        # 
        moves = "b2b3 c4c3 d2d3".split(' ')
        for move_str in moves:
            make_move(self.game, move_str)
        with assert_raises(Core.InvalidMove) as cm:
            make_move(self.game, "c3d2")
        ex = cm.exception
        ok_(ex.args[0] == "illegal attempt at en-passant",
            "en-passant can only capture after a startup boost")
    def test_enpassant_delayed(self):
        eq_(self.game.phase, self.game.Phases.Playing)

        # 
        moves = "b2b3 c4b3 c2c4 a4a3 c1a3".split(' ')
        for move_str in moves:
            make_move(self.game, move_str)
        with assert_raises(Core.InvalidMove) as cm:
            make_move(self.game, "b4c3")
        ex = cm.exception
        ok_(ex.args[0] == "illegal attempt at en-passant",
            "en-passant can only capture immediately after startup boost")

    # FIXME: lacks test for en-passant with a pinned pawn

    def test_pawn_check(self):
        moves = "c2c3 b4c3 b2b4".split(' ')
        for move_str in moves:
            make_move(self.game, move_str)
        with assert_raises(Core.InvalidMove) as cm:
            make_move(self.game, "a4a3")
        ex = cm.exception
        ok_(ex.args[0].startswith("player's king would be in check"),
            "threat from promoting pawn should be seen as any threat")
    def test_undo_promotion(self):
        g = self.game
        moves = "d2d3 e4d3 d1e3 d3d2 e3d5".split(' ')
        for move_str in moves:
            make_move(g, move_str)
        # before propotion move, we have a pawn here
        pawnloc = g.board.Location(g.board, 3, 3)
        pawn = g.board.piece_at(pawnloc)
        eq_(pawn.type, ChessFacetype.PAWN)
        # promote, we have no more pawn, and a promoted queen
        make_move(g, "d2d1q")
        queenloc = g.board.Location(g.board, 3, 4)
        eq_(g.board.piece_at(pawnloc), None)
        eq_(g.board.piece_at(queenloc).type, ChessFacetype.QUEEN)
        # undo, we have a pawn back
        g.undo_move()
        eq_(g.board.piece_at(queenloc), None)
        eq_(g.board.piece_at(pawnloc), pawn)
