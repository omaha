#!/usr/bin/python
from __future__ import with_statement
from DistUtilsExtra.auto import setup
import os

## first some magic to store version number
## from git for tarball distribution, and use
## it when
version = None

# guess version from git if possible and store into .version
if os.path.exists('.git'):
    import subprocess
    try:
        p = subprocess.Popen(('git', 'describe'),
                             stdout=subprocess.PIPE)
    except OSError:
        print "Note: git not found in PATH, ignoring .git/"
    else:
        stdout, stderr = p.communicate()
        stdout = stdout.rstrip()
        if stdout.startswith('v'):
            version = stdout[1:]

        # add -dirty when needed
        p = subprocess.Popen(('git', 'diff-index', '--name-only', 'HEAD'),
                             stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()
        stdout = stdout.rstrip()
        if len(stdout) != 0:
            version += '-dirty'

        # generate .version
        with open('.version', 'w') as f:
            f.write(version + "\n")

# else get from .version
if version is None and os.path.exists('.version'):
    with open('.version', 'r') as f:
        version = f.readline().rstrip()

if version is None:
    raise Exception("What version is that!?")


## real setup work

setup(name='omaha',
      description='A (wannabe) GUI to play arbitrary board games.',
      version=version,
      url='https://alioth.debian.org/projects/omaha/',
      author='Yann Dirson',
      author_email='ydirson@free.fr',

      scripts=[ 'scripts/omaha' ],
      data_files=( [ ('share/applications', ['omaha.desktop']) ] +
                   # recursive copy of relevant plugin files
                   [ ('share/games/omaha/' + path,
                      [ path + '/' + f for f in files
                        if f.endswith('.desktop')
                        or f.endswith('.png') or f.endswith('.svg') ])
                     for path, dirs, files in os.walk('plugins')
                     ] +
                   [ ('share/' + path,
                      [ path + '/' + f for f in files
                        if f.endswith('.png') or f.endswith('.svg') ])
                     for path, dirs, files in os.walk('icons')
                     ]),

      package_data = {'Omaha/test': ['data/*']},

      requires=[
        # required by Core
        'xdg',
        # required by GUI.Gtk (gtk pulls cairo, but we use it directly)
        'gtk', 'gobject', 'cairo',
        ],
      )
