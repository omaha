#!/usr/bin/env python

import collections, os
import dogtail.tree
from dogtail.tree import predicate, SearchError

# some config tuning
dogtail.config.config.searchCutoffCount = 5
dogtail.config.config.debugSearching = False
dogtail.config.config.debugSearchPaths = False

app = dogtail.tree.root.application("omaha")
launcher = app.window("omaha - launcher")

btn_play = launcher.button("play")

def my_click(accessible):
    accessible.doActionNamed('press')
    accessible.doActionNamed('release')

def excercise_game(game):
    # locate GUI items
    window = app.window("omaha - " + game)
    btn_prefs = window.button("Preferences")

    # display and validate prefs
    my_click(btn_prefs)
    dlg = app.dialog("UI parameters")
    my_click(dlg.button("OK"))

    # close window
    my_click(window.button("Close"))


# load some games

files = {'Omaha/test/data/go19a.sgf': 'Go',
         'Omaha/test/data/shogi1.pgn': 'Shogi',
         }

for f, game in files.iteritems():
    my_click(launcher.button("Open"))
    dlg = app.dialog("omaha - load game")
    # select "File System" cell
    table = dlg.childNamed("Places")
    table.add_row_selection(table.get_row_at_index(
        table.childNamed("File System").parent.indexInParent))
    # set path
    text_field = dlg.child(roleName="text", label="Location:")
    text_field.text = os.path.join(os.path.realpath(os.path.curdir), f)
    my_click(dlg.button("OK"))

    excercise_game(game)


# try all games, with various params

games_params = collections.defaultdict(
    # default set of param changes is empty
    lambda: ({},),
    # explicit sets of param changes, None if no param dialog expected
    {
        'Chess': None,
        'Chess 5x5': None,
        "Glinski's Chess": None,
        "Tori Shogi": None,
        'Go': ( { 'Board size': 19, 'Handicap stones': 5 },
                { 'Board size': 9, 'Handicap stones': 2 } ),
        })

for icon in launcher.findChildren(predicate.GenericPredicate(roleName='icon')):
    game = icon.text
    print game, "..."
    if games_params[game] is None:
        icon.doActionNamed('activate')
        excercise_game(game)
    else:
        for paramset in games_params[game]:
            icon.doActionNamed('activate')

            dlg_params = app.dialog("Game parameters")
            for label, value in paramset.iteritems():
                param_panel = dlg_params.childNamed(label)
                assert isinstance(value, int) # FIXME: support other types
                edit_field = param_panel.child(roleName='spin button')
                edit_field.value = value
            my_click(dlg_params.button("OK"))

            # in case of a launch bug causing param dialog to stay open, close it
            if not dlg_params.dead:
                my_click(dlg_params.button("Cancel"))

            excercise_game(game)

print "UI test finished."
